//********************************///********************************/
//  Name    : logic_card_firmware.ino
//  Author  : Attiq.Rehman@uib.no
//  Date    : 03 Oct, 2023
//  Notes   : This code provides follwing features
//            0) PROVIDES A BASIC TEMPLATE TO TEST AND BUILD FINAL FIRMWARE FOR LOGIC CARD
//            1) PIN CONFIGURATION BETWEEN ARDUINO AND LOGIC CARD BOARD
//            2) WRITES TO SHIFT REGISTERS (PISO) AND DRIVES LEDS VIA NUMBERS TO DISPLAY
//            2.a) NUMBERS TO DISPLAY CAN BE CHANGED TO ACTION TO BE TAKEN UPON DATA FROM TRIGGER CARD
//            2.b) RECEIVES DATA FROM SERIAL REGISTER AFTER SELECTING RIGHT PARAMETER VIA SELECT LINES
//            2.c) CHANGE SERIAL PRINT MESSAGES TO CONTROL ACTION AFTER DATA IS RECEIVED
// Last modified date : 31.10.2024
// Last modified by : TP
/*
//********************************///********************************/
/* Please include "NanoPinNaming.h" given in the folder to map pin numbering used to map with IOs
  Arduino Nano Every PinOut (ATmega4809)

                                        _________
                                       |         |
                          (D1) 	PC4  --|         |--  VIN
                          (DO)  PC5  --|         |--  GND
                              RESET  --|         |--  RESET
                 		            GND  --|         |--  5V
            serial_in_clk (D2)  PA0  --|         |--  PD5  (D21)
                serial_in (D3)  PF5  --|         |--  PD4  (D20)
                 inMUX_S0 (D4)  PC6  --|         |--  PA3/PF3  (D19/D23)
                 inMUX_S1 (D5)  PB2  --|         |--  PA2/PF2  (D18/D22)
                 inMUX_S3 (D6)  PF4  --|         |--  PD0  (D17)
                 inMUX_S2 (D7)  PA1  --|         |--  PD1  (D16) stcp1
                       PL (D8)  PE3  --|         |--  PD2  (D15) stcp2
                out_clock (D9)  PB0  --|         |--  PD3  (D14) stcp3
              out_enable (D10)  PB1  --|         |--  PD7  (AREF)
                out_data (D11)  PE0  --|         |--  3.3V
                         (D12)  PE1  --|         |--  PE2  (D13)
                                       |_________|
                                           |_|
*/
//********************************///********************************/

// Pin

constexpr uint8_t pinPC5 = 0;
constexpr uint8_t pinPC4 = 1;
constexpr uint8_t pinPA0 = 2;
constexpr uint8_t pinPF5 = 3;
constexpr uint8_t pinPC6 = 4;
constexpr uint8_t pinPB2 = 5;
constexpr uint8_t pinPF4 = 6;
constexpr uint8_t pinPA1 = 7;
constexpr uint8_t pinPE3 = 8;
constexpr uint8_t pinPB0 = 9;
constexpr uint8_t pinPB1 = 10;
constexpr uint8_t pinPE0 = 11;
constexpr uint8_t pinPE1 = 12;
constexpr uint8_t pinPE2 = 13;
constexpr uint8_t pinPD3 = 14;
constexpr uint8_t pinPD2 = 15;
constexpr uint8_t pinPD1 = 16;
constexpr uint8_t pinPD0 = 17;
constexpr uint8_t pinPF2 = 18;
constexpr uint8_t pinPF3 = 19;
constexpr uint8_t pinPD4 = 20;
constexpr uint8_t pinPD5 = 21;

/*
  Pin Naming and Configuration (see NanoPinNaming.h for mappings)
  Arduino Nano Every PinOut (ATmega4809)

    - Pins D2 to D7 for multiplexer (MUX) select lines and PISO clock
    - Pins D8 to D13 for data shift control
*/

//#include "NanoPinNaming.h" // Uncomment when including this file in the actual code

// Constants and Definitions
#define SHIFT_DELAY 20      // Delay in microseconds for shifting
#define EXPECTED_BITS 8     // Number of bits expected from PISO shift register

// 74HC595 Shift Register control pins
int stcp3 = pinPD3;         // Store clock pin for 3rd 74HC595 (SIPO shift register)
int stcp2 = pinPD2;         // Store clock pin for 2nd 74HC595
int stcp1 = pinPD1;         // Store clock pin for 1st 74HC595

// Multiplexer select line pins (connected to CD74HC4067 select lines S0-S3)
int inMUX_S0 = pinPC6;      // Select line S0
int inMUX_S1 = pinPB2;      // Select line S1
int inMUX_S2 = pinPA1;      // Select line S2
int inMUX_S3 = pinPF4;      // Select line S3 (only used for 7 channels in this setup)

// PISO shift register (4021B) control pins
int serial_in_clk = pinPA0; // Serial input clock for 4021B
int serial_in     = pinPF5; // Data input from 4021B PISO shift register
int PL            = pinPE2; // Parallel load pin for 4021B PISO

// SIPO shift register (74HC595) control pins
int out_clk       = pinPB0; // Output clock pin
int out_data      = pinPE0; // Data output pin
int out_enable    = pinPB1; // Enable pin (controls output state)

int numberToDisplay2 = 0x0e; // Example value for controlling LEDs on SIPO shift register 2
int numberToDisplay3 = 0x0c; // Example value for controlling LEDs on SIPO shift register 3
int serial_data      = 0;

void setup() {
    Serial.begin(9600);

    // Set pin modes for 74HC595 control
    pinMode(stcp1, OUTPUT);
    pinMode(stcp2, OUTPUT);
    pinMode(stcp3, OUTPUT);

    // Set pin modes for SIPO shift register (74HC595)
    pinMode(out_clk, OUTPUT);
    pinMode(out_data, OUTPUT);
    pinMode(out_enable, OUTPUT);

    // Set pin modes for PISO shift register (4021B)
    pinMode(serial_in, INPUT);
    pinMode(serial_in_clk, OUTPUT);
    pinMode(PL, OUTPUT);

    // Set pin modes for MUX select lines
    pinMode(inMUX_S0, OUTPUT);
    pinMode(inMUX_S1, OUTPUT);
    pinMode(inMUX_S2, OUTPUT);
    pinMode(inMUX_S3, OUTPUT);

    // Initialize pin states
    digitalWrite(serial_in_clk, LOW);
    digitalWrite(out_enable, HIGH);  // Initially disable outputs
}

void loop() {
    uint8_t sevenBitOutput = 0;  // Holds extracted data from 7 channels
    serial_data = 0;

    Serial.println("Reading bit0 from first 7 channels...");

    // Loop through channels 0 to 6 to read data
    for (int channel = 0; channel <= 6; channel++) {
        // Set multiplexer select lines to select a specific channel
        digitalWrite(inMUX_S0, channel & 0x01);
        digitalWrite(inMUX_S1, (channel >> 1) & 0x01);
        digitalWrite(inMUX_S2, (channel >> 2) & 0x01);
        digitalWrite(inMUX_S3, LOW);  // Only 7 channels are required

        delayMicroseconds(SHIFT_DELAY);

        // Load data from PISO shift register
        digitalWrite(PL, HIGH);
        delayMicroseconds(SHIFT_DELAY);
        digitalWrite(PL, LOW);

        // Read data bit by bit
        serial_data = 0;
        for (int n = 0; n < EXPECTED_BITS; n++) {
            digitalWrite(serial_in_clk, LOW);
            delayMicroseconds(SHIFT_DELAY);

            int tmp = digitalRead(serial_in);
            serial_data = (serial_data << 1) | tmp;

            digitalWrite(serial_in_clk, HIGH);
            delayMicroseconds(SHIFT_DELAY);
        }

        // Extract the least significant bit (bit 0) for each channel and store it in sevenBitOutput
        uint8_t bit0 = (serial_data >> 3) & 0x01; // Extract bit0 from 7 channels
        sevenBitOutput |= (bit0 << channel);
    }

    // Check specific bits and control LEDs based on conditions for channels 1, 4, 5, and 6
    if (((sevenBitOutput >> 1) & 0x01) == 0 ||
        ((sevenBitOutput >> 4) & 0x01) == 0 ||
        ((sevenBitOutput >> 5) & 0x01) == 0 ||
        ((sevenBitOutput >> 6) & 0x01) == 0) {
        numberToDisplay2 = 0x00;
        numberToDisplay3 = 0x00;
    } else if (((sevenBitOutput >> 1) & 0x01) == 1 ||
               ((sevenBitOutput >> 4) & 0x01) == 1 ||
               ((sevenBitOutput >> 5) & 0x01) == 1 ||
               ((sevenBitOutput >> 6) & 0x01) == 1) {
        numberToDisplay2 = 0x0f;
        numberToDisplay3 = 0x0f;
    }

    // Display the 7-bit output for debugging
    Serial.println("7-bit output:");
    for (int i = 0; i < 7; i++) {
        Serial.print("bit");
        Serial.print(i);
        Serial.print(": ");
        Serial.println((sevenBitOutput >> i) & 0x01);
    }

    delay(1000);

    // Send data to 74HC595 shift registers to update LED states
    digitalWrite(out_enable, HIGH); // Enable outputs for shifting
    digitalWrite(out_clk, LOW);

    shiftOut(out_data, out_clk, MSBFIRST, sevenBitOutput);
    digitalWrite(stcp1, HIGH);
    delayMicroseconds(SHIFT_DELAY);
    digitalWrite(stcp1, LOW);

    shiftOut(out_data, out_clk, MSBFIRST, numberToDisplay2);
    digitalWrite(stcp2, HIGH);
    delayMicroseconds(SHIFT_DELAY);
    digitalWrite(stcp2, LOW);

    shiftOut(out_data, out_clk, MSBFIRST, numberToDisplay3);
    digitalWrite(stcp3, HIGH);
    delayMicroseconds(SHIFT_DELAY);
    digitalWrite(stcp3, LOW);

    digitalWrite(out_enable, LOW);  // Disable outputs after update

    delay(100);  // Delay between readings
}
