//********************************///********************************/
//  Name    : logic_card_firmware.ino
//  Author  : Attiq.Rehman@uib.no
//  Date    : 03 Oct, 2023
//  Notes   : This code provides follwing features
//            0) PROVIDES A BASIC TEMPELATE TO TEST AND BUILD FINAL FIRMWARE FOR LOGIC CARD
//            1) PIN CONFGURATION BETWEEN AUDRION AND LOGIC CARD BOARD
//            2) WRITES TO SHIFT REGISTERS (PISO) AND DRIVES LEDS VIA NUMBERS TO DSIPLAY
//            2.a) NUMBERS TO DISPLAY CAN BE CHANGED TO ACTION TO BE TAKEN UPONG DATA FROM TRIGGER CARD
//            2.b) RECEIVES DATA FROM SERAIL REGISTER AFTER SELECTING RIGHT PARAMETER VIA SELECT LINES
//            2.c) CHANGE SERIAL PRINT MESSAGES TO CONTROLING ACTION AFTER DATA IS RECEIVED
// Last modified date : 31.10.2023
// Last modified by : ØB
/*
//********************************///********************************/
/* Please include "NanoPinNaming.h" given in the folder to map pin numbering used to map with IOs
  Arduino Nano Every PinOut (ATmega4809)
                                            _
                                        ___|_|___    
                                       |         |  
                         (D13)  PE2  --|         |--  PE1  (D12)  
                               3.3V  --|         |--  PE0  (D11) out_data  
                         (AREF) PD7  --|         |--  PB1  (D10) out_enbale
                   stcp3 (D14)  PD3  --|         |--  PB0  (D9) out_clk
                   stcp2 (D15)  PD2  --|         |--  PE3  (D8) PL
                   stcp1 (D16)  PD1  --|         |--  PA1  (D7) inMUX_S2
                         (D17)  PD0  --|         |--  PF4  (D6) inMUX_S3
                         (D18)  PF2  --|         |--  PB2  (D5) inMUX_S1
                         (D19)  PF3  --|         |--  PC6  (D4) inMUX_S0
                         (D20)  PD4  --|         |--  PF5  (D3) serial_in
                         (D21)  PD5  --|         |--  PA0  (D2) serial_in_clk
                               5.0V  --|         |--  GND
                              RESET  --|         |--  RESET
                                GND  --|         |--  PC5  (D 0) 
                                VIN  --|         |--  PC4  (D 1) 
                                       |_________|  
  */                       
//********************************///********************************/

// Pin

constexpr uint8_t pinPC5 = 0;
constexpr uint8_t pinPC4 = 1;
constexpr uint8_t pinPA0 = 2;
constexpr uint8_t pinPF5 = 3;
constexpr uint8_t pinPC6 = 4;
constexpr uint8_t pinPB2 = 5;
constexpr uint8_t pinPF4 = 6;
constexpr uint8_t pinPA1 = 7;
constexpr uint8_t pinPE3 = 8;
constexpr uint8_t pinPB0 = 9;
constexpr uint8_t pinPB1 = 10;
constexpr uint8_t pinPE0 = 11;
constexpr uint8_t pinPE1 = 12;
constexpr uint8_t pinPE2 = 13;
constexpr uint8_t pinPD3 = 14;
constexpr uint8_t pinPD2 = 15;
constexpr uint8_t pinPD1 = 16;
constexpr uint8_t pinPD0 = 17;
constexpr uint8_t pinPF2 = 18;
constexpr uint8_t pinPF3 = 19;
constexpr uint8_t pinPD4 = 20;
constexpr uint8_t pinPD5 = 21;


// #include <C:\Temp\Blink_every\NanoPinNaming.h> // file is uploaded , check if needed then place in your local folder
//Pins connected to ST_CP of 74HC595
           int stcp3 = pinPD3;        // 14 pinPD3 --> Pin connected to SH_CP of 74HC595
           int stcp2 = pinPD2;        // 15;    // pinPD2 --> Pin connected to SH_CP of 74HC595
           int stcp1 = pinPD1;       // 16;    // pinPD1 --> Pin connected to SH_CP of 74HC595
           
           // Pins to multiplexer select lines
           int inMUX_S0 = pinPC6;    // 4;
           int inMUX_S1 = pinPB2;    // 5;
           int inMUX_S2 = pinPA1;    // 7;
           int inMUX_S3 = pinPF4;    // 6;           
           
           //Pins connected to PISO shift register
           int serial_in_clk = pinPA0;    // 2 ;
           int serial_in     = pinPF5;    // 3 ;
           int PL            = pinPE1;    // 12 ;

//Common pincs to all shift registers
           int out_clk    = pinPB0;    // 9;   // pinPB0 
           int out_data   = pinPE0;    // 11; // pinPE0 
           int out_enable = pinPE2;    // 13; // pinPE2
           
           int numberToDisplay  = 0x11;
           int numberToDisplay2 = 0x22;
           int numberToDisplay3 = 0x33;
           int serial_data      = 0;
           int reading = LOW;
           
           void setup() {
//set pins to output so you can control the shift register
           pinMode(stcp1, OUTPUT);
           pinMode(stcp2, OUTPUT);
           pinMode(stcp3, OUTPUT);
           pinMode(out_clk, OUTPUT);
           pinMode(out_data, OUTPUT);
           pinMode(out_enable, OUTPUT);
           pinMode(serial_in, INPUT);
           pinMode(serial_in_clk, OUTPUT);
           pinMode(PL, OUTPUT);
           pinMode( inMUX_S0, OUTPUT);
           pinMode( inMUX_S1, OUTPUT);
           pinMode( inMUX_S3, OUTPUT);
           pinMode( inMUX_S2, OUTPUT);
           digitalWrite(serial_in_clk, LOW);
           }

        void loop() {
            
            Serial.begin(9600);
              digitalWrite(PL, HIGH);
              digitalWrite(serial_in_clk, LOW);
              digitalWrite(inMUX_S0, LOW);
              digitalWrite(inMUX_S1, LOW);
              digitalWrite(inMUX_S2, LOW);
              digitalWrite(inMUX_S3, LOW);
              digitalWrite(PL, LOW);
            Serial.println("First");
            serial_data = 0;
            for (int n=0; n<=7; n++)// for testing, reading 1 by 1 bit
             {  
               
               int tmp = digitalRead(serial_in);
               serial_data = serial_data << 1 | tmp;
               Serial.println(tmp);
               digitalWrite(serial_in_clk, HIGH);
               digitalWrite(serial_in_clk, LOW);
               }

              //serial_data = shiftIn(serial_in,  serial_in_clk, MSBFIRST);
              Serial.println("Hex value:");
              Serial.println(serial_data, HEX);     
              delay(100);


             /*switch (serial_data) {
             case 0x0:
             // code to take action goes here
             //  Serial.println("All input are off / not connected on connector");
               break;
             case 0x1:
             // code to take action goes here
             //  Serial.println("One input is ON or only one connected");
               break;
             default:
             //  Serial.println("Default non sense");
               break;
           }*/
             
            delay(100);
           // the LEDs don't change while you're sending in bits:
           digitalWrite(stcp1, LOW);
           digitalWrite(stcp2, LOW);
           digitalWrite(stcp3, LOW);
           digitalWrite(out_enable, HIGH);
           digitalWrite(out_enable, LOW);

       numberToDisplay = 0xFF;
       numberToDisplay2 = 0x22;
       numberToDisplay3 = 0x33;

      //stcp3 = pinPD3;  which is connector #2
      //stcp2 = pinPD2;   which is connector #1
      // stcp1 = pinPD1; which is connector #3
      
         digitalWrite(out_clk, LOW);
         shiftOut(out_data, out_clk, MSBFIRST, serial_data);
         //shiftOut(out_data, out_clk, MSBFIRST, numberToDisplay1);
         digitalWrite(stcp1, HIGH);
         //shiftOut(out_data, out_clk, MSBFIRST, numberToDisplay2);
         shiftOut(out_data, out_clk, MSBFIRST, serial_data);
         digitalWrite(stcp2, HIGH);
         //shiftOut(out_data, out_clk, MSBFIRST, numberToDisplay3);
         shiftOut(out_data, out_clk, MSBFIRST, serial_data);
         digitalWrite(stcp3, HIGH);
    }
