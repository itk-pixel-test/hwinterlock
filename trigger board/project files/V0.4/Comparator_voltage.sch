EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 30 30
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 7600 2900 2    50   Input ~ 0
!V_arm
$Comp
L Device:R R?
U 1 1 5EF1B2F5
P 4500 2850
AR Path="/5EF1B2F5" Ref="R?"  Part="1" 
AR Path="/5EB41214/5EF1B2F5" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5EF1B2F5" Ref="R31"  Part="1" 
AR Path="/5ECA2575/5EF1B2F5" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5EF1B2F5" Ref="R?"  Part="1" 
F 0 "R31" V 4293 2850 50  0000 C CNN
F 1 "R 1.3K " V 4384 2850 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4430 2850 50  0001 C CNN
F 3 "~" H 4500 2850 50  0001 C CNN
F 4 "0.1%" H 4500 2850 50  0001 C CNN "Tolerance"
	1    4500 2850
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5EF19946
P 4900 2850
AR Path="/5EF19946" Ref="R?"  Part="1" 
AR Path="/5EB41214/5EF19946" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5EF19946" Ref="R33"  Part="1" 
AR Path="/5ECA2575/5EF19946" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5EF19946" Ref="R?"  Part="1" 
F 0 "R33" V 4693 2850 50  0000 C CNN
F 1 "R 12K" V 4784 2850 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4830 2850 50  0001 C CNN
F 3 "~" H 4900 2850 50  0001 C CNN
F 4 "0.1%" H 4900 2850 50  0001 C CNN "Tolerance"
	1    4900 2850
	0    1    1    0   
$EndComp
Text Notes 5150 2750 0    50   ~ 0
0.5V setpoint
Wire Wire Line
	4900 3300 3750 3300
Wire Wire Line
	4300 2850 4350 2850
Wire Wire Line
	5200 2550 5200 2850
Connection ~ 5200 2850
Wire Wire Line
	5050 2850 5200 2850
Wire Wire Line
	4700 2850 4750 2850
Wire Wire Line
	4700 3100 4700 2850
Wire Wire Line
	4900 3100 4700 3100
Connection ~ 4700 2850
Wire Wire Line
	4650 2850 4700 2850
Wire Wire Line
	5200 3000 5200 2850
$Comp
L power:GND1 #PWR?
U 1 1 5EC8A321
P 5200 3500
F 0 "#PWR?" H 5200 3250 50  0001 C CNN
F 1 "GND1" H 5205 3327 50  0000 C CNN
F 2 "" H 5200 3500 50  0001 C CNN
F 3 "" H 5200 3500 50  0001 C CNN
	1    5200 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR?
U 1 1 5EC8B75B
P 4300 2850
F 0 "#PWR?" H 4300 2600 50  0001 C CNN
F 1 "GND1" H 4305 2677 50  0000 C CNN
F 2 "" H 4300 2850 50  0001 C CNN
F 3 "" H 4300 2850 50  0001 C CNN
	1    4300 2850
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5EC88F72
P 5200 2550
F 0 "#PWR?" H 5200 2400 50  0001 C CNN
F 1 "+5VA" H 5215 2723 50  0000 C CNN
F 2 "" H 5200 2550 50  0001 C CNN
F 3 "" H 5200 2550 50  0001 C CNN
	1    5200 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5ED05503
P 4900 4200
AR Path="/5ED05503" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED05503" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED05503" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ED05503" Ref="R?"  Part="1" 
AR Path="/5EBFB816/5ED05503" Ref="R?"  Part="1" 
F 0 "R?" V 4693 4200 50  0000 C CNN
F 1 "R" V 4784 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4830 4200 50  0001 C CNN
F 3 "~" H 4900 4200 50  0001 C CNN
F 4 "1%" H 4900 4200 50  0001 C CNN "Tolerance"
	1    4900 4200
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5ED0550A
P 4500 4200
AR Path="/5ED0550A" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED0550A" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED0550A" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ED0550A" Ref="R?"  Part="1" 
AR Path="/5EBFB816/5ED0550A" Ref="R?"  Part="1" 
F 0 "R?" V 4707 4200 50  0000 C CNN
F 1 "R 10K 0.1%" V 4616 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4430 4200 50  0001 C CNN
F 3 "~" H 4500 4200 50  0001 C CNN
F 4 "0.1%" H 4500 4200 50  0001 C CNN "Tolerance"
	1    4500 4200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 4200 4700 4200
Wire Wire Line
	4900 4450 4700 4450
Wire Wire Line
	4700 4450 4700 4200
Connection ~ 4700 4200
Wire Wire Line
	4700 4200 4750 4200
Wire Wire Line
	5050 4200 5200 4200
Connection ~ 5200 4200
Wire Wire Line
	5200 4200 5200 4100
Text HLabel 8900 4300 2    50   Input ~ 0
Isens_ARM_alarm
Text GLabel 7300 3750 0    50   Input ~ 0
!ARM_LVsens
Text GLabel 7550 4450 2    50   Input ~ 0
!I_arm
Wire Wire Line
	5200 4350 5200 4200
$Comp
L power:+5VA #PWR?
U 1 1 5ED0554B
P 5200 4100
AR Path="/5EB41214/5ED0554B" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED0554B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5200 3950 50  0001 C CNN
F 1 "+5VA" H 5215 4273 50  0000 C CNN
F 2 "" H 5200 4100 50  0001 C CNN
F 3 "" H 5200 4100 50  0001 C CNN
	1    5200 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR?
U 1 1 5ED0556A
P 4350 4200
AR Path="/5EB41214/5ED0556A" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED0556A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4350 3950 50  0001 C CNN
F 1 "GND1" H 4355 4027 50  0000 C CNN
F 2 "" H 4350 4200 50  0001 C CNN
F 3 "" H 4350 4200 50  0001 C CNN
	1    4350 4200
	1    0    0    -1  
$EndComp
Text HLabel 3750 3300 0    50   Input ~ 0
Voltage
Text HLabel 3700 4650 0    50   Input ~ 0
Current
Wire Wire Line
	3700 4650 4900 4650
$Comp
L dk_Linear-Comparators:LM2903DR U?
U 1 1 5ED268CD
P 5200 3200
F 0 "U?" H 5544 3253 60  0000 L CNN
F 1 "LM2903DR" H 5544 3147 60  0000 L CNN
F 2 "digikey-footprints:SOIC-8_W3.9mm" H 5400 3400 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm193" H 5400 3500 60  0001 L CNN
F 4 "296-1012-1-ND" H 5400 3600 60  0001 L CNN "Digi-Key_PN"
F 5 "LM2903DR" H 5400 3700 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 5400 3800 60  0001 L CNN "Category"
F 7 "Linear - Comparators" H 5400 3900 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm193" H 5400 4000 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/LM2903DR/296-1012-1-ND/276280" H 5400 4100 60  0001 L CNN "DK_Detail_Page"
F 10 "IC DUAL DIFF COMP 8-SOIC" H 5400 4200 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 5400 4300 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5400 4400 60  0001 L CNN "Status"
	1    5200 3200
	1    0    0    -1  
$EndComp
$Comp
L dk_Linear-Comparators:LM2903DR U?
U 2 1 5ED273A4
P 5200 4550
F 0 "U?" H 5544 4603 60  0000 L CNN
F 1 "LM2903DR" H 5544 4497 60  0000 L CNN
F 2 "digikey-footprints:SOIC-8_W3.9mm" H 5400 4750 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm193" H 5400 4850 60  0001 L CNN
F 4 "296-1012-1-ND" H 5400 4950 60  0001 L CNN "Digi-Key_PN"
F 5 "LM2903DR" H 5400 5050 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 5400 5150 60  0001 L CNN "Category"
F 7 "Linear - Comparators" H 5400 5250 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm193" H 5400 5350 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/LM2903DR/296-1012-1-ND/276280" H 5400 5450 60  0001 L CNN "DK_Detail_Page"
F 10 "IC DUAL DIFF COMP 8-SOIC" H 5400 5550 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 5400 5650 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5400 5750 60  0001 L CNN "Status"
	2    5200 4550
	1    0    0    -1  
$EndComp
Text HLabel 8900 3050 2    50   Input ~ 0
Vsens_ARM_alarm
$Comp
L Device:R R?
U 1 1 5ED43ACB
P 5950 4300
AR Path="/5ED43ACB" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED43ACB" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED43ACB" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ED43ACB" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5ED43ACB" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5ED43ACB" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5ED43ACB" Ref="R?"  Part="1" 
F 0 "R?" H 5850 4300 50  0000 C CNN
F 1 "R 16K" H 5700 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5880 4300 50  0001 C CNN
F 3 "~" H 5950 4300 50  0001 C CNN
F 4 "1%" H 5950 4300 50  0001 C CNN "Tolerance"
	1    5950 4300
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5ED43AD1
P 5950 4000
AR Path="/5EE8A13E/5ED43AD1" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED43AD1" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5950 3850 50  0001 C CNN
F 1 "+5V" H 5965 4173 50  0000 C CNN
F 2 "" H 5950 4000 50  0001 C CNN
F 3 "" H 5950 4000 50  0001 C CNN
	1    5950 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2600 6000 2650
Wire Wire Line
	5950 4000 5950 4150
Wire Wire Line
	6000 3000 6000 3200
Wire Wire Line
	5500 3200 6000 3200
Connection ~ 6000 3200
Wire Wire Line
	5950 4450 5950 4550
Wire Wire Line
	5500 4550 5950 4550
$Comp
L dk_Transistors-Bipolar-BJT-Single:BC817-25-TP Q?
U 1 1 5ED43AE8
P 6750 3200
AR Path="/5ED2BC2A/5ED43AE8" Ref="Q?"  Part="1" 
AR Path="/5EE8A13E/5ED43AE8" Ref="Q?"  Part="1" 
F 0 "Q?" H 6938 3253 60  0000 L CNN
F 1 "BC817-25-TP" H 6950 3100 60  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6950 3400 60  0001 L CNN
F 3 "https://www.mccsemi.com/pdf/Products/BC817-16~BC817-40(SOT-23).pdf" H 6950 3500 60  0001 L CNN
F 4 "BC817-25-TPMSCT-ND" H 6950 3600 60  0001 L CNN "Digi-Key_PN"
F 5 "BC817-25-TP" H 6950 3700 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 6950 3800 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 6950 3900 60  0001 L CNN "Family"
F 8 "https://www.mccsemi.com/pdf/Products/BC817-16~BC817-40(SOT-23).pdf" H 6950 4000 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/micro-commercial-co/BC817-25-TP/BC817-25-TPMSCT-ND/1960198" H 6950 4100 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 45V 0.8A SOT-23" H 6950 4200 60  0001 L CNN "Description"
F 11 "Micro Commercial Co" H 6950 4300 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6950 4400 60  0001 L CNN "Status"
	1    6750 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5ED43AEF
P 6350 3200
AR Path="/5ED43AEF" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED43AEF" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED43AEF" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5ED43AEF" Ref="R?"  Part="1" 
F 0 "R?" V 6250 3200 50  0000 C CNN
F 1 "R 51.1K " V 6150 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6280 3200 50  0001 C CNN
F 3 "~" H 6350 3200 50  0001 C CNN
F 4 "1%" H 6350 3200 50  0001 C CNN "Tolerance"
	1    6350 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6500 3200 6550 3200
$Comp
L Device:R R?
U 1 1 5ED43AF7
P 6850 2800
AR Path="/5ED43AF7" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED43AF7" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED43AF7" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5ED43AF7" Ref="R?"  Part="1" 
F 0 "R?" H 6750 2850 50  0000 C CNN
F 1 "R 10K" H 6600 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6780 2800 50  0001 C CNN
F 3 "~" H 6850 2800 50  0001 C CNN
F 4 "1%" H 6850 2800 50  0001 C CNN "Tolerance"
	1    6850 2800
	-1   0    0    1   
$EndComp
Wire Wire Line
	6850 2950 6850 3000
$Comp
L power:+5V #PWR?
U 1 1 5ED43AFE
P 6000 2600
AR Path="/5EE8A13E/5ED43AFE" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED43AFE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6000 2450 50  0001 C CNN
F 1 "+5V" H 6015 2773 50  0000 C CNN
F 2 "" H 6000 2600 50  0001 C CNN
F 3 "" H 6000 2600 50  0001 C CNN
	1    6000 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 2650 6000 2650
Connection ~ 6000 2650
Wire Wire Line
	6000 2650 6000 2700
Wire Wire Line
	6200 3200 6000 3200
Wire Wire Line
	6850 3400 6850 3500
Connection ~ 6850 3000
$Comp
L Device:R R?
U 1 1 5ED43B0B
P 6000 2850
AR Path="/5ED43B0B" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED43B0B" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED43B0B" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5ED43B0B" Ref="R?"  Part="1" 
F 0 "R?" H 5900 2900 50  0000 C CNN
F 1 "R 10K" H 5750 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5930 2850 50  0001 C CNN
F 3 "~" H 6000 2850 50  0001 C CNN
F 4 "1%" H 6000 2850 50  0001 C CNN "Tolerance"
	1    6000 2850
	-1   0    0    1   
$EndComp
$Comp
L dk_Transistors-Bipolar-BJT-Single:BC817-25-TP Q?
U 1 1 5ED43B1A
P 6750 4550
AR Path="/5ED2BC2A/5ED43B1A" Ref="Q?"  Part="1" 
AR Path="/5EE8A13E/5ED43B1A" Ref="Q?"  Part="1" 
F 0 "Q?" H 6938 4603 60  0000 L CNN
F 1 "BC817-25-TP" H 6950 4450 60  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6950 4750 60  0001 L CNN
F 3 "https://www.mccsemi.com/pdf/Products/BC817-16~BC817-40(SOT-23).pdf" H 6950 4850 60  0001 L CNN
F 4 "BC817-25-TPMSCT-ND" H 6950 4950 60  0001 L CNN "Digi-Key_PN"
F 5 "BC817-25-TP" H 6950 5050 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 6950 5150 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 6950 5250 60  0001 L CNN "Family"
F 8 "https://www.mccsemi.com/pdf/Products/BC817-16~BC817-40(SOT-23).pdf" H 6950 5350 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/micro-commercial-co/BC817-25-TP/BC817-25-TPMSCT-ND/1960198" H 6950 5450 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 45V 0.8A SOT-23" H 6950 5550 60  0001 L CNN "Description"
F 11 "Micro Commercial Co" H 6950 5650 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6950 5750 60  0001 L CNN "Status"
	1    6750 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5ED43B21
P 6350 4550
AR Path="/5ED43B21" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED43B21" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED43B21" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5ED43B21" Ref="R?"  Part="1" 
F 0 "R?" V 6250 4550 50  0000 C CNN
F 1 "R 51.1K " V 6150 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6280 4550 50  0001 C CNN
F 3 "~" H 6350 4550 50  0001 C CNN
F 4 "1%" H 6350 4550 50  0001 C CNN "Tolerance"
	1    6350 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6500 4550 6550 4550
$Comp
L Device:R R?
U 1 1 5ED43B29
P 6850 4150
AR Path="/5ED43B29" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED43B29" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED43B29" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5ED43B29" Ref="R?"  Part="1" 
F 0 "R?" H 6750 4200 50  0000 C CNN
F 1 "R 10K" H 6600 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6780 4150 50  0001 C CNN
F 3 "~" H 6850 4150 50  0001 C CNN
F 4 "1%" H 6850 4150 50  0001 C CNN "Tolerance"
	1    6850 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	6850 4300 6850 4350
Connection ~ 6850 4350
Wire Wire Line
	5950 4550 6200 4550
Connection ~ 5950 4550
Wire Wire Line
	6850 4000 5950 4000
Connection ~ 5950 4000
$Comp
L dk_Logic-Gates-and-Inverters:SN74LVC1G14DBVR U?
U 1 1 5ED43B40
P 7700 3750
AR Path="/5EE8A13E/5ED43B40" Ref="U?"  Part="1" 
AR Path="/5ED2BC2A/5ED43B40" Ref="U?"  Part="1" 
F 0 "U?" H 7944 3803 60  0000 L CNN
F 1 "SN74LVC1G14DBVR" H 7850 3650 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 7900 3950 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 7900 4050 60  0001 L CNN
F 4 "296-11607-1-ND" H 7900 4150 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G14DBVR" H 7900 4250 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 7900 4350 60  0001 L CNN "Category"
F 7 "Logic - Gates and Inverters" H 7900 4450 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 7900 4550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G14DBVR/296-11607-1-ND/385746" H 7900 4650 60  0001 L CNN "DK_Detail_Page"
F 10 "IC INVERTER SCHMITT 1CH SOT23-5" H 7900 4750 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 7900 4850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7900 4950 60  0001 L CNN "Status"
	1    7700 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 3750 7400 3750
Wire Wire Line
	7900 3750 7900 3100
Wire Wire Line
	7900 3100 8100 3100
Wire Wire Line
	7900 3750 7900 4250
Wire Wire Line
	7900 4250 8100 4250
Connection ~ 7900 3750
$Comp
L power:+5V #PWR?
U 1 1 5ED43B4C
P 7700 3500
AR Path="/5EE8A13E/5ED43B4C" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED43B4C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7700 3350 50  0001 C CNN
F 1 "+5V" H 7715 3673 50  0000 C CNN
F 2 "" H 7700 3500 50  0001 C CNN
F 3 "" H 7700 3500 50  0001 C CNN
	1    7700 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 3500 7700 3550
$Comp
L power:GND #PWR?
U 1 1 5ED43B53
P 7600 4000
AR Path="/5EB41214/5ED43B53" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED43B53" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5ED43B53" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5ED43B53" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5ED43B53" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5ED43B53" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7600 3750 50  0001 C CNN
F 1 "GND" H 7605 3827 50  0000 C CNN
F 2 "" H 7600 4000 50  0001 C CNN
F 3 "" H 7600 4000 50  0001 C CNN
	1    7600 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 3950 7600 4000
Wire Wire Line
	6850 3000 7550 3000
Wire Wire Line
	6850 4350 7550 4350
Wire Wire Line
	7600 2900 7550 2900
Wire Wire Line
	7550 2900 7550 3000
Connection ~ 7550 3000
Wire Wire Line
	7550 3000 8100 3000
Wire Wire Line
	7550 4450 7550 4350
Connection ~ 7550 4350
Wire Wire Line
	7550 4350 8100 4350
$Comp
L logic_gates_magne:74LVC2G32 U?
U 1 1 5ED43B66
P 8400 3050
AR Path="/5EE8A13E/5ED43B66" Ref="U?"  Part="1" 
AR Path="/5ED2BC2A/5ED43B66" Ref="U?"  Part="1" 
F 0 "U?" H 8375 3317 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 8375 3226 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 8550 3100 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 8600 3050 50  0001 C CNN
	1    8400 3050
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U?
U 2 1 5ED43B6C
P 8400 4300
AR Path="/5EE8A13E/5ED43B6C" Ref="U?"  Part="2" 
AR Path="/5ED2BC2A/5ED43B6C" Ref="U?"  Part="2" 
F 0 "U?" H 8375 4567 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 8375 4476 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 8550 4350 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 8600 4300 50  0001 C CNN
	2    8400 4300
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U?
U 3 1 5ED43B72
P 8150 4950
AR Path="/5EE8A13E/5ED43B72" Ref="U?"  Part="3" 
AR Path="/5ED2BC2A/5ED43B72" Ref="U?"  Part="3" 
F 0 "U?" H 8203 4996 50  0000 L CNN
F 1 "SN74LVC2G32DCUR" H 8203 4905 50  0000 L CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 8300 5000 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 8350 4950 50  0001 C CNN
	3    8150 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 4750 6850 5200
Connection ~ 6850 5200
$Comp
L Device:C_Small C?
U 1 1 5ED43B7D
P 7750 5000
AR Path="/5EB41214/5ED43B7D" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ED43B7D" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5ED43B7D" Ref="C?"  Part="1" 
F 0 "C?" H 7842 5046 50  0000 L CNN
F 1 "C 47n" H 7842 4955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7750 5000 50  0001 C CNN
F 3 "~" H 7750 5000 50  0001 C CNN
F 4 "16" H 7750 5000 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7750 5000 50  0001 C CNN "Tolerance"
	1    7750 5000
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5ED43B83
P 7750 4850
AR Path="/5EE8A13E/5ED43B83" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED43B83" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 7750 4700 50  0001 C CNN
F 1 "+5V" H 7765 5023 50  0000 C CNN
F 2 "" H 7750 4850 50  0001 C CNN
F 3 "" H 7750 4850 50  0001 C CNN
	1    7750 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 5100 7750 5200
Connection ~ 7750 5200
Wire Wire Line
	7750 5200 8150 5200
Wire Wire Line
	7750 4850 7750 4900
Wire Wire Line
	7750 4900 8000 4900
Wire Wire Line
	8000 4900 8000 4700
Wire Wire Line
	8000 4700 8150 4700
Connection ~ 7750 4900
Wire Wire Line
	5200 3400 5200 3500
Wire Wire Line
	5200 3500 6850 3500
Connection ~ 5200 3500
Wire Wire Line
	6850 5200 7750 5200
Wire Wire Line
	5400 4950 5400 5000
$Comp
L Device:C_Small C?
U 1 1 5ED05585
P 5400 5100
AR Path="/5EB41214/5ED05585" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ED05585" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ED05585" Ref="C?"  Part="1" 
AR Path="/5EBFB816/5ED05585" Ref="C?"  Part="1" 
F 0 "C?" H 5492 5146 50  0000 L CNN
F 1 "C 47n" H 5492 5055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5400 5100 50  0001 C CNN
F 3 "~" H 5400 5100 50  0001 C CNN
F 4 "16" H 5400 5100 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5400 5100 50  0001 C CNN "Tolerance"
	1    5400 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR?
U 1 1 5ED05563
P 5200 5200
AR Path="/5EB41214/5ED05563" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED05563" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5200 4950 50  0001 C CNN
F 1 "GND1" H 5205 5027 50  0000 C CNN
F 2 "" H 5200 5200 50  0001 C CNN
F 3 "" H 5200 5200 50  0001 C CNN
	1    5200 5200
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5ED05551
P 5400 4950
AR Path="/5EB41214/5ED05551" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED05551" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5400 4800 50  0001 C CNN
F 1 "+5VA" H 5415 5123 50  0000 C CNN
F 2 "" H 5400 4950 50  0001 C CNN
F 3 "" H 5400 4950 50  0001 C CNN
	1    5400 4950
	1    0    0    -1  
$EndComp
Connection ~ 5400 5200
Wire Wire Line
	5400 5200 6850 5200
Wire Wire Line
	5200 5200 5400 5200
Wire Wire Line
	5200 4750 5200 5200
Connection ~ 5200 5200
Wire Wire Line
	8650 3050 8900 3050
Wire Wire Line
	8650 4300 8900 4300
$EndSCHEMATC
