EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3450 3300 0    50   Input ~ 0
Input
$Comp
L power:GND #PWR?
U 1 1 5EBF29FA
P 3050 4550
AR Path="/5EB41214/5EBF29FA" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5EBF29FA" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5EBF29FA" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5EBF29FA" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5EBF29FA" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5EBF29FA" Ref="#PWR036"  Part="1" 
F 0 "#PWR036" H 3050 4300 50  0001 C CNN
F 1 "GND" H 3055 4377 50  0000 C CNN
F 2 "" H 3050 4550 50  0001 C CNN
F 3 "" H 3050 4550 50  0001 C CNN
	1    3050 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 4400 3050 4550
Wire Wire Line
	3050 4550 3250 4550
Wire Wire Line
	2950 4000 3050 4000
Wire Wire Line
	3250 4450 3250 4550
Connection ~ 3250 4000
Wire Wire Line
	3250 4000 3650 4000
Wire Wire Line
	3050 4200 3050 4000
Connection ~ 3050 4000
Wire Wire Line
	3050 4000 3250 4000
Connection ~ 3050 4550
Wire Wire Line
	3050 2850 3050 2900
$Comp
L Device:R R?
U 1 1 5EE9A4F8
P 2800 2450
AR Path="/5EE9A4F8" Ref="R?"  Part="1" 
AR Path="/5EB41214/5EE9A4F8" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5EE9A4F8" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5EE9A4F8" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5EE9A4F8" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5EE9A4F8" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5EE9A4F8" Ref="R23"  Part="1" 
F 0 "R23" V 3007 2450 50  0000 C CNN
F 1 "R 52.3K" V 2916 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2730 2450 50  0001 C CNN
F 3 "~" H 2800 2450 50  0001 C CNN
F 4 "0.1%" H 2800 2450 50  0001 C CNN "Tolerance"
F 5 "P52.3KDACT-ND" H 2800 2450 50  0001 C CNN "Digi-Key_PN"
	1    2800 2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 2450 3050 2450
Wire Wire Line
	3250 2600 3250 2450
Connection ~ 3250 2450
Wire Wire Line
	3250 2450 3650 2450
Connection ~ 3050 2450
Wire Wire Line
	3050 2450 3250 2450
Wire Wire Line
	3050 2900 3150 2900
Wire Wire Line
	3250 2900 3150 2900
$Comp
L power:GND #PWR?
U 1 1 5EE9D143
P 3150 2900
AR Path="/5EB41214/5EE9D143" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5EE9D143" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5EE9D143" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5EE9D143" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5EE9D143" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5EE9D143" Ref="#PWR037"  Part="1" 
F 0 "#PWR037" H 3150 2650 50  0001 C CNN
F 1 "GND" H 3155 2727 50  0000 C CNN
F 2 "" H 3150 2900 50  0001 C CNN
F 3 "" H 3150 2900 50  0001 C CNN
	1    3150 2900
	1    0    0    -1  
$EndComp
Connection ~ 3150 2900
$Comp
L power:+5V #PWR032
U 1 1 5EE9E0FE
P 2550 2450
F 0 "#PWR032" H 2550 2300 50  0001 C CNN
F 1 "+5V" H 2565 2623 50  0000 C CNN
F 2 "" H 2550 2450 50  0001 C CNN
F 3 "" H 2550 2450 50  0001 C CNN
	1    2550 2450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR033
U 1 1 5EE9FBF4
P 2550 4000
F 0 "#PWR033" H 2550 3850 50  0001 C CNN
F 1 "+5V" H 2565 4173 50  0000 C CNN
F 2 "" H 2550 4000 50  0001 C CNN
F 3 "" H 2550 4000 50  0001 C CNN
	1    2550 4000
	1    0    0    -1  
$EndComp
Text HLabel 6250 3850 2    50   Input ~ 0
T_ARM_alarm_hi
Text Notes 3200 2000 0    50   ~ 0
14.7C setpoint
Text Notes 2150 4250 0    50   ~ 0
29.7C setpoint
Wire Wire Line
	4000 3200 4100 3200
Wire Wire Line
	4600 2650 4800 2650
Wire Wire Line
	4600 3800 4800 3800
$Comp
L power:+5V #PWR039
U 1 1 5EC8466A
P 4400 2950
F 0 "#PWR039" H 4400 2800 50  0001 C CNN
F 1 "+5V" H 4415 3123 50  0000 C CNN
F 2 "" H 4400 2950 50  0001 C CNN
F 3 "" H 4400 2950 50  0001 C CNN
	1    4400 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2950 4400 3000
$Comp
L power:GND #PWR?
U 1 1 5EC875DD
P 4300 3450
AR Path="/5EB41214/5EC875DD" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5EC875DD" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5EC875DD" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5EC875DD" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5EC875DD" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5EC875DD" Ref="#PWR038"  Part="1" 
F 0 "#PWR038" H 4300 3200 50  0001 C CNN
F 1 "GND" H 4305 3277 50  0000 C CNN
F 2 "" H 4300 3450 50  0001 C CNN
F 3 "" H 4300 3450 50  0001 C CNN
	1    4300 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3400 4300 3450
Text Notes 3150 2150 0    50   ~ 0
13Hz low pass
Text Notes 2150 4350 0    50   ~ 0
5.3Hz low pass
Text GLabel 4000 3200 0    50   Input ~ 0
!ARM
Text GLabel 5350 1800 2    50   Input ~ 0
!T_arm_low
Text Notes 3050 1750 0    50   ~ 0
Comparison (+)      :   15.2C (1.159V)\nNTC input  (-)       :   22C (0.939V)\nComparator output   : High(False)
$Comp
L logic_gates_magne:74LVC2G32 U17
U 1 1 5ECE38A3
P 5100 2600
F 0 "U17" H 5075 2867 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 5075 2776 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 5250 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 5300 2600 50  0001 C CNN
F 4 "296-13268-1-ND" H 5100 2600 50  0001 C CNN "Digi-Key_PN"
	1    5100 2600
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U17
U 2 1 5ECE4164
P 5100 3850
F 0 "U17" H 5075 4117 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 5075 4026 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 5250 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 5300 3850 50  0001 C CNN
F 4 "296-13268-1-ND" H 5100 3850 50  0001 C CNN "Digi-Key_PN"
	2    5100 3850
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U17
U 3 1 5ECE4946
P 4950 5550
F 0 "U17" H 4800 5250 50  0000 L CNN
F 1 "SN74LVC2G32DCUR" H 4400 5150 50  0000 L CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 5100 5600 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 5150 5550 50  0001 C CNN
F 4 "296-13268-1-ND" H 4950 5550 50  0001 C CNN "Digi-Key_PN"
	3    4950 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5ECE84F6
P 5200 5700
AR Path="/5EB41214/5ECE84F6" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ECE84F6" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5ECE84F6" Ref="C26"  Part="1" 
F 0 "C26" H 5292 5746 50  0000 L CNN
F 1 "C 100n" H 5292 5655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5200 5700 50  0001 C CNN
F 3 "~" H 5200 5700 50  0001 C CNN
F 4 "16" H 5200 5700 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5200 5700 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 5200 5700 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 5200 5700 50  0001 C CNN "Farnell_PN"
	1    5200 5700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR034
U 1 1 5ECE8925
P 3000 5300
F 0 "#PWR034" H 3000 5150 50  0001 C CNN
F 1 "+5V" H 3015 5473 50  0000 C CNN
F 2 "" H 3000 5300 50  0001 C CNN
F 3 "" H 3000 5300 50  0001 C CNN
	1    3000 5300
	1    0    0    -1  
$EndComp
Text Notes 3400 4900 0    50   ~ 0
NTC input  (+)       :   22C (0.939V)\nComparison (-)      :   30.5C (0.719V)\nComparator output   :  High (False)
$Comp
L dk_Logic-Gates-and-Inverters:SN74LVC1G14DBVR U16
U 1 1 5EC6F18C
P 4400 3200
F 0 "U16" H 4644 3253 60  0000 L CNN
F 1 "SN74LVC1G14DBVR" H 4550 3100 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 4600 3400 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 4600 3500 60  0001 L CNN
F 4 "296-11607-1-ND" H 4600 3600 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G14DBVR" H 4600 3700 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 4600 3800 60  0001 L CNN "Category"
F 7 "Logic - Gates and Inverters" H 4600 3900 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 4600 4000 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G14DBVR/296-11607-1-ND/385746" H 4600 4100 60  0001 L CNN "DK_Detail_Page"
F 10 "IC INVERTER SCHMITT 1CH SOT23-5" H 4600 4200 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 4600 4300 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4600 4400 60  0001 L CNN "Status"
	1    4400 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3900 6050 3850
Wire Wire Line
	6050 3850 6250 3850
$Comp
L Device:R R?
U 1 1 5F8B63B3
P 5850 3850
AR Path="/5F8B63B3" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F8B63B3" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F8B63B3" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5F8B63B3" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5F8B63B3" Ref="R?"  Part="1" 
AR Path="/5EED7553/5F8B63B3" Ref="R?"  Part="1" 
AR Path="/5EEDC7D6/5F8B63B3" Ref="R?"  Part="1" 
AR Path="/5ECDAC29/5F8B63B3" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5F8B63B3" Ref="R30"  Part="1" 
F 0 "R30" V 5643 3850 50  0000 C CNN
F 1 "R 10" V 5734 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5780 3850 50  0001 C CNN
F 3 "~" H 5850 3850 50  0001 C CNN
F 4 "" H 5850 3850 50  0001 C CNN "Tolerance"
F 5 "RMCF0603FT10R0CT-ND" H 5850 3850 50  0001 C CNN "Digi-Key_PN"
	1    5850 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 3850 6050 3850
Connection ~ 6050 3850
Wire Wire Line
	6050 4100 6050 4550
Wire Wire Line
	5250 1850 5250 1800
Wire Wire Line
	5250 1800 5350 1800
$Comp
L Device:R R?
U 1 1 5F8C7C61
P 5050 1800
AR Path="/5F8C7C61" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F8C7C61" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F8C7C61" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5F8C7C61" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5F8C7C61" Ref="R?"  Part="1" 
AR Path="/5EED7553/5F8C7C61" Ref="R?"  Part="1" 
AR Path="/5EEDC7D6/5F8C7C61" Ref="R?"  Part="1" 
AR Path="/5ECDAC29/5F8C7C61" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5F8C7C61" Ref="R28"  Part="1" 
F 0 "R28" V 4843 1800 50  0000 C CNN
F 1 "R 10" V 4934 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4980 1800 50  0001 C CNN
F 3 "~" H 5050 1800 50  0001 C CNN
F 4 "" H 5050 1800 50  0001 C CNN "Tolerance"
F 5 "RMCF0603FT10R0CT-ND" H 5050 1800 50  0001 C CNN "Digi-Key_PN"
	1    5050 1800
	0    1    1    0   
$EndComp
Wire Wire Line
	5200 1800 5250 1800
Connection ~ 5250 1800
$Comp
L power:GND #PWR?
U 1 1 5F8DEA70
P 5250 2050
AR Path="/5EB41214/5F8DEA70" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5F8DEA70" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5F8DEA70" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5F8DEA70" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5F8DEA70" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5F8DEA70" Ref="#PWR040"  Part="1" 
F 0 "#PWR040" H 5250 1800 50  0001 C CNN
F 1 "GND" H 5255 1877 50  0000 C CNN
F 2 "" H 5250 2050 50  0001 C CNN
F 3 "" H 5250 2050 50  0001 C CNN
	1    5250 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2550 4800 1800
Wire Wire Line
	4800 1800 4900 1800
$Comp
L Device:C_Small C?
U 1 1 5F90ABB7
P 6050 4000
AR Path="/5EB41214/5F90ABB7" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F90ABB7" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F90ABB7" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F90ABB7" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5F90ABB7" Ref="C?"  Part="1" 
AR Path="/5ECA36FF/5F90ABB7" Ref="C?"  Part="1" 
AR Path="/5EED7553/5F90ABB7" Ref="C?"  Part="1" 
AR Path="/5EEDC7D6/5F90ABB7" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5F90ABB7" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F90ABB7" Ref="C29"  Part="1" 
F 0 "C29" H 6142 4046 50  0000 L CNN
F 1 "C 470p" H 6142 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6050 4000 50  0001 C CNN
F 3 "~" H 6050 4000 50  0001 C CNN
F 4 "16" H 6050 4000 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 6050 4000 50  0001 C CNN "Tolerance"
F 6 "399-10051-1-ND" H 6050 4000 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 6050 4000 50  0001 C CNN "Farnell_PN"
	1    6050 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F91B9DD
P 5250 1950
AR Path="/5EB41214/5F91B9DD" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F91B9DD" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F91B9DD" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F91B9DD" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5F91B9DD" Ref="C?"  Part="1" 
AR Path="/5ECA36FF/5F91B9DD" Ref="C?"  Part="1" 
AR Path="/5EED7553/5F91B9DD" Ref="C?"  Part="1" 
AR Path="/5EEDC7D6/5F91B9DD" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5F91B9DD" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F91B9DD" Ref="C27"  Part="1" 
F 0 "C27" H 5342 1996 50  0000 L CNN
F 1 "C 470p" H 5342 1905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5250 1950 50  0001 C CNN
F 3 "~" H 5250 1950 50  0001 C CNN
F 4 "16" H 5250 1950 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5250 1950 50  0001 C CNN "Tolerance"
F 6 "399-10051-1-ND" H 5250 1950 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 5250 1950 50  0001 C CNN "Farnell_PN"
	1    5250 1950
	1    0    0    -1  
$EndComp
$Comp
L comparators_magne:TLV3202 U15
U 1 1 5F9A1C50
P 3750 2550
F 0 "U15" H 3950 2917 50  0000 C CNN
F 1 "TLV3202" H 3950 2826 50  0000 C CNN
F 2 "Package_SO:TSSOP-8_3x3mm_P0.65mm" H 3750 2550 50  0001 C CNN
F 3 "" H 3750 2550 50  0001 C CNN
F 4 "296-39262-1-ND" H 3750 2550 50  0001 C CNN "Digi-Key_PN"
	1    3750 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2450 2650 2450
$Comp
L comparators_magne:TLV3202 U15
U 2 1 5F9B955C
P 3750 3900
F 0 "U15" H 3950 4267 50  0000 C CNN
F 1 "TLV3202" H 3950 4176 50  0000 C CNN
F 2 "Package_SO:TSSOP-8_3x3mm_P0.65mm" H 3750 3900 50  0001 C CNN
F 3 "" H 3750 3900 50  0001 C CNN
F 4 "296-39262-1-ND" H 3750 3900 50  0001 C CNN "Digi-Key_PN"
	2    3750 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 4000 2650 4000
Connection ~ 4600 3200
Wire Wire Line
	4600 3200 4600 3800
Wire Wire Line
	4600 2650 4600 3200
Wire Wire Line
	4250 2550 4800 2550
Connection ~ 4800 2550
Connection ~ 3250 4550
Wire Wire Line
	3650 2650 3650 3300
Wire Wire Line
	3450 3300 3650 3300
Connection ~ 3650 3300
Wire Wire Line
	3650 3300 3650 3800
Wire Wire Line
	5350 2600 5450 2600
$Comp
L Device:C_Small C?
U 1 1 5F8ABD22
P 5800 2750
AR Path="/5EB41214/5F8ABD22" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F8ABD22" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F8ABD22" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F8ABD22" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5F8ABD22" Ref="C?"  Part="1" 
AR Path="/5ECA36FF/5F8ABD22" Ref="C?"  Part="1" 
AR Path="/5EED7553/5F8ABD22" Ref="C?"  Part="1" 
AR Path="/5EEDC7D6/5F8ABD22" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5F8ABD22" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F8ABD22" Ref="C28"  Part="1" 
F 0 "C28" H 5892 2796 50  0000 L CNN
F 1 "C 470p" H 5892 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5800 2750 50  0001 C CNN
F 3 "~" H 5800 2750 50  0001 C CNN
F 4 "16" H 5800 2750 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5800 2750 50  0001 C CNN "Tolerance"
F 6 "399-10051-1-ND" H 5800 2750 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 5800 2750 50  0001 C CNN "Farnell_PN"
	1    5800 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 2850 5800 3150
Connection ~ 5800 2600
Wire Wire Line
	5750 2600 5800 2600
$Comp
L Device:R R?
U 1 1 5F8ABD2C
P 5600 2600
AR Path="/5F8ABD2C" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F8ABD2C" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F8ABD2C" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5F8ABD2C" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5F8ABD2C" Ref="R?"  Part="1" 
AR Path="/5EED7553/5F8ABD2C" Ref="R?"  Part="1" 
AR Path="/5EEDC7D6/5F8ABD2C" Ref="R?"  Part="1" 
AR Path="/5ECDAC29/5F8ABD2C" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5F8ABD2C" Ref="R29"  Part="1" 
F 0 "R29" V 5393 2600 50  0000 C CNN
F 1 "R 10" V 5484 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5530 2600 50  0001 C CNN
F 3 "~" H 5600 2600 50  0001 C CNN
F 4 "" H 5600 2600 50  0001 C CNN "Tolerance"
F 5 "RMCF0603FT10R0CT-ND" H 5600 2600 50  0001 C CNN "Digi-Key_PN"
	1    5600 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 2600 5950 2600
Wire Wire Line
	5800 2650 5800 2600
Text HLabel 5950 2600 2    50   Input ~ 0
T_ARM_alarm_lo
Wire Wire Line
	5350 3850 5700 3850
$Comp
L power:GND #PWR?
U 1 1 5F8DD37F
P 5800 3150
AR Path="/5EB41214/5F8DD37F" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5F8DD37F" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5F8DD37F" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5F8DD37F" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5F8DD37F" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5F8DD37F" Ref="#PWR041"  Part="1" 
F 0 "#PWR041" H 5800 2900 50  0001 C CNN
F 1 "GND" H 5805 2977 50  0000 C CNN
F 2 "" H 5800 3150 50  0001 C CNN
F 3 "" H 5800 3150 50  0001 C CNN
	1    5800 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F95B697
P 3250 2750
AR Path="/5F95B697" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F95B697" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F95B697" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5F95B697" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5F95B697" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5F95B697" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5F95B697" Ref="R25"  Part="1" 
F 0 "R25" H 3450 2700 50  0000 C CNN
F 1 "R 16K" H 3400 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3180 2750 50  0001 C CNN
F 3 "~" H 3250 2750 50  0001 C CNN
F 4 "1%" H 3250 2750 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 3250 2750 50  0001 C CNN "Digi-Key_PN"
	1    3250 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 4150 3250 4000
$Comp
L Device:R R?
U 1 1 5F95BEA8
P 3250 4300
AR Path="/5F95BEA8" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F95BEA8" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F95BEA8" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5F95BEA8" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5F95BEA8" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5F95BEA8" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5F95BEA8" Ref="R26"  Part="1" 
F 0 "R26" V 3457 4300 50  0000 C CNN
F 1 "R 16K" V 3366 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3180 4300 50  0001 C CNN
F 3 "~" H 3250 4300 50  0001 C CNN
F 4 "1%" H 3250 4300 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 3250 4300 50  0001 C CNN "Digi-Key_PN"
	1    3250 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F95C023
P 2800 4000
AR Path="/5F95C023" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F95C023" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F95C023" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5F95C023" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5F95C023" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5F95C023" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5F95C023" Ref="R24"  Part="1" 
F 0 "R24" V 3007 4000 50  0000 C CNN
F 1 "R 93.1K" V 2916 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2730 4000 50  0001 C CNN
F 3 "~" H 2800 4000 50  0001 C CNN
F 4 "0.1%" H 2800 4000 50  0001 C CNN "Tolerance"
F 5 "P93.1KDACT-ND" H 2800 4000 50  0001 C CNN "Digi-Key_PN"
	1    2800 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	3050 2650 3050 2450
$Comp
L Device:C_Small C?
U 1 1 5F97D805
P 3050 4300
AR Path="/5EB41214/5F97D805" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F97D805" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F97D805" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F97D805" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5F97D805" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F97D805" Ref="C23"  Part="1" 
AR Path="/5ED08686/5F97D805" Ref="C?"  Part="1" 
F 0 "C23" H 3142 4346 50  0000 L CNN
F 1 "C 2.2u" H 3142 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3050 4300 50  0001 C CNN
F 3 "~" H 3050 4300 50  0001 C CNN
F 4 "16" H 3050 4300 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 3050 4300 50  0001 C CNN "Tolerance"
F 6 "490-4787-1-ND" H 3050 4300 50  0001 C CNN "Digi-Key_PN"
F 7 "1458904" H 3050 4300 50  0001 C CNN "Farnell_PN"
	1    3050 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F97E750
P 3050 2750
AR Path="/5EB41214/5F97E750" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F97E750" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F97E750" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F97E750" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5F97E750" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F97E750" Ref="C22"  Part="1" 
AR Path="/5ED08686/5F97E750" Ref="C?"  Part="1" 
F 0 "C22" H 2850 2800 50  0000 L CNN
F 1 "C 1u" H 2750 2700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3050 2750 50  0001 C CNN
F 3 "~" H 3050 2750 50  0001 C CNN
F 4 "16" H 3050 2750 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 3050 2750 50  0001 C CNN "Tolerance"
F 6 "311-1365-1-ND" H 3050 2750 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 3050 2750 50  0001 C CNN "Farnell_PN"
	1    3050 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FA7C2D0
P 3500 5700
AR Path="/5EB41214/5FA7C2D0" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FA7C2D0" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FA7C2D0" Ref="C24"  Part="1" 
F 0 "C24" H 3592 5746 50  0000 L CNN
F 1 "C 100n" H 3592 5655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3500 5700 50  0001 C CNN
F 3 "~" H 3500 5700 50  0001 C CNN
F 4 "16" H 3500 5700 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 3500 5700 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 3500 5700 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 3500 5700 50  0001 C CNN "Farnell_PN"
	1    3500 5700
	1    0    0    -1  
$EndComp
$Comp
L comparators_magne:TLV3202 U15
U 3 1 5FA83B17
P 3000 5700
F 0 "U15" H 3044 5796 50  0000 L CNN
F 1 "TLV3202" H 3044 5705 50  0000 L CNN
F 2 "Package_SO:TSSOP-8_3x3mm_P0.65mm" H 3000 5700 50  0001 C CNN
F 3 "" H 3000 5700 50  0001 C CNN
F 4 "296-39262-1-ND" H 3000 5700 50  0001 C CNN "Digi-Key_PN"
	3    3000 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 5800 3500 5800
Connection ~ 3500 5800
Wire Wire Line
	3500 5800 4950 5800
Connection ~ 4950 5800
Wire Wire Line
	4950 5800 5200 5800
Wire Wire Line
	3000 5500 3000 5300
Wire Wire Line
	3000 5300 3500 5300
Wire Wire Line
	5200 5300 5200 5600
Connection ~ 4950 5300
Wire Wire Line
	4950 5300 5200 5300
Wire Wire Line
	3500 5600 3500 5300
Connection ~ 3500 5300
Wire Wire Line
	3500 5300 4950 5300
Connection ~ 3000 5300
$Comp
L power:GND #PWR?
U 1 1 5FA8BB27
P 3000 5800
AR Path="/5EB41214/5FA8BB27" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5FA8BB27" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5FA8BB27" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5FA8BB27" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5FA8BB27" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5FA8BB27" Ref="#PWR035"  Part="1" 
F 0 "#PWR035" H 3000 5550 50  0001 C CNN
F 1 "GND" H 3005 5627 50  0000 C CNN
F 2 "" H 3000 5800 50  0001 C CNN
F 3 "" H 3000 5800 50  0001 C CNN
	1    3000 5800
	1    0    0    -1  
$EndComp
Connection ~ 3000 5800
Wire Wire Line
	3250 4550 5150 4550
Text GLabel 5450 4200 2    50   Input ~ 0
!T_arm_high
Wire Wire Line
	5150 4250 5150 4200
$Comp
L Device:R R?
U 1 1 5FBEAA53
P 4950 4200
AR Path="/5FBEAA53" Ref="R?"  Part="1" 
AR Path="/5EB41214/5FBEAA53" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5FBEAA53" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5FBEAA53" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5FBEAA53" Ref="R?"  Part="1" 
AR Path="/5EED7553/5FBEAA53" Ref="R?"  Part="1" 
AR Path="/5EEDC7D6/5FBEAA53" Ref="R?"  Part="1" 
AR Path="/5ECDAC29/5FBEAA53" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5FBEAA53" Ref="R27"  Part="1" 
F 0 "R27" V 4743 4200 50  0000 C CNN
F 1 "R 10" V 4834 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4880 4200 50  0001 C CNN
F 3 "~" H 4950 4200 50  0001 C CNN
F 4 "" H 4950 4200 50  0001 C CNN "Tolerance"
F 5 "RMCF0603FT10R0CT-ND" H 4950 4200 50  0001 C CNN "Digi-Key_PN"
	1    4950 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 4200 5150 4200
Connection ~ 5150 4200
$Comp
L Device:C_Small C?
U 1 1 5FBEAA65
P 5150 4350
AR Path="/5EB41214/5FBEAA65" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FBEAA65" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FBEAA65" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FBEAA65" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5FBEAA65" Ref="C?"  Part="1" 
AR Path="/5ECA36FF/5FBEAA65" Ref="C?"  Part="1" 
AR Path="/5EED7553/5FBEAA65" Ref="C?"  Part="1" 
AR Path="/5EEDC7D6/5FBEAA65" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5FBEAA65" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FBEAA65" Ref="C25"  Part="1" 
F 0 "C25" H 5242 4396 50  0000 L CNN
F 1 "C 470p" H 5242 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5150 4350 50  0001 C CNN
F 3 "~" H 5150 4350 50  0001 C CNN
F 4 "16" H 5150 4350 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5150 4350 50  0001 C CNN "Tolerance"
F 6 "399-10051-1-ND" H 5150 4350 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 5150 4350 50  0001 C CNN "Farnell_PN"
	1    5150 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 4200 5450 4200
Wire Wire Line
	5150 4450 5150 4550
Connection ~ 5150 4550
Wire Wire Line
	5150 4550 6050 4550
Wire Wire Line
	4800 4200 4800 3900
Connection ~ 4800 3900
Wire Wire Line
	4250 3900 4800 3900
$EndSCHEMATC
