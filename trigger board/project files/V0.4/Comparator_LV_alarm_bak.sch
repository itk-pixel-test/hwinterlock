EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 26 30
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5ECAD0C8
P 6050 2550
AR Path="/5ECAD0C8" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ECAD0C8" Ref="R66"  Part="1" 
AR Path="/5ED2BC2A/5ECAD0C8" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ECAD0C8" Ref="R?"  Part="1" 
AR Path="/5EBFB816/5ECAD0C8" Ref="R?"  Part="1" 
F 0 "R66" V 6257 2550 50  0000 C CNN
F 1 "R 10K " V 6166 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5980 2550 50  0001 C CNN
F 3 "~" H 6050 2550 50  0001 C CNN
F 4 "0.1%" V 6050 2550 50  0001 C CNN "Tolerance"
	1    6050 2550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 2600 6650 2550
$Comp
L Device:C_Small C62
U 1 1 5ECAD0C5
P 9550 3450
AR Path="/5EB41214/5ECAD0C5" Ref="C62"  Part="1" 
AR Path="/5ED2BC2A/5ECAD0C5" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ECAD0C5" Ref="C?"  Part="1" 
AR Path="/5EBFB816/5ECAD0C5" Ref="C?"  Part="1" 
F 0 "C62" H 9642 3496 50  0000 L CNN
F 1 "C 47n" H 9642 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9550 3450 50  0001 C CNN
F 3 "~" H 9550 3450 50  0001 C CNN
F 4 "16" H 9550 3450 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 9550 3450 50  0001 C CNN "Tolerance"
	1    9550 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 3300 9550 3350
$Comp
L Device:C_Small C58
U 1 1 5EE8A62C
P 6900 3450
AR Path="/5EB41214/5EE8A62C" Ref="C58"  Part="1" 
AR Path="/5ED2BC2A/5EE8A62C" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5EE8A62C" Ref="C?"  Part="1" 
AR Path="/5EBFB816/5EE8A62C" Ref="C?"  Part="1" 
F 0 "C58" H 6992 3496 50  0000 L CNN
F 1 "C 47n" H 6992 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6900 3450 50  0001 C CNN
F 3 "~" H 6900 3450 50  0001 C CNN
F 4 "16" H 6900 3450 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 6900 3450 50  0001 C CNN "Tolerance"
	1    6900 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 3550 6900 3550
Wire Wire Line
	6900 3200 6900 3350
Wire Wire Line
	6650 3200 6650 3550
Wire Wire Line
	6200 2550 6250 2550
Wire Wire Line
	6450 2800 6250 2800
Wire Wire Line
	6250 2800 6250 2550
Connection ~ 6250 2550
Connection ~ 6650 2550
Wire Wire Line
	6650 2550 6650 2250
$Comp
L comparators_magne:MAX9141 U42
U 1 1 5ECAD0C6
P 6750 2900
AR Path="/5EB41214/5ECAD0C6" Ref="U42"  Part="1" 
AR Path="/5ED2BC2A/5ECAD0C6" Ref="U?"  Part="1" 
AR Path="/5EE0AC5D/5ECAD0C6" Ref="U?"  Part="1" 
AR Path="/5EBFB816/5ECAD0C6" Ref="U?"  Part="1" 
F 0 "U42" H 6850 3150 50  0000 L CNN
F 1 "MAX9141ESA+-ND" H 6850 3050 50  0000 L CNN
F 2 "digikey-footprints:SOIC-8_W3.9mm" H 6750 2900 50  0001 C CNN
F 3 "" H 6750 2900 50  0001 C CNN
	1    6750 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3200 6750 3200
Wire Wire Line
	9650 2950 9950 2950
Wire Wire Line
	6800 1450 6800 2600
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:SN74LVC1G07DBVR U?
U 1 1 5EEE408A
P 8450 2900
AR Path="/5ED2BC2A/5EEE408A" Ref="U?"  Part="1" 
AR Path="/5EB41214/5EEE408A" Ref="U44"  Part="1" 
F 0 "U44" H 8550 2550 60  0000 L CNN
F 1 "SN74LVC1G07DBVR" H 8550 2450 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 8650 3100 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g07" H 8650 3200 60  0001 L CNN
F 4 "296-8485-1-ND" H 8650 3300 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G07DBVR" H 8650 3400 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 8650 3500 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 8650 3600 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g07" H 8650 3700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G07DBVR/296-8485-1-ND/377454" H 8650 3800 60  0001 L CNN "DK_Detail_Page"
F 10 "IC BUF NON-INVERT 5.5V SOT23-5" H 8650 3900 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 8650 4000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8650 4100 60  0001 L CNN "Status"
	1    8450 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3200 8450 3550
Wire Wire Line
	8450 2500 8450 2600
$Comp
L Device:C_Small C60
U 1 1 5EEE4094
P 7650 3200
AR Path="/5EB41214/5EEE4094" Ref="C60"  Part="1" 
AR Path="/5ED2BC2A/5EEE4094" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5EEE4094" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5EEE4094" Ref="C?"  Part="1" 
F 0 "C60" H 7742 3246 50  0000 L CNN
F 1 "C 1u" H 7742 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7650 3200 50  0001 C CNN
F 3 "~" H 7650 3200 50  0001 C CNN
F 4 "16" H 7650 3200 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7650 3200 50  0001 C CNN "Tolerance"
	1    7650 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EEE409A
P 7500 2900
AR Path="/5EEE409A" Ref="R?"  Part="1" 
AR Path="/5EB41214/5EEE409A" Ref="R70"  Part="1" 
AR Path="/5ED2BC2A/5EEE409A" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5EEE409A" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5EEE409A" Ref="R?"  Part="1" 
F 0 "R70" V 7293 2900 50  0000 C CNN
F 1 "R 150K" V 7384 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7430 2900 50  0001 C CNN
F 3 "~" H 7500 2900 50  0001 C CNN
F 4 "1%" H 7500 2900 50  0001 C CNN "Tolerance"
	1    7500 2900
	0    1    1    0   
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5EEE40A0
P 7500 2500
AR Path="/5ED2BC2A/5EEE40A0" Ref="D?"  Part="1" 
AR Path="/5EB41214/5EEE40A0" Ref="D16"  Part="1" 
F 0 "D16" H 7500 2717 50  0000 C CNN
F 1 "CUS520,H3F" H 7500 2626 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 7500 2500 50  0001 C CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=7041&prodName=CUS520" H 7500 2500 50  0001 C CNN
	1    7500 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 2500 7650 2900
Connection ~ 7650 2900
Wire Wire Line
	7650 2900 8150 2900
Wire Wire Line
	7650 2900 7650 3100
Wire Wire Line
	7050 2900 7350 2900
Text Notes 7650 2150 0    50   ~ 0
200ms delay
Wire Wire Line
	8750 2900 8900 2900
Wire Wire Line
	6900 3550 7650 3550
Connection ~ 6900 3550
Connection ~ 8450 3550
Wire Notes Line
	7150 2200 7150 3350
Wire Notes Line
	7150 3350 8750 3350
Wire Notes Line
	8750 3350 8750 2200
Wire Notes Line
	8750 2200 7150 2200
Wire Wire Line
	7650 3300 7650 3550
Connection ~ 7650 3550
Wire Wire Line
	7650 3550 8450 3550
Text HLabel 9950 2950 2    50   Input ~ 0
Isens_interlock
Wire Wire Line
	5050 3000 6450 3000
Wire Wire Line
	8900 1150 8900 2150
$Sheet
S 9000 2050 550  200 
U 5F076AF2
F0 "RED/GREEN LED indicator" 50
F1 "R_G_LED_indicator_LVsens.sch" 50
F2 "Cntrl" I L 9000 2150 50 
$EndSheet
Wire Wire Line
	9000 2150 8900 2150
Wire Wire Line
	5500 1400 5500 1150
Wire Wire Line
	5500 1150 8900 1150
Wire Wire Line
	6250 2550 6300 2550
Wire Wire Line
	6600 2550 6650 2550
$Comp
L Device:R R?
U 1 1 5ECAD0C7
P 6450 2550
AR Path="/5ECAD0C7" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ECAD0C7" Ref="R68"  Part="1" 
AR Path="/5ED2BC2A/5ECAD0C7" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ECAD0C7" Ref="R?"  Part="1" 
AR Path="/5EBFB816/5ECAD0C7" Ref="R?"  Part="1" 
F 0 "R68" V 6243 2550 50  0000 C CNN
F 1 "R" V 6334 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6380 2550 50  0001 C CNN
F 3 "~" H 6450 2550 50  0001 C CNN
F 4 "1%" H 6450 2550 50  0001 C CNN "Tolerance"
	1    6450 2550
	0    1    1    0   
$EndComp
Text GLabel 9100 3200 0    50   Input ~ 0
!ARM_LVsens
Text GLabel 5500 1500 0    50   Input ~ 0
!LE_LVsens
Wire Wire Line
	9100 3000 9100 3200
Wire Wire Line
	8900 2150 8900 2900
Connection ~ 8900 2150
Connection ~ 8900 2900
Wire Wire Line
	8900 2900 9100 2900
Text Notes 4600 2900 0    50   ~ 0
What to set as limit? Chip abs max is 4A.\nExpected consumption is 1.1A.
Connection ~ 7350 2900
Wire Wire Line
	7350 2500 7350 2900
$Comp
L power:+5VA #PWR0129
U 1 1 5EC9E1F7
P 6650 2250
F 0 "#PWR0129" H 6650 2100 50  0001 C CNN
F 1 "+5VA" H 6665 2423 50  0000 C CNN
F 2 "" H 6650 2250 50  0001 C CNN
F 3 "" H 6650 2250 50  0001 C CNN
	1    6650 2250
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR0135
U 1 1 5EC9E437
P 8450 2500
F 0 "#PWR0135" H 8450 2350 50  0001 C CNN
F 1 "+5VA" H 8465 2673 50  0000 C CNN
F 2 "" H 8450 2500 50  0001 C CNN
F 3 "" H 8450 2500 50  0001 C CNN
	1    8450 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR0133
U 1 1 5EC9E68C
P 6900 3200
F 0 "#PWR0133" H 6900 3050 50  0001 C CNN
F 1 "+5VA" H 6915 3373 50  0000 C CNN
F 2 "" H 6900 3200 50  0001 C CNN
F 3 "" H 6900 3200 50  0001 C CNN
	1    6900 3200
	1    0    0    -1  
$EndComp
Connection ~ 6900 3200
$Comp
L power:+5VA #PWR0139
U 1 1 5ECA8A8C
P 9550 3300
F 0 "#PWR0139" H 9550 3150 50  0001 C CNN
F 1 "+5VA" H 9565 3473 50  0000 C CNN
F 2 "" H 9550 3300 50  0001 C CNN
F 3 "" H 9550 3300 50  0001 C CNN
	1    9550 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR0130
U 1 1 5ECAA11B
P 6650 3550
F 0 "#PWR0130" H 6650 3300 50  0001 C CNN
F 1 "GND1" H 6655 3377 50  0000 C CNN
F 2 "" H 6650 3550 50  0001 C CNN
F 3 "" H 6650 3550 50  0001 C CNN
	1    6650 3550
	1    0    0    -1  
$EndComp
Connection ~ 6650 3550
$Comp
L power:GND1 #PWR0127
U 1 1 5ECAA248
P 5900 2550
F 0 "#PWR0127" H 5900 2300 50  0001 C CNN
F 1 "GND1" H 5905 2377 50  0000 C CNN
F 2 "" H 5900 2550 50  0001 C CNN
F 3 "" H 5900 2550 50  0001 C CNN
	1    5900 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3550 9550 3550
$Comp
L logic_gates_magne:74LVC2G32 U41
U 1 1 5ECD3981
P 6050 1450
F 0 "U41" H 6025 1717 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 6025 1626 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 6200 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 6250 1450 50  0001 C CNN
	1    6050 1450
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U41
U 2 1 5ECD45CA
P 9400 2950
F 0 "U41" H 9375 3217 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 9375 3126 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 9550 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 9600 2950 50  0001 C CNN
	2    9400 2950
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U41
U 3 1 5ECD4E7B
P 10000 3300
F 0 "U41" H 10053 3346 50  0000 L CNN
F 1 "SN74LVC2G32DCUR" H 10053 3255 50  0000 L CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 10150 3350 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 10200 3300 50  0001 C CNN
	3    10000 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 3300 9800 3300
Wire Wire Line
	9800 3300 9800 3050
Wire Wire Line
	9800 3050 10000 3050
Wire Wire Line
	5500 1500 5750 1500
Wire Wire Line
	5500 1400 5750 1400
Connection ~ 9550 3300
Wire Wire Line
	9550 3550 10000 3550
Connection ~ 9550 3550
Wire Wire Line
	6300 1450 6800 1450
$Comp
L Device:C_Small C?
U 1 1 5ED0D1FD
P 3850 6300
AR Path="/5EB41214/5ED0D1FD" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ED0D1FD" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5ED0D1FD" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5ED0D1FD" Ref="C?"  Part="1" 
F 0 "C?" H 3942 6346 50  0000 L CNN
F 1 "C 47n" H 3942 6255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3850 6300 50  0001 C CNN
F 3 "~" H 3850 6300 50  0001 C CNN
F 4 "16" H 3850 6300 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 3850 6300 50  0001 C CNN "Tolerance"
	1    3850 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 6150 3850 6200
Wire Wire Line
	3600 6050 3600 6400
Wire Wire Line
	3700 6150 3700 6050
Wire Wire Line
	3850 6150 3700 6150
Wire Wire Line
	3950 6150 3850 6150
Connection ~ 3850 6150
Connection ~ 3850 6400
Wire Notes Line
	4050 5050 4050 6150
Wire Notes Line
	4050 6150 5750 6150
Wire Wire Line
	3600 6400 3850 6400
Wire Wire Line
	3500 6400 3600 6400
Connection ~ 3600 6400
Wire Wire Line
	2000 4250 2200 4250
Wire Wire Line
	6600 6150 6600 6200
Wire Wire Line
	2750 4300 3750 4300
$Comp
L Device:C_Small C?
U 1 1 5ED0D214
P 6600 6300
AR Path="/5EB41214/5ED0D214" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ED0D214" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5ED0D214" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5ED0D214" Ref="C?"  Part="1" 
F 0 "C?" H 6692 6346 50  0000 L CNN
F 1 "C 47n" H 6692 6255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6600 6300 50  0001 C CNN
F 3 "~" H 6600 6300 50  0001 C CNN
F 4 "16" H 6600 6300 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 6600 6300 50  0001 C CNN "Tolerance"
	1    6600 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 4000 2000 4250
Text GLabel 2100 4350 0    50   Input ~ 0
!LE_LVsens
Wire Wire Line
	2100 4350 2200 4350
Wire Wire Line
	2000 4000 6150 4000
Text HLabel 7100 5800 2    50   Input ~ 0
Vsens_interlock
Wire Wire Line
	6150 4850 6150 5750
Wire Wire Line
	6350 4850 6150 4850
Connection ~ 6150 4850
Wire Wire Line
	6150 4000 6150 4850
Wire Notes Line
	5750 5050 4050 5050
Wire Notes Line
	5750 6150 5750 5050
Text Notes 4600 5000 0    50   ~ 0
200ms delay
Wire Wire Line
	4000 5750 4300 5750
Wire Wire Line
	3850 6400 4600 6400
Connection ~ 4600 6400
Wire Wire Line
	4600 6150 4600 6400
Wire Wire Line
	4600 5750 4600 5950
Wire Wire Line
	4600 5750 5100 5750
Wire Wire Line
	4300 5350 4300 5750
Connection ~ 4600 5750
Wire Wire Line
	4600 5350 4600 5750
$Comp
L Device:D_Schottky D?
U 1 1 5ED0D22F
P 4450 5350
AR Path="/5ED2BC2A/5ED0D22F" Ref="D?"  Part="1" 
AR Path="/5EB41214/5ED0D22F" Ref="D?"  Part="1" 
F 0 "D?" H 4450 5567 50  0000 C CNN
F 1 "CUS520,H3F" H 4450 5476 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 4450 5350 50  0001 C CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=7041&prodName=CUS520" H 4450 5350 50  0001 C CNN
	1    4450 5350
	1    0    0    -1  
$EndComp
Connection ~ 4300 5750
$Comp
L Device:R R?
U 1 1 5ED0D237
P 4450 5750
AR Path="/5ED0D237" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED0D237" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED0D237" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5ED0D237" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5ED0D237" Ref="R?"  Part="1" 
F 0 "R?" V 4243 5750 50  0000 C CNN
F 1 "R 150K" V 4334 5750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4380 5750 50  0001 C CNN
F 3 "~" H 4450 5750 50  0001 C CNN
F 4 "1%" H 4450 5750 50  0001 C CNN "Tolerance"
	1    4450 5750
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5ED0D23F
P 4600 6050
AR Path="/5EB41214/5ED0D23F" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ED0D23F" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5ED0D23F" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5ED0D23F" Ref="C?"  Part="1" 
F 0 "C?" H 4692 6096 50  0000 L CNN
F 1 "C 1u" H 4692 6005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4600 6050 50  0001 C CNN
F 3 "~" H 4600 6050 50  0001 C CNN
F 4 "16" H 4600 6050 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 4600 6050 50  0001 C CNN "Tolerance"
	1    4600 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 5350 5400 5450
Wire Wire Line
	5250 5350 5400 5350
Wire Wire Line
	4600 6400 5400 6400
Wire Wire Line
	5400 6050 5400 6400
Wire Wire Line
	5700 5750 6150 5750
Text Notes 2400 5550 0    50   ~ 0
2.0V setpoint
Text GLabel 6150 5950 0    50   Input ~ 0
!ARM_LVsens
Wire Wire Line
	3750 4300 3750 5450
Wire Wire Line
	6700 5800 7100 5800
Wire Wire Line
	3600 5100 3600 5400
Wire Wire Line
	6150 5950 6150 5850
Wire Wire Line
	3450 5400 3600 5400
Wire Wire Line
	3050 5400 3150 5400
Wire Wire Line
	3050 5650 3050 5400
Wire Wire Line
	3400 5650 3050 5650
Connection ~ 3050 5400
Wire Wire Line
	2950 5400 3050 5400
Connection ~ 3600 5400
Wire Wire Line
	3600 5450 3600 5400
$Comp
L Device:R R?
U 1 1 5ED0D259
P 3300 5400
AR Path="/5ED0D259" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED0D259" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED0D259" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5ED0D259" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5ED0D259" Ref="R?"  Part="1" 
F 0 "R?" V 3507 5400 50  0000 C CNN
F 1 "R 15K" V 3416 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3230 5400 50  0001 C CNN
F 3 "~" H 3300 5400 50  0001 C CNN
F 4 "0.1%" H 3300 5400 50  0001 C CNN "Tolerance"
	1    3300 5400
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5ED0D260
P 2800 5400
AR Path="/5ED0D260" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED0D260" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED0D260" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5ED0D260" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5ED0D260" Ref="R?"  Part="1" 
F 0 "R?" V 2593 5400 50  0000 C CNN
F 1 "R 10K 0.1%" V 2684 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2730 5400 50  0001 C CNN
F 3 "~" H 2800 5400 50  0001 C CNN
F 4 "0.1%" H 2800 5400 50  0001 C CNN "Tolerance"
	1    2800 5400
	0    1    1    0   
$EndComp
$Sheet
S 6350 4750 550  200 
U 5ED0D263
F0 "sheet5ED0D1F5" 50
F1 "R_G_LED_indicator_LVsens.sch" 50
F2 "Cntrl" I L 6350 4850 50 
$EndSheet
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:SN74LVC1G07DBVR U?
U 1 1 5ED0D272
P 5400 5750
AR Path="/5ED2BC2A/5ED0D272" Ref="U?"  Part="1" 
AR Path="/5EB41214/5ED0D272" Ref="U?"  Part="1" 
F 0 "U?" H 5500 5400 60  0000 L CNN
F 1 "SN74LVC1G07DBVR" H 5500 5300 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 5600 5950 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g07" H 5600 6050 60  0001 L CNN
F 4 "296-8485-1-ND" H 5600 6150 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G07DBVR" H 5600 6250 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 5600 6350 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 5600 6450 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g07" H 5600 6550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G07DBVR/296-8485-1-ND/377454" H 5600 6650 60  0001 L CNN "DK_Detail_Page"
F 10 "IC BUF NON-INVERT 5.5V SOT23-5" H 5600 6750 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 5600 6850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5600 6950 60  0001 L CNN "Status"
	1    5400 5750
	1    0    0    -1  
$EndComp
$Comp
L comparators_magne:MAX9141 U?
U 1 1 5ED0D278
P 3700 5750
AR Path="/5EB41214/5ED0D278" Ref="U?"  Part="1" 
AR Path="/5ED2BC2A/5ED0D278" Ref="U?"  Part="1" 
AR Path="/5ECA2575/5ED0D278" Ref="U?"  Part="1" 
AR Path="/5ECAC95D/5ED0D278" Ref="U?"  Part="1" 
F 0 "U?" H 3850 6000 50  0000 L CNN
F 1 "MAX9141ESA+-ND" H 3850 5900 50  0000 L CNN
F 2 "digikey-footprints:SOIC-8_W3.9mm" H 3700 5750 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX9140-MAX9144.pdf" H 3700 5750 50  0001 C CNN
	1    3700 5750
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5ED0D27E
P 6600 6150
AR Path="/5ED2BC2A/5ED0D27E" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED0D27E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6600 6000 50  0001 C CNN
F 1 "+5VA" H 6615 6323 50  0000 C CNN
F 2 "" H 6600 6150 50  0001 C CNN
F 3 "" H 6600 6150 50  0001 C CNN
	1    6600 6150
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5ED0D284
P 3600 5100
AR Path="/5ED2BC2A/5ED0D284" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED0D284" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3600 4950 50  0001 C CNN
F 1 "+5VA" H 3615 5273 50  0000 C CNN
F 2 "" H 3600 5100 50  0001 C CNN
F 3 "" H 3600 5100 50  0001 C CNN
	1    3600 5100
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5ED0D28A
P 3950 6150
AR Path="/5ED2BC2A/5ED0D28A" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED0D28A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3950 6000 50  0001 C CNN
F 1 "+5VA" H 3965 6323 50  0000 C CNN
F 2 "" H 3950 6150 50  0001 C CNN
F 3 "" H 3950 6150 50  0001 C CNN
	1    3950 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR?
U 1 1 5ED0D290
P 3500 6400
AR Path="/5ED2BC2A/5ED0D290" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED0D290" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3500 6150 50  0001 C CNN
F 1 "GND1" H 3505 6227 50  0000 C CNN
F 2 "" H 3500 6400 50  0001 C CNN
F 3 "" H 3500 6400 50  0001 C CNN
	1    3500 6400
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR?
U 1 1 5ED0D296
P 2400 5400
AR Path="/5ED2BC2A/5ED0D296" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED0D296" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2400 5150 50  0001 C CNN
F 1 "GND1" H 2405 5227 50  0000 C CNN
F 2 "" H 2400 5400 50  0001 C CNN
F 3 "" H 2400 5400 50  0001 C CNN
	1    2400 5400
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5ED0D29C
P 5250 5350
AR Path="/5ED2BC2A/5ED0D29C" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED0D29C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5250 5200 50  0001 C CNN
F 1 "+5VA" H 5265 5523 50  0000 C CNN
F 2 "" H 5250 5350 50  0001 C CNN
F 3 "" H 5250 5350 50  0001 C CNN
	1    5250 5350
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U?
U 1 1 5ED0D2A2
P 2500 4300
AR Path="/5ED2BC2A/5ED0D2A2" Ref="U?"  Part="1" 
AR Path="/5EB41214/5ED0D2A2" Ref="U?"  Part="1" 
F 0 "U?" H 2475 4567 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 2475 4476 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 2650 4350 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 2700 4300 50  0001 C CNN
	1    2500 4300
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U?
U 2 1 5ED0D2A8
P 6450 5800
AR Path="/5ED2BC2A/5ED0D2A8" Ref="U?"  Part="2" 
AR Path="/5EB41214/5ED0D2A8" Ref="U?"  Part="2" 
F 0 "U?" H 6425 6067 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 6425 5976 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 6600 5850 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 6650 5800 50  0001 C CNN
	2    6450 5800
	1    0    0    -1  
$EndComp
Connection ~ 6150 5750
$Comp
L logic_gates_magne:74LVC2G32 U?
U 3 1 5ED0D2AF
P 6900 6150
AR Path="/5ED2BC2A/5ED0D2AF" Ref="U?"  Part="3" 
AR Path="/5EB41214/5ED0D2AF" Ref="U?"  Part="3" 
F 0 "U?" H 6953 6196 50  0000 L CNN
F 1 "SN74LVC2G32DCUR" H 6953 6105 50  0000 L CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 7050 6200 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 7100 6150 50  0001 C CNN
	3    6900 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5400 2650 5400
Wire Wire Line
	6600 6400 6900 6400
Wire Wire Line
	6600 6150 6800 6150
Wire Wire Line
	6800 6150 6800 5900
Wire Wire Line
	6800 5900 6900 5900
Connection ~ 6600 6150
Wire Wire Line
	6600 6400 5400 6400
Connection ~ 6600 6400
Connection ~ 5400 6400
Text HLabel 4250 3050 0    50   Input ~ 0
Voltage
Text HLabel 4250 3350 0    50   Input ~ 0
Current
$EndSCHEMATC
