EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 23 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C_Small C?
U 1 1 5ED09E6D
P 3550 4650
AR Path="/5EB41214/5ED09E6D" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ED09E6D" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5ED09E6D" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5ED09E6D" Ref="C?"  Part="1" 
AR Path="/5ED08686/5ED09E6D" Ref="C53"  Part="1" 
F 0 "C53" H 3642 4696 50  0000 L CNN
F 1 "C 100n" H 3642 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3550 4650 50  0001 C CNN
F 3 "~" H 3550 4650 50  0001 C CNN
F 4 "16" H 3550 4650 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 3550 4650 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 3550 4650 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 3550 4650 50  0001 C CNN "Farnell_PN"
	1    3550 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5ED09E87
P 3950 4650
AR Path="/5EB41214/5ED09E87" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ED09E87" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5ED09E87" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5ED09E87" Ref="C?"  Part="1" 
AR Path="/5ED08686/5ED09E87" Ref="C54"  Part="1" 
F 0 "C54" H 4042 4696 50  0000 L CNN
F 1 "C 1u" H 4042 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3950 4650 50  0001 C CNN
F 3 "~" H 3950 4650 50  0001 C CNN
F 4 "16" H 3950 4650 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 3950 4650 50  0001 C CNN "Tolerance"
F 6 "311-1365-1-ND" H 3950 4650 50  0001 C CNN "Digi-Key_PN"
F 7 "2547037" H 3950 4650 50  0001 C CNN "Farnell_PN"
	1    3950 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 4750 3550 4750
Connection ~ 3550 4750
Wire Wire Line
	3550 4750 2750 4750
Wire Wire Line
	3950 4550 3550 4550
Wire Wire Line
	3550 4500 3550 4550
Connection ~ 3550 4550
$Comp
L power:GND #PWR074
U 1 1 5ED0D1E1
P 2750 4800
F 0 "#PWR074" H 2750 4550 50  0001 C CNN
F 1 "GND" H 2755 4627 50  0000 C CNN
F 2 "" H 2750 4800 50  0001 C CNN
F 3 "" H 2750 4800 50  0001 C CNN
	1    2750 4800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR073
U 1 1 5ED0D468
P 3550 4500
F 0 "#PWR073" H 3550 4350 50  0001 C CNN
F 1 "+5V" H 3565 4673 50  0000 C CNN
F 2 "" H 3550 4500 50  0001 C CNN
F 3 "" H 3550 4500 50  0001 C CNN
	1    3550 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 4750 2750 4800
Connection ~ 2750 4750
$Sheet
S 6850 3150 550  200 
U 5ED1C478
F0 "sheet5ED1C466" 50
F1 "R_G_LED_indicator.sch" 50
F2 "Cntrl" I L 6850 3250 50 
$EndSheet
$Comp
L Device:C_Small C?
U 1 1 5ED1C480
P 7100 4650
AR Path="/5EB41214/5ED1C480" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ED1C480" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ED1C480" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5ED1C480" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5ED1C480" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5ED1C480" Ref="C?"  Part="1" 
AR Path="/5ED08686/5ED1C480" Ref="C59"  Part="1" 
F 0 "C59" H 7192 4696 50  0000 L CNN
F 1 "C 100n" H 7192 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7100 4650 50  0001 C CNN
F 3 "~" H 7100 4650 50  0001 C CNN
F 4 "16" H 7100 4650 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7100 4650 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 7100 4650 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 7100 4650 50  0001 C CNN "Farnell_PN"
	1    7100 4650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5ED1C48C
P 7100 4500
AR Path="/5EB41214/5ED1C48C" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED1C48C" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5ED1C48C" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5ED1C48C" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5ED1C48C" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5ED1C48C" Ref="#PWR?"  Part="1" 
AR Path="/5ED08686/5ED1C48C" Ref="#PWR082"  Part="1" 
F 0 "#PWR082" H 7100 4350 50  0001 C CNN
F 1 "+5V" H 7115 4673 50  0000 C CNN
F 2 "" H 7100 4500 50  0001 C CNN
F 3 "" H 7100 4500 50  0001 C CNN
	1    7100 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 4500 7100 4550
$Comp
L Device:C_Small C?
U 1 1 5ED1C495
P 5550 4650
AR Path="/5EB41214/5ED1C495" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ED1C495" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ED1C495" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5ED1C495" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5ED1C495" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5ED1C495" Ref="C?"  Part="1" 
AR Path="/5ED08686/5ED1C495" Ref="C58"  Part="1" 
F 0 "C58" H 5642 4696 50  0000 L CNN
F 1 "C 100n" H 5642 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5550 4650 50  0001 C CNN
F 3 "~" H 5550 4650 50  0001 C CNN
F 4 "16" H 5550 4650 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5550 4650 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 5550 4650 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 5550 4650 50  0001 C CNN "Farnell_PN"
	1    5550 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4750 5550 4750
Connection ~ 5550 4750
$Comp
L power:+5V #PWR?
U 1 1 5ED1C49E
P 5550 4500
AR Path="/5EB41214/5ED1C49E" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED1C49E" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5ED1C49E" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5ED1C49E" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5ED1C49E" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5ED1C49E" Ref="#PWR?"  Part="1" 
AR Path="/5ED08686/5ED1C49E" Ref="#PWR080"  Part="1" 
F 0 "#PWR080" H 5550 4350 50  0001 C CNN
F 1 "+5V" H 5565 4673 50  0000 C CNN
F 2 "" H 5550 4500 50  0001 C CNN
F 3 "" H 5550 4500 50  0001 C CNN
	1    5550 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4500 5550 4550
Wire Wire Line
	4400 4600 4400 4750
$Comp
L power:+5V #PWR?
U 1 1 5ED1C4B5
P 4950 2550
AR Path="/5EB41214/5ED1C4B5" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED1C4B5" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5ED1C4B5" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5ED1C4B5" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5ED1C4B5" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5ED1C4B5" Ref="#PWR?"  Part="1" 
AR Path="/5ED08686/5ED1C4B5" Ref="#PWR076"  Part="1" 
F 0 "#PWR076" H 4950 2400 50  0001 C CNN
F 1 "+5V" H 4850 2650 50  0000 C CNN
F 2 "" H 4950 2550 50  0001 C CNN
F 3 "" H 4950 2550 50  0001 C CNN
	1    4950 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4400 5300 4750
Connection ~ 5300 4750
Wire Wire Line
	5550 4500 5400 4500
Wire Wire Line
	5400 4500 5400 4400
Connection ~ 5550 4500
$Comp
L power:GND #PWR?
U 1 1 5ED1C4D4
P 4950 3100
AR Path="/5EB41214/5ED1C4D4" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED1C4D4" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5ED1C4D4" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5ED1C4D4" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5ED1C4D4" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5ED1C4D4" Ref="#PWR?"  Part="1" 
AR Path="/5ED08686/5ED1C4D4" Ref="#PWR077"  Part="1" 
F 0 "#PWR077" H 4950 2850 50  0001 C CNN
F 1 "GND" H 4955 2927 50  0000 C CNN
F 2 "" H 4950 3100 50  0001 C CNN
F 3 "" H 4950 3100 50  0001 C CNN
	1    4950 3100
	-1   0    0    -1  
$EndComp
Text GLabel 6550 4300 0    50   Input ~ 0
!ARM
Wire Wire Line
	6550 4300 6650 4300
$Comp
L power:+5V #PWR?
U 1 1 5ED1C4DF
P 6950 3950
AR Path="/5EB41214/5ED1C4DF" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED1C4DF" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5ED1C4DF" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5ED1C4DF" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5ED1C4DF" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5ED1C4DF" Ref="#PWR?"  Part="1" 
AR Path="/5ED08686/5ED1C4DF" Ref="#PWR081"  Part="1" 
F 0 "#PWR081" H 6950 3800 50  0001 C CNN
F 1 "+5V" H 6965 4123 50  0000 C CNN
F 2 "" H 6950 3950 50  0001 C CNN
F 3 "" H 6950 3950 50  0001 C CNN
	1    6950 3950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6950 4750 7100 4750
Connection ~ 6950 4750
Wire Wire Line
	4650 2350 4650 2600
Text HLabel 8150 4200 2    50   Input ~ 0
Output
Wire Wire Line
	5950 4100 6650 4100
Connection ~ 5950 4100
Wire Wire Line
	5700 4100 5950 4100
Wire Wire Line
	6850 3250 6650 3250
Wire Wire Line
	6650 3250 6650 4100
$Comp
L Device:C_Small C?
U 1 1 5ED1C4F4
P 5100 3000
AR Path="/5EB41214/5ED1C4F4" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ED1C4F4" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ED1C4F4" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5ED1C4F4" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5ED1C4F4" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5ED1C4F4" Ref="C?"  Part="1" 
AR Path="/5ED08686/5ED1C4F4" Ref="C57"  Part="1" 
F 0 "C57" H 5192 3046 50  0000 L CNN
F 1 "C 100n" H 5192 2955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5100 3000 50  0001 C CNN
F 3 "~" H 5100 3000 50  0001 C CNN
F 4 "16" H 5100 3000 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5100 3000 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 5100 3000 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 5100 3000 50  0001 C CNN "Farnell_PN"
	1    5100 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3100 5100 3100
$Comp
L power:+5V #PWR?
U 1 1 5ED1C4FD
P 5100 2900
AR Path="/5EB41214/5ED1C4FD" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED1C4FD" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5ED1C4FD" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5ED1C4FD" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5ED1C4FD" Ref="#PWR?"  Part="1" 
AR Path="/5EE8A13E/5ED1C4FD" Ref="#PWR?"  Part="1" 
AR Path="/5ED08686/5ED1C4FD" Ref="#PWR078"  Part="1" 
F 0 "#PWR078" H 5100 2750 50  0001 C CNN
F 1 "+5V" H 5115 3073 50  0000 C CNN
F 2 "" H 5100 2900 50  0001 C CNN
F 3 "" H 5100 2900 50  0001 C CNN
	1    5100 2900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4650 2350 5950 2350
Wire Wire Line
	5950 2350 5950 4100
Wire Wire Line
	5450 2650 5450 3800
$Comp
L Device:R R?
U 1 1 5ED1C50C
P 4050 4200
AR Path="/5ED1C50C" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED1C50C" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED1C50C" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ED1C50C" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5ED1C50C" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5ED1C50C" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5ED1C50C" Ref="R?"  Part="1" 
AR Path="/5ED08686/5ED1C50C" Ref="R60"  Part="1" 
F 0 "R60" V 4257 4200 50  0000 C CNN
F 1 "R 16K" V 4166 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3980 4200 50  0001 C CNN
F 3 "~" H 4050 4200 50  0001 C CNN
F 4 "1%" H 4050 4200 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 4050 4200 50  0001 C CNN "Digi-Key_PN"
	1    4050 4200
	0    -1   -1   0   
$EndComp
Text GLabel 4400 2700 0    50   Input ~ 0
!LE
Wire Wire Line
	4400 2700 4650 2700
Wire Wire Line
	5200 2650 5450 2650
$Comp
L Device:R R?
U 1 1 5ED28602
P 5000 3750
AR Path="/5ED28602" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED28602" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED28602" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ED28602" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5ED28602" Ref="R?"  Part="1" 
AR Path="/5ED08686/5ED28602" Ref="R62"  Part="1" 
F 0 "R62" V 4793 3750 50  0000 C CNN
F 1 "R 33K" V 4884 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4930 3750 50  0001 C CNN
F 3 "~" H 5000 3750 50  0001 C CNN
F 4 "1%" H 5000 3750 50  0001 C CNN "Tolerance"
F 5 "541-33.0KCCT-ND" H 5000 3750 50  0001 C CNN "Digi-Key_PN"
	1    5000 3750
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5ED28609
P 4500 3750
AR Path="/5ED28609" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED28609" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ED28609" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ED28609" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5ED28609" Ref="R?"  Part="1" 
AR Path="/5ED08686/5ED28609" Ref="R61"  Part="1" 
F 0 "R61" V 4707 3750 50  0000 C CNN
F 1 "R 12K" V 4616 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4430 3750 50  0001 C CNN
F 3 "~" H 4500 3750 50  0001 C CNN
F 4 "0.1%" H 4500 3750 50  0001 C CNN "Tolerance"
F 5 "P12KDACT-ND" H 4500 3750 50  0001 C CNN "Digi-Key_PN"
	1    4500 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5300 3800 5300 3750
$Comp
L power:+5V #PWR?
U 1 1 5ED28610
P 5300 3700
AR Path="/5EB41214/5ED28610" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED28610" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5ED28610" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5ED28610" Ref="#PWR?"  Part="1" 
AR Path="/5ED08686/5ED28610" Ref="#PWR079"  Part="1" 
F 0 "#PWR079" H 5300 3550 50  0001 C CNN
F 1 "+5V" H 5315 3873 50  0000 C CNN
F 2 "" H 5300 3700 50  0001 C CNN
F 3 "" H 5300 3700 50  0001 C CNN
	1    5300 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 3750 4750 3750
Connection ~ 4750 3750
Wire Wire Line
	4750 3750 4850 3750
Wire Wire Line
	5150 3750 5300 3750
Connection ~ 5300 3750
Wire Wire Line
	5300 3750 5300 3700
$Comp
L power:GND #PWR?
U 1 1 5ED2861D
P 4350 3800
AR Path="/5EB41214/5ED2861D" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED2861D" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5ED2861D" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5ED2861D" Ref="#PWR?"  Part="1" 
AR Path="/5ED08686/5ED2861D" Ref="#PWR075"  Part="1" 
F 0 "#PWR075" H 4350 3550 50  0001 C CNN
F 1 "GND" H 4355 3627 50  0000 C CNN
F 2 "" H 4350 3800 50  0001 C CNN
F 3 "" H 4350 3800 50  0001 C CNN
	1    4350 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3750 4350 3800
$Comp
L comparators_magne:MAX9141 U?
U 1 1 5ED28624
P 5400 4100
AR Path="/5EB41214/5ED28624" Ref="U?"  Part="1" 
AR Path="/5ED2BC2A/5ED28624" Ref="U?"  Part="1" 
AR Path="/5EE0AC5D/5ED28624" Ref="U?"  Part="1" 
AR Path="/5EE0E313/5ED28624" Ref="U?"  Part="1" 
AR Path="/5ED08686/5ED28624" Ref="U29"  Part="1" 
F 0 "U29" H 5600 4200 50  0000 L CNN
F 1 "MAX9141ESA+-ND" H 5550 4300 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5400 4100 50  0001 C CNN
F 3 "" H 5400 4100 50  0001 C CNN
F 4 "	MAX9141ESA+-ND" H 5400 4100 50  0001 C CNN "Digi-Key_PN"
	1    5400 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 4000 4950 4000
Wire Wire Line
	4200 4200 4400 4200
Wire Wire Line
	4400 4300 4400 4200
Connection ~ 4400 4200
Wire Wire Line
	4400 4200 5100 4200
Text Notes 4500 3500 0    50   ~ 0
43Hz low pass
$Comp
L Device:C_Small C?
U 1 1 5ED2F9E9
P 4950 4400
AR Path="/5EB41214/5ED2F9E9" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ED2F9E9" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ED2F9E9" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5ED2F9E9" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5ED2F9E9" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5ED2F9E9" Ref="C?"  Part="1" 
AR Path="/5ED08686/5ED2F9E9" Ref="C56"  Part="1" 
F 0 "C56" H 5042 4446 50  0000 L CNN
F 1 "C 2.2u" H 5042 4355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4950 4400 50  0001 C CNN
F 3 "~" H 4950 4400 50  0001 C CNN
F 4 "16" H 4950 4400 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 4950 4400 50  0001 C CNN "Tolerance"
F 6 "490-4787-1-ND" H 4950 4400 50  0001 C CNN "Digi-Key_PN"
F 7 "1458904" H 4950 4400 50  0001 C CNN "Farnell_PN"
	1    4950 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4300 4950 4000
Connection ~ 4950 4000
Wire Wire Line
	4950 4000 5100 4000
Wire Wire Line
	4950 4500 4950 4750
Connection ~ 4950 4750
Wire Wire Line
	4950 4750 5300 4750
Wire Wire Line
	5550 4750 6950 4750
Connection ~ 4400 4750
Connection ~ 3950 4750
Wire Wire Line
	4400 4750 3950 4750
Wire Wire Line
	4400 4750 4950 4750
Wire Wire Line
	3900 4200 3850 4200
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J2
U 1 1 5ED4BA72
P 3550 3700
F 0 "J2" H 3600 4017 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 3600 3926 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 3550 3700 50  0001 C CNN
F 3 "~" H 3550 3700 50  0001 C CNN
F 4 "952-2121-ND" H 3550 3700 50  0001 C CNN "Digi-Key_PN"
	1    3550 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3800 3850 4200
Wire Wire Line
	3850 3800 3850 3700
Connection ~ 3850 3800
Connection ~ 3850 3700
Wire Wire Line
	3850 3700 3850 3600
Wire Wire Line
	6600 4200 6650 4200
Wire Wire Line
	6950 4325 6950 4750
Wire Wire Line
	7250 4200 7500 4200
Connection ~ 6650 4100
Wire Wire Line
	6950 3950 6950 4075
$Comp
L 74xGxx:74LVC1G332 U?
U 1 1 5ED5401E
P 6950 4200
AR Path="/5EE5AAE3/5ED5401E" Ref="U?"  Part="1" 
AR Path="/5EE77D5A/5ED5401E" Ref="U?"  Part="1" 
AR Path="/5ED08686/5ED5401E" Ref="U30"  Part="1" 
F 0 "U30" H 7100 4550 50  0000 C CNN
F 1 "SN74LVC1G332DCKRG4" H 7300 4450 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6_Handsoldering" H 6950 4200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74lvc1g332.pdf?HQS=TI-null-null-digikeymode-df-pf-null-wwe&ts=1590142069492" H 6950 4200 50  0001 C CNN
F 4 "296-51594-1-ND" H 6950 4200 50  0001 C CNN "Digi-Key_PN"
	1    6950 4200
	1    0    0    -1  
$EndComp
Text GLabel 6600 4200 0    50   Input ~ 0
!T_arm_low
$Comp
L logic_gates_magne:74LVC1G32 U?
U 1 1 5ED1633D
P 4950 2650
AR Path="/5ED2BC2A/5ED1633D" Ref="U?"  Part="1" 
AR Path="/5ED08686/5ED1633D" Ref="U28"  Part="1" 
F 0 "U28" H 5244 2696 50  0000 L CNN
F 1 "SN74LVC1G32DCKR" H 5244 2605 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 4950 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 4950 2650 50  0001 C CNN
F 4 "296-9848-1-ND" H 4950 2650 50  0001 C CNN "Digi-Key_PN"
	1    4950 2650
	1    0    0    -1  
$EndComp
Connection ~ 4950 3100
Wire Wire Line
	4950 2750 4950 3100
Wire Wire Line
	3350 3700 2950 3700
Wire Wire Line
	1950 3600 3350 3600
Text Notes 4500 3400 0    50   ~ 0
26% RH setpoint
$Comp
L dk_Rectangular-Connectors-Headers-Male-Pins:640456-3 J3
U 1 1 5F782311
P 4650 3900
F 0 "J3" V 4425 3908 50  0000 C CNN
F 1 "640456-3" V 4516 3908 50  0000 C CNN
F 2 "digikey-footprints:PinHeader_1x3_P2.54_Drill1.1mm" H 4850 4100 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=640456&DocType=Customer+Drawing&DocLang=English" H 4850 4200 60  0001 L CNN
F 4 "A19470-ND" H 4850 4300 60  0001 L CNN "Digi-Key_PN"
F 5 "640456-3" H 4850 4400 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 4850 4500 60  0001 L CNN "Category"
F 7 "Rectangular Connectors - Headers, Male Pins" H 4850 4600 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=640456&DocType=Customer+Drawing&DocLang=English" H 4850 4700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-amp-connectors/640456-3/A19470-ND/259010" H 4850 4800 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN HEADER VERT 3POS 2.54MM" H 4850 4900 60  0001 L CNN "Description"
F 11 "TE Connectivity AMP Connectors" H 4850 5000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4850 5100 60  0001 L CNN "Status"
	1    4650 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 3900 4750 3750
Text HLabel 4650 5400 0    50   Input ~ 0
NTC
Wire Wire Line
	4750 4100 4750 5400
Wire Wire Line
	7850 4250 7850 4200
Wire Wire Line
	7850 4200 8150 4200
$Comp
L Device:R R?
U 1 1 5F88A5ED
P 7650 4200
AR Path="/5F88A5ED" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F88A5ED" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F88A5ED" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5F88A5ED" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5F88A5ED" Ref="R?"  Part="1" 
AR Path="/5EED7553/5F88A5ED" Ref="R?"  Part="1" 
AR Path="/5EEDC7D6/5F88A5ED" Ref="R?"  Part="1" 
AR Path="/5ED08686/5F88A5ED" Ref="R63"  Part="1" 
F 0 "R63" V 7443 4200 50  0000 C CNN
F 1 "R 10" V 7534 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7580 4200 50  0001 C CNN
F 3 "~" H 7650 4200 50  0001 C CNN
F 4 "" H 7650 4200 50  0001 C CNN "Tolerance"
F 5 "RMCF0603FT10R0CT-ND" H 7650 4200 50  0001 C CNN "Digi-Key_PN"
	1    7650 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	7800 4200 7850 4200
Connection ~ 7850 4200
Wire Wire Line
	7100 4750 7850 4750
Wire Wire Line
	7850 4750 7850 4450
Connection ~ 7100 4750
$Comp
L Device:C_Small C?
U 1 1 5F9188D6
P 7850 4350
AR Path="/5EB41214/5F9188D6" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F9188D6" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F9188D6" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F9188D6" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5F9188D6" Ref="C?"  Part="1" 
AR Path="/5ECA36FF/5F9188D6" Ref="C?"  Part="1" 
AR Path="/5EED7553/5F9188D6" Ref="C?"  Part="1" 
AR Path="/5EEDC7D6/5F9188D6" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5F9188D6" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F9188D6" Ref="C?"  Part="1" 
AR Path="/5ED08686/5F9188D6" Ref="C60"  Part="1" 
F 0 "C60" H 7942 4396 50  0000 L CNN
F 1 "C 470p" H 7942 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7850 4350 50  0001 C CNN
F 3 "~" H 7850 4350 50  0001 C CNN
F 4 "16" H 7850 4350 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7850 4350 50  0001 C CNN "Tolerance"
F 6 "399-10051-1-ND" H 7850 4350 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 7850 4350 50  0001 C CNN "Farnell_PN"
	1    7850 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 5400 4750 5400
$Comp
L Device:Jumper_NC_Small JP?
U 1 1 5FB5DC5B
P 2100 4650
AR Path="/5ECDAC29/5FB5DC5B" Ref="JP?"  Part="1" 
AR Path="/5EE77D5A/5FB5DC5B" Ref="JP?"  Part="1" 
AR Path="/5ED08686/5FB5DC5B" Ref="JP2"  Part="1" 
F 0 "JP2" V 2150 4850 50  0000 R CNN
F 1 "Jumper" V 2050 5000 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" V 2009 4563 50  0001 R CNN
F 3 "~" H 2100 4650 50  0001 C CNN
F 4 "732-5315-ND" H 2100 4650 50  0001 C CNN "Digi-Key_PN"
	1    2100 4650
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 60208F49
P 4400 4450
AR Path="/5EEDC7D6/60208F49" Ref="C?"  Part="1" 
AR Path="/5F19D573/5FC01F23/60208F49" Ref="C?"  Part="1" 
AR Path="/5ED08686/60208F49" Ref="C55"  Part="1" 
F 0 "C55" H 4600 4400 50  0000 R CNN
F 1 "C 4.7u" H 4800 4500 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4438 4300 50  0001 C CNN
F 3 "~" H 4400 4450 50  0001 C CNN
F 4 "490-11992-1-ND" H 4400 4450 50  0001 C CNN "Digi-Key_PN"
	1    4400 4450
	1    0    0    1   
$EndComp
$Comp
L dk_Sockets-for-ICs-Transistors:A_08-LC-TT XU48
U 1 1 6024F91E
P 2500 4400
F 0 "XU48" H 2500 4775 50  0000 C CNN
F 1 "A_08-LC-TT" H 2500 4684 50  0000 C CNN
F 2 "digikey-footprints:SOCKET_DIP-8_7.62mm_Conn" H 2700 4600 60  0001 L CNN
F 3 "http://www.assmann-wsw.com/fileadmin/datasheets/ASS_0810_CO.pdf" H 2700 4700 60  0001 L CNN
F 4 "AE9986-ND" H 2700 4800 60  0001 L CNN "Digi-Key_PN"
F 5 "A 08-LC-TT" H 2700 4900 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 2700 5000 60  0001 L CNN "Category"
F 7 "Sockets for ICs, Transistors" H 2700 5100 60  0001 L CNN "Family"
F 8 "http://www.assmann-wsw.com/fileadmin/datasheets/ASS_0810_CO.pdf" H 2700 5200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/assmann-wsw-components/A-08-LC-TT/AE9986-ND/821740" H 2700 5300 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN IC DIP SOCKET 8POS TIN" H 2700 5400 60  0001 L CNN "Description"
F 11 "Assmann WSW Components" H 2700 5500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2700 5600 60  0001 L CNN "Status"
	1    2500 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 4450 1950 4450
Wire Wire Line
	1950 3600 1950 4450
Text HLabel 2800 4350 2    50   Input ~ 0
HUM_SCL
Wire Wire Line
	2800 4350 2750 4350
Wire Wire Line
	2750 4450 2950 4450
Wire Wire Line
	2950 3700 2950 4450
Text HLabel 3000 4550 2    50   Input ~ 0
HUM_SDA
$Comp
L Device:R R?
U 1 1 5FB64FC3
P 2100 4100
AR Path="/5EEDC7D6/5FB64FC3" Ref="R?"  Part="1" 
AR Path="/5ED08686/5FB64FC3" Ref="R59"  Part="1" 
F 0 "R59" H 2000 4300 50  0000 C CNN
F 1 "R 16K" H 1950 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2030 4100 50  0001 C CNN
F 3 "~" H 2100 4100 50  0001 C CNN
F 4 "1%" H 2100 4100 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 2100 4100 50  0001 C CNN "Digi-Key_PN"
	1    2100 4100
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR072
U 1 1 5FB6B9E5
P 2100 3900
F 0 "#PWR072" H 2100 3750 50  0001 C CNN
F 1 "+5V" H 2115 4073 50  0000 C CNN
F 2 "" H 2100 3900 50  0001 C CNN
F 3 "" H 2100 3900 50  0001 C CNN
	1    2100 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3900 2100 3950
Wire Wire Line
	2250 4550 2250 4750
Wire Wire Line
	2250 4750 2750 4750
Wire Wire Line
	2750 4550 3000 4550
Wire Wire Line
	3000 4550 3000 3800
Wire Wire Line
	3000 3800 3350 3800
Connection ~ 2100 4250
Wire Wire Line
	2100 4250 2250 4250
Wire Wire Line
	2100 4250 2100 4550
Wire Wire Line
	2100 4750 2250 4750
Connection ~ 2250 4750
NoConn ~ 2250 4350
Wire Wire Line
	2750 4250 2800 4250
Wire Wire Line
	2100 3950 2800 3950
Wire Wire Line
	2800 3950 2800 4250
Connection ~ 2100 3950
NoConn ~ 2550 5300
NoConn ~ 2250 5500
NoConn ~ 2250 5600
NoConn ~ 2250 5700
NoConn ~ 2250 5800
NoConn ~ 2250 5900
NoConn ~ 2250 6000
NoConn ~ 2550 6200
NoConn ~ 2400 6950
$EndSCHEMATC
