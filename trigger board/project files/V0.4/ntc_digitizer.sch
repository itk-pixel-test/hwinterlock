EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 34 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L adc_magne:MCP3221 U45
U 1 1 5FAAFC74
P 4550 3900
F 0 "U45" H 4200 4300 50  0000 C CNN
F 1 "MCP3221" H 4050 4400 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4200 4500 50  0001 C CNN
F 3 "https://ww1.microchip.com/downloads/en/DeviceDoc/20001732E.pdf" H 4200 4500 50  0001 C CNN
F 4 "MCP3221A0T-E/OTCT-ND" H 4550 3900 50  0001 C CNN "Digi-Key_PN"
	1    4550 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FAB41B0
P 5000 5100
AR Path="/5EB41214/5FAB41B0" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FAB41B0" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FAB41B0" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FAB41B0" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5FAB41B0" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FAB41B0" Ref="C?"  Part="1" 
AR Path="/5ED08686/5FAB41B0" Ref="C?"  Part="1" 
AR Path="/5FAAE40B/5FAB41B0" Ref="C85"  Part="1" 
F 0 "C85" H 5092 5146 50  0000 L CNN
F 1 "C 100n" H 5092 5055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5000 5100 50  0001 C CNN
F 3 "~" H 5000 5100 50  0001 C CNN
F 4 "16" H 5000 5100 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5000 5100 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 5000 5100 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 5000 5100 50  0001 C CNN "Farnell_PN"
	1    5000 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FAB41BA
P 4550 5100
AR Path="/5EB41214/5FAB41BA" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FAB41BA" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FAB41BA" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FAB41BA" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5FAB41BA" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FAB41BA" Ref="C?"  Part="1" 
AR Path="/5ED08686/5FAB41BA" Ref="C?"  Part="1" 
AR Path="/5FAAE40B/5FAB41BA" Ref="C84"  Part="1" 
F 0 "C84" H 4642 5146 50  0000 L CNN
F 1 "C 2.2u" H 4642 5055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4550 5100 50  0001 C CNN
F 3 "~" H 4550 5100 50  0001 C CNN
F 4 "16" H 4550 5100 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 4550 5100 50  0001 C CNN "Tolerance"
F 6 "490-4787-1-ND" H 4550 5100 50  0001 C CNN "Digi-Key_PN"
F 7 "1458904" H 4550 5100 50  0001 C CNN "Farnell_PN"
	1    4550 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 5200 5000 5200
Wire Wire Line
	4550 5000 5000 5000
$Comp
L power:GND #PWR0135
U 1 1 5FAB4412
P 4550 4350
F 0 "#PWR0135" H 4550 4100 50  0001 C CNN
F 1 "GND" H 4555 4177 50  0000 C CNN
F 2 "" H 4550 4350 50  0001 C CNN
F 3 "" H 4550 4350 50  0001 C CNN
	1    4550 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0137
U 1 1 5FAB4563
P 4550 5200
F 0 "#PWR0137" H 4550 4950 50  0001 C CNN
F 1 "GND" H 4555 5027 50  0000 C CNN
F 2 "" H 4550 5200 50  0001 C CNN
F 3 "" H 4550 5200 50  0001 C CNN
	1    4550 5200
	1    0    0    -1  
$EndComp
Connection ~ 4550 5200
$Comp
L power:+5V #PWR0134
U 1 1 5FAB4736
P 4550 3450
F 0 "#PWR0134" H 4550 3300 50  0001 C CNN
F 1 "+5V" H 4565 3623 50  0000 C CNN
F 2 "" H 4550 3450 50  0001 C CNN
F 3 "" H 4550 3450 50  0001 C CNN
	1    4550 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0136
U 1 1 5FAB4914
P 4550 5000
F 0 "#PWR0136" H 4550 4850 50  0001 C CNN
F 1 "+5V" H 4565 5173 50  0000 C CNN
F 2 "" H 4550 5000 50  0001 C CNN
F 3 "" H 4550 5000 50  0001 C CNN
	1    4550 5000
	1    0    0    -1  
$EndComp
Connection ~ 4550 5000
Text HLabel 3950 3900 0    50   Input ~ 0
In
Wire Wire Line
	5050 3800 5150 3800
Wire Wire Line
	5900 3600 5800 3600
Wire Wire Line
	5800 3600 5800 4000
Wire Wire Line
	5800 4000 5500 4000
Wire Wire Line
	4550 3500 4550 3450
Wire Wire Line
	5900 3500 5500 3500
Connection ~ 4550 3500
Wire Wire Line
	4550 4300 4550 4350
Wire Wire Line
	4550 4300 5900 4300
Wire Wire Line
	5900 4300 5900 3900
Connection ~ 4550 4300
$Comp
L Device:C_Small C?
U 1 1 5FABB6DA
P 5500 5100
AR Path="/5EB41214/5FABB6DA" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FABB6DA" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FABB6DA" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FABB6DA" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5FABB6DA" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FABB6DA" Ref="C?"  Part="1" 
AR Path="/5ED08686/5FABB6DA" Ref="C?"  Part="1" 
AR Path="/5FAAE40B/5FABB6DA" Ref="C86"  Part="1" 
F 0 "C86" H 5592 5146 50  0000 L CNN
F 1 "C 100n" H 5592 5055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5500 5100 50  0001 C CNN
F 3 "~" H 5500 5100 50  0001 C CNN
F 4 "16" H 5500 5100 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5500 5100 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 5500 5100 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 5500 5100 50  0001 C CNN "Farnell_PN"
	1    5500 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 5000 5500 5000
Connection ~ 5000 5000
Wire Wire Line
	5500 5200 5000 5200
Connection ~ 5000 5200
$Comp
L Device:R R?
U 1 1 5FABD811
P 5150 3650
AR Path="/5FABD811" Ref="R?"  Part="1" 
AR Path="/5EB41214/5FABD811" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5FABD811" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5FABD811" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5FABD811" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5FABD811" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5FABD811" Ref="R?"  Part="1" 
AR Path="/5FAAE40B/5FABD811" Ref="R91"  Part="1" 
F 0 "R91" H 5350 3600 50  0000 C CNN
F 1 "R 16K" H 5300 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5080 3650 50  0001 C CNN
F 3 "~" H 5150 3650 50  0001 C CNN
F 4 "1%" H 5150 3650 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 5150 3650 50  0001 C CNN "Digi-Key_PN"
	1    5150 3650
	1    0    0    -1  
$EndComp
Connection ~ 5150 3500
Wire Wire Line
	5150 3500 4550 3500
Connection ~ 5150 3800
Wire Wire Line
	5150 3800 5900 3800
$Comp
L Device:R R?
U 1 1 5FABDF28
P 5500 3850
AR Path="/5FABDF28" Ref="R?"  Part="1" 
AR Path="/5EB41214/5FABDF28" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5FABDF28" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5FABDF28" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5FABDF28" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5FABDF28" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5FABDF28" Ref="R?"  Part="1" 
AR Path="/5FAAE40B/5FABDF28" Ref="R92"  Part="1" 
F 0 "R92" H 5700 3800 50  0000 C CNN
F 1 "R 16K" H 5650 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5430 3850 50  0001 C CNN
F 3 "~" H 5500 3850 50  0001 C CNN
F 4 "1%" H 5500 3850 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 5500 3850 50  0001 C CNN "Digi-Key_PN"
	1    5500 3850
	1    0    0    -1  
$EndComp
Connection ~ 5500 4000
Wire Wire Line
	5500 4000 5050 4000
Wire Wire Line
	5500 3700 5500 3500
Connection ~ 5500 3500
Wire Wire Line
	5500 3500 5150 3500
$Comp
L dk_Rectangular-Connectors-Headers-Male-Pins:0022232041 J7
U 1 1 5FAC98B3
P 8300 3400
F 0 "J7" V 8491 3272 50  0000 R CNN
F 1 "0022232041" V 8400 3272 50  0000 R CNN
F 2 "magne_footprints:0011121041" H 8500 3600 60  0001 L CNN
F 3 "https://www.molex.com/pdm_docs/sd/022232041_sd.pdf" H 8500 3700 60  0001 L CNN
F 4 "WM4202-ND" H 8500 3800 60  0001 L CNN "Digi-Key_PN"
F 5 "0022232041" H 8500 3900 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 8500 4000 60  0001 L CNN "Category"
F 7 "Rectangular Connectors - Headers, Male Pins" H 8500 4100 60  0001 L CNN "Family"
F 8 "https://www.molex.com/pdm_docs/sd/022232041_sd.pdf" H 8500 4200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/molex/0022232041/WM4202-ND/26671" H 8500 4300 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN HEADER VERT 4POS 2.54MM" H 8500 4400 60  0001 L CNN "Description"
F 11 "Molex" H 8500 4500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8500 4600 60  0001 L CNN "Status"
	1    8300 3400
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FACA188
P 7500 3450
AR Path="/5FACA188" Ref="R?"  Part="1" 
AR Path="/5EB41214/5FACA188" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5FACA188" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5FACA188" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5FACA188" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5FACA188" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5FACA188" Ref="R?"  Part="1" 
AR Path="/5FAAE40B/5FACA188" Ref="R93"  Part="1" 
F 0 "R93" H 7700 3400 50  0000 C CNN
F 1 "R 16K" H 7650 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7430 3450 50  0001 C CNN
F 3 "~" H 7500 3450 50  0001 C CNN
F 4 "1%" H 7500 3450 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 7500 3450 50  0001 C CNN "Digi-Key_PN"
	1    7500 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FACA485
P 7800 3650
AR Path="/5FACA485" Ref="R?"  Part="1" 
AR Path="/5EB41214/5FACA485" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5FACA485" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5FACA485" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5FACA485" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5FACA485" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5FACA485" Ref="R?"  Part="1" 
AR Path="/5FAAE40B/5FACA485" Ref="R94"  Part="1" 
F 0 "R94" H 8000 3600 50  0000 C CNN
F 1 "R 16K" H 7950 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7730 3650 50  0001 C CNN
F 3 "~" H 7800 3650 50  0001 C CNN
F 4 "1%" H 7800 3650 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 7800 3650 50  0001 C CNN "Digi-Key_PN"
	1    7800 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3600 7500 3600
Wire Wire Line
	6900 3800 7800 3800
$Comp
L digital_isolators_magne:SI8602 U46
U 1 1 5FAB7CE2
P 6400 3700
F 0 "U46" H 6400 4167 50  0000 C CNN
F 1 "SI8602" H 6400 4076 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 6400 4025 50  0001 C CNN
F 3 "" H 6400 4025 50  0001 C CNN
F 4 "SI8602AB-B-IS-ND" H 6400 3700 50  0001 C CNN "Digi-Key_PN"
	1    6400 3700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6900 3500 6900 3300
Wire Wire Line
	6900 3300 7050 3300
Connection ~ 7500 3300
Wire Wire Line
	7500 3300 7800 3300
Wire Wire Line
	7800 3500 7800 3300
Wire Wire Line
	6900 3900 7050 3900
Connection ~ 7800 3800
Wire Wire Line
	7500 3600 8000 3600
Connection ~ 7500 3600
$Comp
L Device:C_Small C?
U 1 1 5FAD7E0A
P 7050 3400
AR Path="/5EB41214/5FAD7E0A" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FAD7E0A" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FAD7E0A" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FAD7E0A" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5FAD7E0A" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FAD7E0A" Ref="C?"  Part="1" 
AR Path="/5ED08686/5FAD7E0A" Ref="C?"  Part="1" 
AR Path="/5FAAE40B/5FAD7E0A" Ref="C87"  Part="1" 
F 0 "C87" H 7142 3446 50  0000 L CNN
F 1 "C 100n" H 7142 3355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7050 3400 50  0001 C CNN
F 3 "~" H 7050 3400 50  0001 C CNN
F 4 "16" H 7050 3400 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7050 3400 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 7050 3400 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 7050 3400 50  0001 C CNN "Farnell_PN"
	1    7050 3400
	1    0    0    -1  
$EndComp
Connection ~ 7050 3300
Wire Wire Line
	7050 3300 7500 3300
Wire Wire Line
	7050 3500 7050 3900
Connection ~ 7050 3900
Wire Wire Line
	7050 3900 8100 3900
Wire Wire Line
	8200 3800 8200 3400
Wire Wire Line
	7800 3800 8200 3800
Wire Wire Line
	8200 3100 8000 3100
Wire Wire Line
	8000 3100 8000 3600
Wire Wire Line
	8200 3200 8100 3200
Wire Wire Line
	8100 3200 8100 3900
Wire Wire Line
	8200 3300 7800 3300
Connection ~ 7800 3300
$EndSCHEMATC
