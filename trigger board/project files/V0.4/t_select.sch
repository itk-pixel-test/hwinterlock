EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 30 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 7800 3500 2    50   Input ~ 0
T_SELECT
$Sheet
S 5200 3400 550  350 
U 5F94BADA
F0 "SR Latch" 50
F1 "sr_latch.sch" 50
F2 "S" I L 5200 3500 50 
F3 "R" I L 5200 3650 50 
F4 "Q" I R 5750 3500 50 
F5 "~Q" I R 5750 3650 50 
$EndSheet
Wire Wire Line
	5000 3500 5200 3500
Text GLabel 4450 3650 0    50   Input ~ 0
!ARM
Wire Wire Line
	4450 3550 4450 3650
Wire Wire Line
	4450 3650 5200 3650
Wire Wire Line
	4150 3250 4150 3450
Wire Wire Line
	4150 3450 4450 3450
Wire Wire Line
	4750 3400 4750 3150
$Comp
L power:GND #PWR0125
U 1 1 5F951812
P 4750 4400
F 0 "#PWR0125" H 4750 4150 50  0001 C CNN
F 1 "GND" H 4755 4227 50  0000 C CNN
F 2 "" H 4750 4400 50  0001 C CNN
F 3 "" H 4750 4400 50  0001 C CNN
	1    4750 4400
	1    0    0    -1  
$EndComp
Connection ~ 4750 4400
$Comp
L power:+5V #PWR?
U 1 1 5F94F9D3
P 4750 2750
AR Path="/5F19D573/5F94F9D3" Ref="#PWR?"  Part="1" 
AR Path="/5F94411C/5F94F9D3" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0124" H 4750 2600 50  0001 C CNN
F 1 "+5V" H 4765 2923 50  0000 C CNN
F 2 "" H 4750 2750 50  0001 C CNN
F 3 "" H 4750 2750 50  0001 C CNN
	1    4750 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F95396D
P 4300 3150
AR Path="/5EEDC7D6/5F95396D" Ref="R?"  Part="1" 
AR Path="/5ED08686/5F95396D" Ref="R?"  Part="1" 
AR Path="/5F94411C/5F95396D" Ref="R83"  Part="1" 
F 0 "R83" H 4400 3150 50  0000 C CNN
F 1 "R 16K" H 4500 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4230 3150 50  0001 C CNN
F 3 "~" H 4300 3150 50  0001 C CNN
F 4 "1%" H 4300 3150 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 4300 3150 50  0001 C CNN "Digi-Key_PN"
	1    4300 3150
	0    1    -1   0   
$EndComp
$Comp
L dk_Tactile-Switches:1825910-6 S?
U 1 1 5F94F9E4
P 3950 3350
AR Path="/5F19D573/5F94F9E4" Ref="S?"  Part="1" 
AR Path="/5F94411C/5F94F9E4" Ref="S3"  Part="1" 
F 0 "S3" H 3800 3750 60  0000 C CNN
F 1 "1825910-6" H 3750 3650 60  0000 C CNN
F 2 "digikey-footprints:Switch_Tactile_THT_6x6mm" H 4150 3550 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1825910&DocType=Customer+Drawing&DocLang=English" H 4150 3650 60  0001 L CNN
F 4 "450-1650-ND" H 4150 3750 60  0001 L CNN "Digi-Key_PN"
F 5 "1825910-6" H 4150 3850 60  0001 L CNN "MPN"
F 6 "Switches" H 4150 3950 60  0001 L CNN "Category"
F 7 "Tactile Switches" H 4150 4050 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1825910&DocType=Customer+Drawing&DocLang=English" H 4150 4150 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-alcoswitch-switches/1825910-6/450-1650-ND/1632536" H 4150 4250 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH TACTILE SPST-NO 0.05A 24V" H 4150 4350 60  0001 L CNN "Description"
F 11 "TE Connectivity ALCOSWITCH Switches" H 4150 4450 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4150 4550 60  0001 L CNN "Status"
	1    3950 3350
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHCT1G02 U41
U 1 1 5F9546A4
P 4750 3500
F 0 "U41" H 4725 3767 50  0000 C CNN
F 1 "74AHCT1G02" H 4725 3676 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5" H 4750 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 4750 3500 50  0001 C CNN
F 4 "1727-4204-1-ND" H 4750 3500 50  0001 C CNN "Digi-Key_PN"
	1    4750 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3150 4150 3250
Connection ~ 4150 3250
Wire Wire Line
	4450 3150 4750 3150
Wire Wire Line
	4750 3150 4750 2750
Wire Wire Line
	3750 4400 4750 4400
$Comp
L dk_Logic-Gates-and-Inverters:SN74LVC1G14DBVR U?
U 1 1 5F9C818B
P 8100 4100
AR Path="/5EE8A13E/5F9C818B" Ref="U?"  Part="1" 
AR Path="/5F94411C/5F9C818B" Ref="U43"  Part="1" 
F 0 "U43" H 8200 4300 60  0000 L CNN
F 1 "SN74LVC1G14DBVR" H 8150 3850 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 8300 4300 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 8300 4400 60  0001 L CNN
F 4 "296-11607-1-ND" H 8300 4500 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G14DBVR" H 8300 4600 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 8300 4700 60  0001 L CNN "Category"
F 7 "Logic - Gates and Inverters" H 8300 4800 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 8300 4900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G14DBVR/296-11607-1-ND/385746" H 8300 5000 60  0001 L CNN "DK_Detail_Page"
F 10 "IC INVERTER SCHMITT 1CH SOT23-5" H 8300 5100 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 8300 5200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8300 5300 60  0001 L CNN "Status"
	1    8100 4100
	1    0    0    -1  
$EndComp
$Sheet
S 8450 3600 550  200 
U 5F9C8DE4
F0 "sheet5F9C8DE1" 50
F1 "green_led_indicator.sch" 50
F2 "!EN" I L 8450 3700 50 
$EndSheet
Wire Wire Line
	8000 4400 8000 4300
$Comp
L Device:C_Small C?
U 1 1 5F9CC6EC
P 2900 4300
AR Path="/5EB41214/5F9CC6EC" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F9CC6EC" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F9CC6EC" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F9CC6EC" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5F9CC6EC" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F9CC6EC" Ref="C?"  Part="1" 
AR Path="/5ED08686/5F9CC6EC" Ref="C?"  Part="1" 
AR Path="/5F94411C/5F9CC6EC" Ref="C79"  Part="1" 
F 0 "C79" H 2992 4346 50  0000 L CNN
F 1 "C 100n" H 2992 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2900 4300 50  0001 C CNN
F 3 "~" H 2900 4300 50  0001 C CNN
F 4 "16" H 2900 4300 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 2900 4300 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 2900 4300 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 2900 4300 50  0001 C CNN "Farnell_PN"
	1    2900 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F9CCA1D
P 3350 4300
AR Path="/5EB41214/5F9CCA1D" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F9CCA1D" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F9CCA1D" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F9CCA1D" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5F9CCA1D" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F9CCA1D" Ref="C?"  Part="1" 
AR Path="/5ED08686/5F9CCA1D" Ref="C?"  Part="1" 
AR Path="/5F94411C/5F9CCA1D" Ref="C80"  Part="1" 
F 0 "C80" H 3442 4346 50  0000 L CNN
F 1 "C 100n" H 3442 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3350 4300 50  0001 C CNN
F 3 "~" H 3350 4300 50  0001 C CNN
F 4 "16" H 3350 4300 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 3350 4300 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 3350 4300 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 3350 4300 50  0001 C CNN "Farnell_PN"
	1    3350 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 4400 3350 4400
Connection ~ 3750 4400
Connection ~ 3350 4400
Wire Wire Line
	3350 4400 3750 4400
$Comp
L power:+5V #PWR?
U 1 1 5F9CD403
P 2900 4150
AR Path="/5F19D573/5F9CD403" Ref="#PWR?"  Part="1" 
AR Path="/5F94411C/5F9CD403" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0123" H 2900 4000 50  0001 C CNN
F 1 "+5V" H 2915 4323 50  0000 C CNN
F 2 "" H 2900 4150 50  0001 C CNN
F 3 "" H 2900 4150 50  0001 C CNN
	1    2900 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 4150 2900 4200
Wire Wire Line
	3350 4200 2900 4200
Connection ~ 2900 4200
$Comp
L Jumper:SolderJumper_2_Bridged JP4
U 1 1 5FAE6ED8
P 6400 3300
F 0 "JP4" V 6300 3150 50  0000 L CNN
F 1 "SolderJumper_2_Bridged" V 6200 2400 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6400 3300 50  0001 C CNN
F 3 "~" H 6400 3300 50  0001 C CNN
	1    6400 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FAE7C1A
P 6400 2950
AR Path="/5EEDC7D6/5FAE7C1A" Ref="R?"  Part="1" 
AR Path="/5ED08686/5FAE7C1A" Ref="R?"  Part="1" 
AR Path="/5F94411C/5FAE7C1A" Ref="R84"  Part="1" 
F 0 "R84" H 6500 2950 50  0000 C CNN
F 1 "R 16K" H 6600 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6330 2950 50  0001 C CNN
F 3 "~" H 6400 2950 50  0001 C CNN
F 4 "1%" H 6400 2950 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 6400 2950 50  0001 C CNN "Digi-Key_PN"
	1    6400 2950
	1    0    0    1   
$EndComp
$Comp
L dk_Logic-Gates-and-Inverters:SN74LVC1G14DBVR U?
U 1 1 5FAEADF1
P 6900 3500
AR Path="/5EE8A13E/5FAEADF1" Ref="U?"  Part="1" 
AR Path="/5F94411C/5FAEADF1" Ref="U42"  Part="1" 
F 0 "U42" H 7000 3700 60  0000 L CNN
F 1 "SN74LVC1G14DBVR" H 6650 3150 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 7100 3700 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 7100 3800 60  0001 L CNN
F 4 "296-11607-1-ND" H 7100 3900 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G14DBVR" H 7100 4000 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 7100 4100 60  0001 L CNN "Category"
F 7 "Logic - Gates and Inverters" H 7100 4200 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 7100 4300 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G14DBVR/296-11607-1-ND/385746" H 7100 4400 60  0001 L CNN "DK_Detail_Page"
F 10 "IC INVERTER SCHMITT 1CH SOT23-5" H 7100 4500 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 7100 4600 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7100 4700 60  0001 L CNN "Status"
	1    6900 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 3700 6800 4400
Wire Wire Line
	6800 4400 7500 4400
$Comp
L Device:R R?
U 1 1 5FAEF74B
P 6400 4100
AR Path="/5FAEF74B" Ref="R?"  Part="1" 
AR Path="/5EE136D8/5FAEF74B" Ref="R?"  Part="1" 
AR Path="/5F94411C/5FAEF74B" Ref="R85"  Part="1" 
F 0 "R85" V 6300 4050 50  0000 L CNN
F 1 "R 84.5K" V 6500 3950 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6330 4100 50  0001 C CNN
F 3 "" H 6400 4100 50  0001 C CNN
F 4 "0.1%" H 6400 4100 50  0001 C CNN "Tolerance"
F 5 "P84.5KDACT-ND" H 6400 4100 50  0001 C CNN "Digi-Key_PN"
	1    6400 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2750 6400 2750
Wire Wire Line
	6400 2750 6400 2800
Connection ~ 4750 2750
Wire Wire Line
	6400 2750 6900 2750
Connection ~ 6400 2750
Wire Wire Line
	6900 3300 6900 2750
Connection ~ 6900 2750
Wire Wire Line
	4750 4400 6400 4400
Wire Wire Line
	6400 3100 6400 3150
Wire Wire Line
	6300 3500 6400 3500
Wire Wire Line
	6400 3450 6400 3500
Connection ~ 6400 3500
Wire Wire Line
	6400 3500 6600 3500
Wire Wire Line
	6400 3500 6400 3950
Wire Wire Line
	6400 4250 6400 4400
Connection ~ 6400 4400
Wire Wire Line
	6400 4400 6800 4400
$Comp
L Jumper:SolderJumper_2_Open JP3
U 1 1 5FAE4182
P 6150 3500
F 0 "JP3" H 6100 3650 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6150 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6150 3500 50  0001 C CNN
F 3 "~" H 6150 3500 50  0001 C CNN
	1    6150 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 3550 7500 3500
$Comp
L Device:R R?
U 1 1 5FB61892
P 7300 3500
AR Path="/5FB61892" Ref="R?"  Part="1" 
AR Path="/5EB41214/5FB61892" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5FB61892" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5FB61892" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5FB61892" Ref="R?"  Part="1" 
AR Path="/5EED7553/5FB61892" Ref="R?"  Part="1" 
AR Path="/5EEDC7D6/5FB61892" Ref="R?"  Part="1" 
AR Path="/5ED08686/5FB61892" Ref="R?"  Part="1" 
AR Path="/5F94411C/5FB61892" Ref="R86"  Part="1" 
F 0 "R86" V 7093 3500 50  0000 C CNN
F 1 "R 10" V 7184 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7230 3500 50  0001 C CNN
F 3 "~" H 7300 3500 50  0001 C CNN
F 4 "" H 7300 3500 50  0001 C CNN "Tolerance"
F 5 "RMCF0603FT10R0CT-ND" H 7300 3500 50  0001 C CNN "Digi-Key_PN"
	1    7300 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	7450 3500 7500 3500
Wire Wire Line
	7500 4400 7500 3750
$Comp
L Device:C_Small C?
U 1 1 5FB6189F
P 7500 3650
AR Path="/5EB41214/5FB6189F" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FB6189F" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FB6189F" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FB6189F" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5FB6189F" Ref="C?"  Part="1" 
AR Path="/5ECA36FF/5FB6189F" Ref="C?"  Part="1" 
AR Path="/5EED7553/5FB6189F" Ref="C?"  Part="1" 
AR Path="/5EEDC7D6/5FB6189F" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5FB6189F" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FB6189F" Ref="C?"  Part="1" 
AR Path="/5ED08686/5FB6189F" Ref="C?"  Part="1" 
AR Path="/5F94411C/5FB6189F" Ref="C81"  Part="1" 
F 0 "C81" H 7592 3696 50  0000 L CNN
F 1 "C 470p" H 7592 3605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7500 3650 50  0001 C CNN
F 3 "~" H 7500 3650 50  0001 C CNN
F 4 "16" H 7500 3650 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7500 3650 50  0001 C CNN "Tolerance"
F 6 "399-10051-1-ND" H 7500 3650 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 7500 3650 50  0001 C CNN "Farnell_PN"
	1    7500 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3500 7150 3500
Connection ~ 7500 4400
Wire Wire Line
	7500 4400 8000 4400
Connection ~ 6800 4400
Wire Wire Line
	7500 3500 7800 3500
Connection ~ 7500 3500
Wire Wire Line
	8100 2750 8100 3900
Wire Wire Line
	6900 2750 8100 2750
$Sheet
S 8450 4000 550  200 
U 5F9C29B9
F0 "Green LED Indicator" 50
F1 "green_led_indicator.sch" 50
F2 "!EN" I L 8450 4100 50 
$EndSheet
Wire Wire Line
	8300 4100 8450 4100
Wire Wire Line
	7800 3700 7800 3500
Wire Wire Line
	7800 4100 7800 3700
Connection ~ 7800 3700
Wire Wire Line
	7800 3700 8450 3700
Wire Wire Line
	5750 3500 6000 3500
Wire Wire Line
	4750 4400 4750 3600
Connection ~ 3750 3450
Wire Wire Line
	3750 3450 3750 4400
Connection ~ 4150 3450
Wire Wire Line
	3750 3250 3750 3450
Connection ~ 4750 3150
$EndSCHEMATC
