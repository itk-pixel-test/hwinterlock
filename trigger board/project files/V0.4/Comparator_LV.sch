EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 29 32
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5ECAD0C8
P 4150 2550
AR Path="/5ECAD0C8" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ECAD0C8" Ref="R60"  Part="1" 
AR Path="/5ED2BC2A/5ECAD0C8" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ECAD0C8" Ref="R?"  Part="1" 
AR Path="/5EBFB816/5ECAD0C8" Ref="R?"  Part="1" 
F 0 "R60" V 4357 2550 50  0000 C CNN
F 1 "R 10K " V 4266 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4080 2550 50  0001 C CNN
F 3 "~" H 4150 2550 50  0001 C CNN
F 4 "0.1%" V 4150 2550 50  0001 C CNN "Tolerance"
	1    4150 2550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4750 2600 4750 2550
$Comp
L Device:C_Small C55
U 1 1 5ECAD0C5
P 7650 3450
AR Path="/5EB41214/5ECAD0C5" Ref="C55"  Part="1" 
AR Path="/5ED2BC2A/5ECAD0C5" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ECAD0C5" Ref="C?"  Part="1" 
AR Path="/5EBFB816/5ECAD0C5" Ref="C?"  Part="1" 
F 0 "C55" H 7742 3496 50  0000 L CNN
F 1 "C 47n" H 7742 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7650 3450 50  0001 C CNN
F 3 "~" H 7650 3450 50  0001 C CNN
F 4 "16" H 7650 3450 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7650 3450 50  0001 C CNN "Tolerance"
	1    7650 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 3300 7650 3350
$Comp
L Device:C_Small C52
U 1 1 5EE8A62C
P 5000 3450
AR Path="/5EB41214/5EE8A62C" Ref="C52"  Part="1" 
AR Path="/5ED2BC2A/5EE8A62C" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5EE8A62C" Ref="C?"  Part="1" 
AR Path="/5EBFB816/5EE8A62C" Ref="C?"  Part="1" 
F 0 "C52" H 5092 3496 50  0000 L CNN
F 1 "C 47n" H 5092 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5000 3450 50  0001 C CNN
F 3 "~" H 5000 3450 50  0001 C CNN
F 4 "16" H 5000 3450 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5000 3450 50  0001 C CNN "Tolerance"
	1    5000 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3550 5000 3550
Wire Wire Line
	5000 3200 5000 3350
Wire Wire Line
	4750 3200 4750 3550
Wire Wire Line
	4300 2550 4350 2550
Wire Wire Line
	4550 2800 4350 2800
Wire Wire Line
	4350 2800 4350 2550
Connection ~ 4350 2550
Connection ~ 4750 2550
Wire Wire Line
	4750 2550 4750 2250
$Comp
L comparators_magne:MAX9141 U37
U 1 1 5ECAD0C6
P 4850 2900
AR Path="/5EB41214/5ECAD0C6" Ref="U37"  Part="1" 
AR Path="/5ED2BC2A/5ECAD0C6" Ref="U?"  Part="1" 
AR Path="/5EE0AC5D/5ECAD0C6" Ref="U?"  Part="1" 
AR Path="/5EBFB816/5ECAD0C6" Ref="U?"  Part="1" 
F 0 "U37" H 4950 3150 50  0000 L CNN
F 1 "MAX9141ESA+-ND" H 4950 3050 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4850 2900 50  0001 C CNN
F 3 "" H 4850 2900 50  0001 C CNN
	1    4850 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3200 4850 3200
Wire Wire Line
	7750 2950 8300 2950
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:SN74LVC1G07DBVR U?
U 1 1 5EEE408A
P 6550 2900
AR Path="/5ED2BC2A/5EEE408A" Ref="U?"  Part="1" 
AR Path="/5EB41214/5EEE408A" Ref="U39"  Part="1" 
F 0 "U39" H 6650 2550 60  0000 L CNN
F 1 "SN74LVC1G07DBVR" H 6650 2450 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 6750 3100 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g07" H 6750 3200 60  0001 L CNN
F 4 "296-8485-1-ND" H 6750 3300 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G07DBVR" H 6750 3400 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 6750 3500 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 6750 3600 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g07" H 6750 3700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G07DBVR/296-8485-1-ND/377454" H 6750 3800 60  0001 L CNN "DK_Detail_Page"
F 10 "IC BUF NON-INVERT 5.5V SOT23-5" H 6750 3900 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 6750 4000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6750 4100 60  0001 L CNN "Status"
	1    6550 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3200 6550 3550
Wire Wire Line
	6550 2500 6550 2600
$Comp
L Device:C_Small C54
U 1 1 5EEE4094
P 5750 3200
AR Path="/5EB41214/5EEE4094" Ref="C54"  Part="1" 
AR Path="/5ED2BC2A/5EEE4094" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5EEE4094" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5EEE4094" Ref="C?"  Part="1" 
F 0 "C54" H 5842 3246 50  0000 L CNN
F 1 "C 1u" H 5842 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5750 3200 50  0001 C CNN
F 3 "~" H 5750 3200 50  0001 C CNN
F 4 "16" H 5750 3200 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5750 3200 50  0001 C CNN "Tolerance"
	1    5750 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EEE409A
P 5600 2900
AR Path="/5EEE409A" Ref="R?"  Part="1" 
AR Path="/5EB41214/5EEE409A" Ref="R64"  Part="1" 
AR Path="/5ED2BC2A/5EEE409A" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5EEE409A" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5EEE409A" Ref="R?"  Part="1" 
F 0 "R64" V 5393 2900 50  0000 C CNN
F 1 "R 150K" V 5484 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5530 2900 50  0001 C CNN
F 3 "~" H 5600 2900 50  0001 C CNN
F 4 "1%" H 5600 2900 50  0001 C CNN "Tolerance"
	1    5600 2900
	0    1    1    0   
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5EEE40A0
P 5600 2500
AR Path="/5ED2BC2A/5EEE40A0" Ref="D?"  Part="1" 
AR Path="/5EB41214/5EEE40A0" Ref="D15"  Part="1" 
F 0 "D15" H 5600 2717 50  0000 C CNN
F 1 "CUS520,H3F" H 5600 2626 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 5600 2500 50  0001 C CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=7041&prodName=CUS520" H 5600 2500 50  0001 C CNN
	1    5600 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2500 5750 2900
Connection ~ 5750 2900
Wire Wire Line
	5750 2900 6250 2900
Wire Wire Line
	5750 2900 5750 3100
Wire Wire Line
	5150 2900 5450 2900
Text Notes 5750 2150 0    50   ~ 0
200ms delay
Wire Wire Line
	6850 2900 7200 2900
Wire Wire Line
	5000 3550 5750 3550
Connection ~ 5000 3550
Connection ~ 6550 3550
Wire Notes Line
	5250 2200 5250 3350
Wire Notes Line
	5250 3350 6850 3350
Wire Notes Line
	6850 3350 6850 2200
Wire Notes Line
	6850 2200 5250 2200
Wire Wire Line
	5750 3300 5750 3550
Connection ~ 5750 3550
Wire Wire Line
	5750 3550 6550 3550
Text HLabel 8300 2950 2    50   Input ~ 0
Isens_interlock
Wire Wire Line
	7200 1600 7200 2150
Wire Wire Line
	3850 1850 3850 1600
Wire Wire Line
	3850 1600 7200 1600
Wire Wire Line
	4350 2550 4400 2550
Wire Wire Line
	4700 2550 4750 2550
$Comp
L Device:R R?
U 1 1 5ECAD0C7
P 4550 2550
AR Path="/5ECAD0C7" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ECAD0C7" Ref="R62"  Part="1" 
AR Path="/5ED2BC2A/5ECAD0C7" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ECAD0C7" Ref="R?"  Part="1" 
AR Path="/5EBFB816/5ECAD0C7" Ref="R?"  Part="1" 
F 0 "R62" V 4343 2550 50  0000 C CNN
F 1 "R" V 4434 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4480 2550 50  0001 C CNN
F 3 "~" H 4550 2550 50  0001 C CNN
F 4 "1%" H 4550 2550 50  0001 C CNN "Tolerance"
	1    4550 2550
	0    1    1    0   
$EndComp
Text GLabel 7200 3200 0    50   Input ~ 0
!ARM_LVsens
Text GLabel 3850 1950 0    50   Input ~ 0
!LE_LVsens
Wire Wire Line
	7200 3000 7200 3200
Wire Wire Line
	7200 2150 7200 2900
Connection ~ 7200 2150
Connection ~ 7200 2900
Text Notes 2700 2900 0    50   ~ 0
What to set as limit? Chip abs max is 4A.\nExpected consumption is 1.1A.
Connection ~ 5450 2900
Wire Wire Line
	5450 2500 5450 2900
$Comp
L power:+5VA #PWR0115
U 1 1 5EC9E1F7
P 4750 2250
F 0 "#PWR0115" H 4750 2100 50  0001 C CNN
F 1 "+5VA" H 4765 2423 50  0000 C CNN
F 2 "" H 4750 2250 50  0001 C CNN
F 3 "" H 4750 2250 50  0001 C CNN
	1    4750 2250
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR0120
U 1 1 5EC9E437
P 6550 2500
F 0 "#PWR0120" H 6550 2350 50  0001 C CNN
F 1 "+5VA" H 6565 2673 50  0000 C CNN
F 2 "" H 6550 2500 50  0001 C CNN
F 3 "" H 6550 2500 50  0001 C CNN
	1    6550 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR0117
U 1 1 5EC9E68C
P 5000 3200
F 0 "#PWR0117" H 5000 3050 50  0001 C CNN
F 1 "+5VA" H 5015 3373 50  0000 C CNN
F 2 "" H 5000 3200 50  0001 C CNN
F 3 "" H 5000 3200 50  0001 C CNN
	1    5000 3200
	1    0    0    -1  
$EndComp
Connection ~ 5000 3200
$Comp
L power:+5VA #PWR0121
U 1 1 5ECA8A8C
P 7650 3300
F 0 "#PWR0121" H 7650 3150 50  0001 C CNN
F 1 "+5VA" H 7665 3473 50  0000 C CNN
F 2 "" H 7650 3300 50  0001 C CNN
F 3 "" H 7650 3300 50  0001 C CNN
	1    7650 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR0116
U 1 1 5ECAA11B
P 4750 3550
F 0 "#PWR0116" H 4750 3300 50  0001 C CNN
F 1 "GND1" H 4755 3377 50  0000 C CNN
F 2 "" H 4750 3550 50  0001 C CNN
F 3 "" H 4750 3550 50  0001 C CNN
	1    4750 3550
	1    0    0    -1  
$EndComp
Connection ~ 4750 3550
$Comp
L power:GND1 #PWR0112
U 1 1 5ECAA248
P 3800 2550
F 0 "#PWR0112" H 3800 2300 50  0001 C CNN
F 1 "GND1" H 3805 2377 50  0000 C CNN
F 2 "" H 3800 2550 50  0001 C CNN
F 3 "" H 3800 2550 50  0001 C CNN
	1    3800 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3550 7650 3550
$Comp
L logic_gates_magne:74LVC2G32 U35
U 1 1 5ECD3981
P 4400 1900
F 0 "U35" H 4375 2167 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 4375 2076 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 4550 1950 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 4600 1900 50  0001 C CNN
	1    4400 1900
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U35
U 2 1 5ECD45CA
P 7500 2950
F 0 "U35" H 7475 3217 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 7475 3126 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 7650 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 7700 2950 50  0001 C CNN
	2    7500 2950
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U35
U 3 1 5ECD4E7B
P 8100 3300
F 0 "U35" H 8153 3346 50  0000 L CNN
F 1 "SN74LVC2G32DCUR" H 8153 3255 50  0000 L CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 8250 3350 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 8300 3300 50  0001 C CNN
	3    8100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 3300 7900 3300
Wire Wire Line
	7900 3300 7900 3050
Wire Wire Line
	7900 3050 8100 3050
Wire Wire Line
	3850 1950 4100 1950
Wire Wire Line
	3850 1850 4100 1850
Connection ~ 7650 3300
Wire Wire Line
	7650 3550 8100 3550
Connection ~ 7650 3550
Wire Wire Line
	4650 1900 4900 1900
$Comp
L Device:C_Small C51
U 1 1 5ED0D1FD
P 4900 5850
AR Path="/5EB41214/5ED0D1FD" Ref="C51"  Part="1" 
AR Path="/5ED2BC2A/5ED0D1FD" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5ED0D1FD" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5ED0D1FD" Ref="C?"  Part="1" 
F 0 "C51" H 4992 5896 50  0000 L CNN
F 1 "C 47n" H 4992 5805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4900 5850 50  0001 C CNN
F 3 "~" H 4900 5850 50  0001 C CNN
F 4 "16" H 4900 5850 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 4900 5850 50  0001 C CNN "Tolerance"
	1    4900 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 5700 4900 5750
Wire Wire Line
	4650 5600 4650 5950
Wire Wire Line
	4750 5700 4750 5600
Wire Wire Line
	4900 5700 4750 5700
Wire Wire Line
	5000 5700 4900 5700
Connection ~ 4900 5700
Connection ~ 4900 5950
Wire Notes Line
	5100 4600 5100 5700
Wire Notes Line
	5100 5700 6800 5700
Wire Wire Line
	4650 5950 4900 5950
Connection ~ 4650 5950
Wire Wire Line
	3700 4250 3900 4250
Wire Wire Line
	7650 5700 7650 5750
Wire Wire Line
	4450 4300 4800 4300
$Comp
L Device:C_Small C56
U 1 1 5ED0D214
P 7650 5850
AR Path="/5EB41214/5ED0D214" Ref="C56"  Part="1" 
AR Path="/5ED2BC2A/5ED0D214" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5ED0D214" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5ED0D214" Ref="C?"  Part="1" 
F 0 "C56" H 7742 5896 50  0000 L CNN
F 1 "C 47n" H 7742 5805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7650 5850 50  0001 C CNN
F 3 "~" H 7650 5850 50  0001 C CNN
F 4 "16" H 7650 5850 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7650 5850 50  0001 C CNN "Tolerance"
	1    7650 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 4000 3700 4250
Text GLabel 3800 4350 0    50   Input ~ 0
!LE_LVsens
Wire Wire Line
	3800 4350 3900 4350
Text HLabel 8150 5350 2    50   Input ~ 0
Vsens_interlock
Wire Wire Line
	7200 4400 7200 5300
Wire Wire Line
	7400 4400 7200 4400
Wire Notes Line
	6800 4600 5100 4600
Wire Notes Line
	6800 5700 6800 4600
Text Notes 5650 4550 0    50   ~ 0
200ms delay
Wire Wire Line
	5050 5300 5350 5300
Wire Wire Line
	4900 5950 5650 5950
Connection ~ 5650 5950
Wire Wire Line
	5650 5700 5650 5950
Wire Wire Line
	5650 5300 5650 5500
Wire Wire Line
	5650 5300 6150 5300
Wire Wire Line
	5350 4900 5350 5300
Connection ~ 5650 5300
Wire Wire Line
	5650 4900 5650 5300
$Comp
L Device:D_Schottky D?
U 1 1 5ED0D22F
P 5500 4900
AR Path="/5ED2BC2A/5ED0D22F" Ref="D?"  Part="1" 
AR Path="/5EB41214/5ED0D22F" Ref="D14"  Part="1" 
F 0 "D14" H 5500 5117 50  0000 C CNN
F 1 "CUS520,H3F" H 5500 5026 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 5500 4900 50  0001 C CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=7041&prodName=CUS520" H 5500 4900 50  0001 C CNN
	1    5500 4900
	1    0    0    -1  
$EndComp
Connection ~ 5350 5300
$Comp
L Device:R R?
U 1 1 5ED0D237
P 5500 5300
AR Path="/5ED0D237" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED0D237" Ref="R63"  Part="1" 
AR Path="/5ED2BC2A/5ED0D237" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5ED0D237" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5ED0D237" Ref="R?"  Part="1" 
F 0 "R63" V 5293 5300 50  0000 C CNN
F 1 "R 150K" V 5384 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5430 5300 50  0001 C CNN
F 3 "~" H 5500 5300 50  0001 C CNN
F 4 "1%" H 5500 5300 50  0001 C CNN "Tolerance"
	1    5500 5300
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C53
U 1 1 5ED0D23F
P 5650 5600
AR Path="/5EB41214/5ED0D23F" Ref="C53"  Part="1" 
AR Path="/5ED2BC2A/5ED0D23F" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5ED0D23F" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5ED0D23F" Ref="C?"  Part="1" 
F 0 "C53" H 5742 5646 50  0000 L CNN
F 1 "C 1u" H 5742 5555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5650 5600 50  0001 C CNN
F 3 "~" H 5650 5600 50  0001 C CNN
F 4 "16" H 5650 5600 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5650 5600 50  0001 C CNN "Tolerance"
	1    5650 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 4900 6450 5000
Wire Wire Line
	5650 5950 6450 5950
Wire Wire Line
	6450 5600 6450 5950
Wire Wire Line
	6750 5300 7200 5300
Text Notes 3950 4650 0    50   ~ 0
2.0V setpoint
Text GLabel 7200 5500 0    50   Input ~ 0
!ARM_LVsens
Wire Wire Line
	7750 5350 8150 5350
Wire Wire Line
	4650 4650 4650 4950
Wire Wire Line
	7200 5500 7200 5400
Wire Wire Line
	4500 4950 4650 4950
Wire Wire Line
	4100 4950 4200 4950
Wire Wire Line
	4100 5200 4100 4950
Wire Wire Line
	4450 5200 4100 5200
Connection ~ 4100 4950
Wire Wire Line
	4000 4950 4100 4950
Connection ~ 4650 4950
Wire Wire Line
	4650 5000 4650 4950
$Comp
L Device:R R?
U 1 1 5ED0D259
P 4350 4950
AR Path="/5ED0D259" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED0D259" Ref="R61"  Part="1" 
AR Path="/5ED2BC2A/5ED0D259" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5ED0D259" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5ED0D259" Ref="R?"  Part="1" 
F 0 "R61" V 4557 4950 50  0000 C CNN
F 1 "R 15K" V 4466 4950 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4280 4950 50  0001 C CNN
F 3 "~" H 4350 4950 50  0001 C CNN
F 4 "0.1%" H 4350 4950 50  0001 C CNN "Tolerance"
	1    4350 4950
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5ED0D260
P 3850 4950
AR Path="/5ED0D260" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED0D260" Ref="R59"  Part="1" 
AR Path="/5ED2BC2A/5ED0D260" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5ED0D260" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5ED0D260" Ref="R?"  Part="1" 
F 0 "R59" V 3643 4950 50  0000 C CNN
F 1 "R 10K" V 3734 4950 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3780 4950 50  0001 C CNN
F 3 "~" H 3850 4950 50  0001 C CNN
F 4 "0.1%" H 3850 4950 50  0001 C CNN "Tolerance"
	1    3850 4950
	0    1    1    0   
$EndComp
$Sheet
S 7400 4300 550  200 
U 5ED0D263
F0 "sheet5ED0D1F5" 50
F1 "R_G_LED_indicator_LVsens.sch" 50
F2 "Cntrl" I L 7400 4400 50 
$EndSheet
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:SN74LVC1G07DBVR U?
U 1 1 5ED0D272
P 6450 5300
AR Path="/5ED2BC2A/5ED0D272" Ref="U?"  Part="1" 
AR Path="/5EB41214/5ED0D272" Ref="U38"  Part="1" 
F 0 "U38" H 6550 4950 60  0000 L CNN
F 1 "SN74LVC1G07DBVR" H 6550 4850 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 6650 5500 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g07" H 6650 5600 60  0001 L CNN
F 4 "296-8485-1-ND" H 6650 5700 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G07DBVR" H 6650 5800 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 6650 5900 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 6650 6000 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g07" H 6650 6100 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G07DBVR/296-8485-1-ND/377454" H 6650 6200 60  0001 L CNN "DK_Detail_Page"
F 10 "IC BUF NON-INVERT 5.5V SOT23-5" H 6650 6300 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 6650 6400 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6650 6500 60  0001 L CNN "Status"
	1    6450 5300
	1    0    0    -1  
$EndComp
$Comp
L comparators_magne:MAX9141 U36
U 1 1 5ED0D278
P 4750 5300
AR Path="/5EB41214/5ED0D278" Ref="U36"  Part="1" 
AR Path="/5ED2BC2A/5ED0D278" Ref="U?"  Part="1" 
AR Path="/5ECA2575/5ED0D278" Ref="U?"  Part="1" 
AR Path="/5ECAC95D/5ED0D278" Ref="U?"  Part="1" 
F 0 "U36" H 4900 5550 50  0000 L CNN
F 1 "MAX9141ESA+-ND" H 4900 5450 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4750 5300 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX9140-MAX9144.pdf" H 4750 5300 50  0001 C CNN
	1    4750 5300
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5ED0D27E
P 7650 5700
AR Path="/5ED2BC2A/5ED0D27E" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED0D27E" Ref="#PWR0122"  Part="1" 
F 0 "#PWR0122" H 7650 5550 50  0001 C CNN
F 1 "+5VA" H 7665 5873 50  0000 C CNN
F 2 "" H 7650 5700 50  0001 C CNN
F 3 "" H 7650 5700 50  0001 C CNN
	1    7650 5700
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5ED0D284
P 4650 4650
AR Path="/5ED2BC2A/5ED0D284" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED0D284" Ref="#PWR0113"  Part="1" 
F 0 "#PWR0113" H 4650 4500 50  0001 C CNN
F 1 "+5VA" H 4665 4823 50  0000 C CNN
F 2 "" H 4650 4650 50  0001 C CNN
F 3 "" H 4650 4650 50  0001 C CNN
	1    4650 4650
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5ED0D28A
P 5000 5700
AR Path="/5ED2BC2A/5ED0D28A" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED0D28A" Ref="#PWR0118"  Part="1" 
F 0 "#PWR0118" H 5000 5550 50  0001 C CNN
F 1 "+5VA" H 5015 5873 50  0000 C CNN
F 2 "" H 5000 5700 50  0001 C CNN
F 3 "" H 5000 5700 50  0001 C CNN
	1    5000 5700
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR?
U 1 1 5ED0D290
P 4650 5950
AR Path="/5ED2BC2A/5ED0D290" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED0D290" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 4650 5700 50  0001 C CNN
F 1 "GND1" H 4655 5777 50  0000 C CNN
F 2 "" H 4650 5950 50  0001 C CNN
F 3 "" H 4650 5950 50  0001 C CNN
	1    4650 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR?
U 1 1 5ED0D296
P 3550 4950
AR Path="/5ED2BC2A/5ED0D296" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED0D296" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 3550 4700 50  0001 C CNN
F 1 "GND1" H 3555 4777 50  0000 C CNN
F 2 "" H 3550 4950 50  0001 C CNN
F 3 "" H 3550 4950 50  0001 C CNN
	1    3550 4950
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5ED0D29C
P 6450 4900
AR Path="/5ED2BC2A/5ED0D29C" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED0D29C" Ref="#PWR0119"  Part="1" 
F 0 "#PWR0119" H 6450 4750 50  0001 C CNN
F 1 "+5VA" H 6465 5073 50  0000 C CNN
F 2 "" H 6450 4900 50  0001 C CNN
F 3 "" H 6450 4900 50  0001 C CNN
	1    6450 4900
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U?
U 1 1 5ED0D2A2
P 4200 4300
AR Path="/5ED2BC2A/5ED0D2A2" Ref="U?"  Part="1" 
AR Path="/5EB41214/5ED0D2A2" Ref="U34"  Part="1" 
F 0 "U34" H 4175 4567 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 4175 4476 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 4350 4350 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 4400 4300 50  0001 C CNN
	1    4200 4300
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U?
U 2 1 5ED0D2A8
P 7500 5350
AR Path="/5ED2BC2A/5ED0D2A8" Ref="U?"  Part="2" 
AR Path="/5EB41214/5ED0D2A8" Ref="U34"  Part="2" 
F 0 "U34" H 7475 5617 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 7475 5526 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 7650 5400 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 7700 5350 50  0001 C CNN
	2    7500 5350
	1    0    0    -1  
$EndComp
Connection ~ 7200 5300
$Comp
L logic_gates_magne:74LVC2G32 U?
U 3 1 5ED0D2AF
P 7950 5700
AR Path="/5ED2BC2A/5ED0D2AF" Ref="U?"  Part="3" 
AR Path="/5EB41214/5ED0D2AF" Ref="U34"  Part="3" 
F 0 "U34" H 8003 5746 50  0000 L CNN
F 1 "SN74LVC2G32DCUR" H 8003 5655 50  0000 L CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 8100 5750 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 8150 5700 50  0001 C CNN
	3    7950 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 4950 3700 4950
Wire Wire Line
	7650 5950 7950 5950
Wire Wire Line
	7650 5700 7850 5700
Wire Wire Line
	7850 5700 7850 5450
Wire Wire Line
	7850 5450 7950 5450
Connection ~ 7650 5700
Wire Wire Line
	7650 5950 6450 5950
Connection ~ 7650 5950
Connection ~ 6450 5950
Text HLabel 4150 5400 0    50   Input ~ 0
Voltage
Text HLabel 4200 3000 0    50   Input ~ 0
Current
Wire Wire Line
	3700 4000 7200 4000
Wire Wire Line
	4800 4300 4800 5000
Wire Wire Line
	7200 4400 7200 4000
Connection ~ 7200 4400
Wire Wire Line
	4900 1900 4900 2600
Wire Wire Line
	7400 2150 7200 2150
$Sheet
S 7400 2050 550  200 
U 5F076AF2
F0 "RED/GREEN LED indicator" 50
F1 "R_G_LED_indicator_LVsens.sch" 50
F2 "Cntrl" I L 7400 2150 50 
$EndSheet
Wire Wire Line
	4200 3000 4550 3000
Wire Wire Line
	4150 5400 4450 5400
Wire Wire Line
	3800 2550 4000 2550
$EndSCHEMATC
