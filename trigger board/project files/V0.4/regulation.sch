EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Switching:LMR16006YQ5 U20
U 1 1 5FC022D1
P 5500 3400
F 0 "U20" H 5500 3867 50  0000 C CNN
F 1 "LMR16006YQ5" H 5500 3776 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 5500 2900 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lmr16006y-q1.pdf" H 5100 3850 50  0001 C CNN
F 4 "296-42515-1-ND" H 5500 3400 50  0001 C CNN "Digi-Key_PN"
	1    5500 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Small L1
U 1 1 5FC093DC
P 6550 3400
F 0 "L1" V 6735 3400 50  0000 C CNN
F 1 "47uH" V 6644 3400 50  0000 C CNN
F 2 "Inductor_THT:L_Radial_D7.8mm_P5.00mm_Fastron_07HCP" H 6550 3400 50  0001 C CNN
F 3 "http://products.sumida.com/products/pdf/RCR-875D.pdf" H 6550 3400 50  0001 C CNN
F 4 "308-2116-ND" H 6550 3400 50  0001 C CNN "Digi-Key_PN"
	1    6550 3400
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FC0B59A
P 4200 3300
AR Path="/5EB41214/5FC0B59A" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FC0B59A" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FC0B59A" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FC0B59A" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5FC0B59A" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FC0B59A" Ref="C?"  Part="1" 
AR Path="/5ED08686/5FC0B59A" Ref="C?"  Part="1" 
AR Path="/5F19D573/5FC01F23/5FC0B59A" Ref="C34"  Part="1" 
F 0 "C34" H 4292 3346 50  0000 L CNN
F 1 "C 2.2u" H 4292 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4200 3300 50  0001 C CNN
F 3 "~" H 4200 3300 50  0001 C CNN
F 4 "25" H 4200 3300 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 4200 3300 50  0001 C CNN "Tolerance"
F 6 "490-4787-1-ND" H 4200 3300 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 4200 3300 50  0001 C CNN "Farnell_PN"
	1    4200 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FC0C075
P 6100 3300
AR Path="/5EB41214/5FC0C075" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FC0C075" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FC0C075" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FC0C075" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5FC0C075" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FC0C075" Ref="C?"  Part="1" 
AR Path="/5F19D573/5FC01F23/5FC0C075" Ref="C35"  Part="1" 
F 0 "C35" H 6192 3346 50  0000 L CNN
F 1 "C 1u" H 6192 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6100 3300 50  0001 C CNN
F 3 "~" H 6100 3300 50  0001 C CNN
F 4 "16" H 6100 3300 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 6100 3300 50  0001 C CNN "Tolerance"
F 6 "311-1365-1-ND" H 6100 3300 50  0001 C CNN "Digi-Key_PN"
F 7 "2547037" H 6100 3300 50  0001 C CNN "Farnell_PN"
	1    6100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3200 6100 3200
Wire Wire Line
	6000 3400 6100 3400
Wire Wire Line
	6100 3400 6450 3400
Connection ~ 6100 3400
$Comp
L Device:C C?
U 1 1 5FC0C7E2
P 7050 3550
AR Path="/5EEDC7D6/5FC0C7E2" Ref="C?"  Part="1" 
AR Path="/5F19D573/5FC01F23/5FC0C7E2" Ref="C36"  Part="1" 
F 0 "C36" H 7250 3500 50  0000 R CNN
F 1 "C 4.7u" H 7450 3600 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7088 3400 50  0001 C CNN
F 3 "~" H 7050 3550 50  0001 C CNN
F 4 "490-11992-1-ND" H 7050 3550 50  0001 C CNN "Digi-Key_PN"
	1    7050 3550
	1    0    0    1   
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5FC0DD76
P 6100 3700
AR Path="/5ECF279F/5FC0DD76" Ref="D?"  Part="1" 
AR Path="/5F19D573/5FC01F23/5FC0DD76" Ref="D5"  Part="1" 
F 0 "D5" V 6054 3780 50  0000 L CNN
F 1 "PMEG2020EJF" V 6145 3780 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 6100 3700 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/PMEG2020EH_EJ.pdf" H 6100 3700 50  0001 C CNN
F 4 "1727-7474-1-ND" H 6100 3700 50  0001 C CNN "Digi-Key_PN"
	1    6100 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 3550 6100 3400
Wire Wire Line
	6650 3400 7050 3400
Wire Wire Line
	5500 3800 5500 4000
Wire Wire Line
	5500 4000 6100 4000
Wire Wire Line
	7050 4000 7050 3700
Wire Wire Line
	6100 3850 6100 4000
Connection ~ 6100 4000
Wire Wire Line
	4200 3200 4700 3200
Wire Wire Line
	4200 3400 4200 4000
Wire Wire Line
	4200 4000 5500 4000
Connection ~ 5500 4000
$Comp
L Device:R R?
U 1 1 5FC11982
P 4700 3350
AR Path="/5FC11982" Ref="R?"  Part="1" 
AR Path="/5EB41214/5FC11982" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5FC11982" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5FC11982" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5FC11982" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5FC11982" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5FC11982" Ref="R?"  Part="1" 
AR Path="/5EE136D8/5FC11982" Ref="R?"  Part="1" 
AR Path="/5F19D573/5FC01F23/5FC11982" Ref="R34"  Part="1" 
F 0 "R34" V 4907 3350 50  0000 C CNN
F 1 "R 16K" V 4816 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4630 3350 50  0001 C CNN
F 3 "~" H 4700 3350 50  0001 C CNN
F 4 "1%" H 4700 3350 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 4700 3350 50  0001 C CNN "Digi-Key_PN"
	1    4700 3350
	1    0    0    -1  
$EndComp
Connection ~ 4700 3200
Wire Wire Line
	4700 3200 5000 3200
Wire Wire Line
	4700 3500 5000 3500
Wire Wire Line
	5000 3500 5000 3400
Text HLabel 4200 3200 0    50   Input ~ 0
Vin
Text HLabel 7050 3400 2    50   Input ~ 0
Vout
$Comp
L power:GND #PWR046
U 1 1 5FC36960
P 5500 4000
F 0 "#PWR046" H 5500 3750 50  0001 C CNN
F 1 "GND" H 5505 3827 50  0000 C CNN
F 2 "" H 5500 4000 50  0001 C CNN
F 3 "" H 5500 4000 50  0001 C CNN
	1    5500 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 4000 7050 4000
Wire Wire Line
	6650 3600 6650 3400
Wire Wire Line
	6000 3600 6650 3600
Connection ~ 6650 3400
$EndSCHEMATC
