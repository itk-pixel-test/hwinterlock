EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3350 3500 0    50   Input ~ 0
Input
$Comp
L power:GND #PWR?
U 1 1 5EB4F76B
P 6300 4250
AR Path="/5EB41214/5EB4F76B" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5EB4F76B" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5EB4F76B" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5EB4F76B" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5EB4F76B" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 6300 4000 50  0001 C CNN
F 1 "GND" H 6305 4077 50  0000 C CNN
F 2 "" H 6300 4250 50  0001 C CNN
F 3 "" H 6300 4250 50  0001 C CNN
	1    6300 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EBE233E
P 5850 4150
AR Path="/5EB41214/5EBE233E" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5EBE233E" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5EBE233E" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5EBE233E" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5EBE233E" Ref="C19"  Part="1" 
F 0 "C19" H 5942 4196 50  0000 L CNN
F 1 "C 100n" H 5942 4105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5850 4150 50  0001 C CNN
F 3 "~" H 5850 4150 50  0001 C CNN
F 4 "16" H 5850 4150 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5850 4150 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 5850 4150 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 5850 4150 50  0001 C CNN "Farnell_PN"
	1    5850 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4250 5850 4250
Connection ~ 5850 4250
Wire Wire Line
	5850 4250 6300 4250
$Comp
L power:+5V #PWR?
U 1 1 5EBE3AA3
P 5850 4000
AR Path="/5EB41214/5EBE3AA3" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5EBE3AA3" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5EBE3AA3" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5EBE3AA3" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5EBE3AA3" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 5850 3850 50  0001 C CNN
F 1 "+5V" H 5865 4173 50  0000 C CNN
F 2 "" H 5850 4000 50  0001 C CNN
F 3 "" H 5850 4000 50  0001 C CNN
	1    5850 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 4000 5850 4050
$Comp
L power:+5V #PWR?
U 1 1 5EB48014
P 5600 3200
AR Path="/5EB41214/5EB48014" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5EB48014" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5EB48014" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5EB48014" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5EB48014" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 5600 3050 50  0001 C CNN
F 1 "+5V" H 5615 3373 50  0000 C CNN
F 2 "" H 5600 3200 50  0001 C CNN
F 3 "" H 5600 3200 50  0001 C CNN
	1    5600 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3900 5600 4250
Connection ~ 5600 4250
Wire Wire Line
	5850 4000 5700 4000
Wire Wire Line
	5700 4000 5700 3900
Connection ~ 5850 4000
Connection ~ 6300 4250
Text HLabel 8300 3700 2    50   Input ~ 0
Output
Wire Wire Line
	6250 3600 6950 3600
Connection ~ 6250 3600
Wire Wire Line
	6000 3600 6250 3600
Wire Wire Line
	7150 2750 6950 2750
Wire Wire Line
	6950 2750 6950 3600
Wire Wire Line
	4900 2400 4950 2400
Text GLabel 4900 2400 0    50   Input ~ 0
!LE
Wire Wire Line
	5500 2350 5750 2350
Wire Wire Line
	4950 2050 6250 2050
Wire Wire Line
	4950 2050 4950 2300
Wire Wire Line
	6300 4250 7250 4250
$Comp
L power:+5V #PWR?
U 1 1 5F181128
P 7250 3500
AR Path="/5EB41214/5F181128" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5F181128" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5F181128" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5F181128" Ref="#PWR?"  Part="1" 
AR Path="/5EE5AAE3/5F181128" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5F181128" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 7250 3350 50  0001 C CNN
F 1 "+5V" H 7265 3673 50  0000 C CNN
F 2 "" H 7250 3500 50  0001 C CNN
F 3 "" H 7250 3500 50  0001 C CNN
	1    7250 3500
	-1   0    0    -1  
$EndComp
$Comp
L 74xGxx:74LVC1G332 U?
U 1 1 5F181131
P 7250 3700
AR Path="/5EE5AAE3/5F181131" Ref="U?"  Part="1" 
AR Path="/5EE77D5A/5F181131" Ref="U13"  Part="1" 
F 0 "U13" H 7400 4050 50  0000 C CNN
F 1 "SN74LVC1G332DCKRG4" H 7600 3950 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6_Handsoldering" H 7250 3700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74lvc1g332.pdf?HQS=TI-null-null-digikeymode-df-pf-null-wwe&ts=1590142069492" H 7250 3700 50  0001 C CNN
F 4 "296-51594-1-ND" H 7250 3700 50  0001 C CNN "Digi-Key_PN"
	1    7250 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 3500 7250 3575
Wire Wire Line
	7250 4250 7250 3825
Wire Wire Line
	6900 3700 6950 3700
Text GLabel 6900 3800 0    50   Input ~ 0
!ARM
Wire Wire Line
	6900 3800 6950 3800
Connection ~ 6950 3600
Connection ~ 7250 4250
Wire Wire Line
	7550 3700 7650 3700
Wire Wire Line
	5750 2350 5750 3300
Wire Wire Line
	6250 2050 6250 3600
$Sheet
S 7150 2650 550  200 
U 5EDB2B01
F0 "sheet5EDB2AFE" 50
F1 "R_G_LED_indicator.sch" 50
F2 "Cntrl" I L 7150 2750 50 
$EndSheet
Text GLabel 6900 3700 0    50   Input ~ 0
!T_arm_low
Wire Wire Line
	5250 2450 5250 2650
$Comp
L power:+5V #PWR?
U 1 1 5EC7BF8A
P 4350 2100
AR Path="/5EB41214/5EC7BF8A" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5EC7BF8A" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5EC7BF8A" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5EC7BF8A" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5EC7BF8A" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 4350 1950 50  0001 C CNN
F 1 "+5V" H 4250 2200 50  0000 C CNN
F 2 "" H 4350 2100 50  0001 C CNN
F 3 "" H 4350 2100 50  0001 C CNN
	1    4350 2100
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC1G32 U?
U 1 1 5ED3994F
P 5250 2350
AR Path="/5ED2BC2A/5ED3994F" Ref="U?"  Part="1" 
AR Path="/5EB41214/5ED3994F" Ref="U?"  Part="1" 
AR Path="/5EE77D5A/5ED3994F" Ref="U11"  Part="1" 
F 0 "U11" H 5544 2396 50  0000 L CNN
F 1 "SN74LVC1G32DCKR" H 5544 2305 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 5250 2350 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 5250 2350 50  0001 C CNN
F 4 "296-9848-1-ND" H 5250 2350 50  0001 C CNN "Digi-Key_PN"
	1    5250 2350
	1    0    0    -1  
$EndComp
Connection ~ 5150 4250
Wire Wire Line
	5150 4250 5600 4250
Wire Wire Line
	4700 4250 5150 4250
Connection ~ 5600 3250
Wire Wire Line
	5600 3250 5600 3200
Wire Wire Line
	5600 3300 5600 3250
Wire Wire Line
	5300 3250 5600 3250
Wire Wire Line
	4650 3250 5000 3250
Connection ~ 5150 3700
Wire Wire Line
	5150 3700 5150 3400
$Comp
L Device:R_POT RV1
U 1 1 5EE7C88B
P 5150 3250
F 0 "RV1" V 4943 3250 50  0000 C CNN
F 1 "R_POT 50K" V 5034 3250 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296W_Vertical" H 5150 3250 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/3296.pdf" H 5150 3250 50  0001 C CNN
F 4 "3296W-503LF-ND" H 5150 3250 50  0001 C CNN "Digi-Key_PN"
	1    5150 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	5150 4000 5150 4250
Wire Wire Line
	5150 3700 5400 3700
Wire Wire Line
	5150 3800 5150 3700
$Comp
L Device:C_Small C?
U 1 1 5EE3DEE5
P 5150 3900
AR Path="/5EB41214/5EE3DEE5" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5EE3DEE5" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5EE3DEE5" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5EE3DEE5" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5EE3DEE5" Ref="C18"  Part="1" 
F 0 "C18" H 5242 3946 50  0000 L CNN
F 1 "C 2.2u" H 5242 3855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5150 3900 50  0001 C CNN
F 3 "~" H 5150 3900 50  0001 C CNN
F 4 "16" H 5150 3900 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5150 3900 50  0001 C CNN "Tolerance"
F 6 "490-4787-1-ND" H 5150 3900 50  0001 C CNN "Digi-Key_PN"
F 7 "1458904" H 5150 3900 50  0001 C CNN "Farnell_PN"
	1    5150 3900
	1    0    0    -1  
$EndComp
Connection ~ 4700 3500
Wire Wire Line
	4450 3500 4700 3500
Wire Wire Line
	4700 3500 5400 3500
$Comp
L comparators_magne:MAX9141 U?
U 1 1 5EE8A633
P 5700 3600
AR Path="/5EB41214/5EE8A633" Ref="U?"  Part="1" 
AR Path="/5ED2BC2A/5EE8A633" Ref="U?"  Part="1" 
AR Path="/5EE0AC5D/5EE8A633" Ref="U?"  Part="1" 
AR Path="/5EE0E313/5EE8A633" Ref="U?"  Part="1" 
AR Path="/5EE77D5A/5EE8A633" Ref="U12"  Part="1" 
F 0 "U12" H 6044 3646 50  0000 L CNN
F 1 "MAX9141ESA+-ND" H 6044 3555 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5700 3600 50  0001 C CNN
F 3 "" H 5700 3600 50  0001 C CNN
F 4 "	MAX9141ESA+-ND" H 5700 3600 50  0001 C CNN "Digi-Key_PN"
	1    5700 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 3250 4650 3300
$Comp
L power:GND #PWR?
U 1 1 5EE8A631
P 4650 3300
AR Path="/5EB41214/5EE8A631" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5EE8A631" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5EE8A631" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5EE8A631" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5EE8A631" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 4650 3050 50  0001 C CNN
F 1 "GND" H 4800 3250 50  0000 C CNN
F 2 "" H 4650 3300 50  0001 C CNN
F 3 "" H 4650 3300 50  0001 C CNN
	1    4650 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3800 4700 3500
Wire Wire Line
	4700 4000 4700 4250
$Comp
L Device:C_Small C?
U 1 1 5ECF4EFB
P 4550 2400
AR Path="/5EB41214/5ECF4EFB" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ECF4EFB" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ECF4EFB" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5ECF4EFB" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5ECF4EFB" Ref="C16"  Part="1" 
F 0 "C16" H 4300 2450 50  0000 L CNN
F 1 "C 100n" H 4250 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4550 2400 50  0001 C CNN
F 3 "~" H 4550 2400 50  0001 C CNN
F 4 "16" H 4550 2400 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 4550 2400 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 4550 2400 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 4550 2400 50  0001 C CNN "Farnell_PN"
	1    4550 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5ECBA194
P 4350 2650
AR Path="/5EB41214/5ECBA194" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ECBA194" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5ECBA194" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5ECBA194" Ref="#PWR?"  Part="1" 
AR Path="/5EE77D5A/5ECBA194" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 4350 2400 50  0001 C CNN
F 1 "GND" H 4355 2477 50  0000 C CNN
F 2 "" H 4350 2650 50  0001 C CNN
F 3 "" H 4350 2650 50  0001 C CNN
	1    4350 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 2250 5250 2100
Wire Wire Line
	5250 2100 4550 2100
Connection ~ 4550 2100
Wire Wire Line
	4550 2100 4350 2100
Wire Wire Line
	4550 2650 5250 2650
Wire Wire Line
	4350 2650 4550 2650
Connection ~ 4550 2650
Wire Wire Line
	4550 2300 4550 2100
Wire Wire Line
	4550 2500 4550 2650
$Comp
L dk_Test-Points:5011 TP?
U 1 1 5EE791AA
P 4450 3400
AR Path="/5EEDC7D6/5EE791AA" Ref="TP?"  Part="1" 
AR Path="/5EED7553/5EE791AA" Ref="TP?"  Part="1" 
AR Path="/5EE77D5A/5EE791AA" Ref="TP1"  Part="1" 
F 0 "TP1" H 4400 3447 50  0000 R CNN
F 1 "5011" H 4450 3300 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4650 3600 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 4650 3700 60  0001 L CNN
F 4 "" H 4650 3800 60  0001 L CNN "Digi-Key_PN"
F 5 "5011" H 4650 3900 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 4650 4000 60  0001 L CNN "Category"
F 7 "Test Points" H 4650 4100 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 4650 4200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5011/36-5011-ND/255333" H 4650 4300 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MULTIPURPOSE BLACK" H 4650 4400 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 4650 4500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4650 4600 60  0001 L CNN "Status"
	1    4450 3400
	-1   0    0    1   
$EndComp
Wire Wire Line
	8000 3750 8000 3700
Wire Wire Line
	8000 3700 8300 3700
$Comp
L Device:R R?
U 1 1 5F89A522
P 7800 3700
AR Path="/5F89A522" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F89A522" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F89A522" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5F89A522" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5F89A522" Ref="R?"  Part="1" 
AR Path="/5EED7553/5F89A522" Ref="R?"  Part="1" 
AR Path="/5EEDC7D6/5F89A522" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5F89A522" Ref="R20"  Part="1" 
F 0 "R20" V 7593 3700 50  0000 C CNN
F 1 "R 10" V 7684 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7730 3700 50  0001 C CNN
F 3 "~" H 7800 3700 50  0001 C CNN
F 4 "" H 7800 3700 50  0001 C CNN "Tolerance"
F 5 "RMCF0603FT10R0CT-ND" H 7800 3700 50  0001 C CNN "Digi-Key_PN"
	1    7800 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	7950 3700 8000 3700
Connection ~ 8000 3700
Wire Wire Line
	8000 4250 8000 3950
$Comp
L Device:C_Small C?
U 1 1 5F926DA2
P 8000 3850
AR Path="/5EB41214/5F926DA2" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F926DA2" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F926DA2" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F926DA2" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5F926DA2" Ref="C?"  Part="1" 
AR Path="/5ECA36FF/5F926DA2" Ref="C?"  Part="1" 
AR Path="/5EED7553/5F926DA2" Ref="C?"  Part="1" 
AR Path="/5EEDC7D6/5F926DA2" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5F926DA2" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F926DA2" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5F926DA2" Ref="C20"  Part="1" 
F 0 "C20" H 8092 3896 50  0000 L CNN
F 1 "C 470p" H 8092 3805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8000 3850 50  0001 C CNN
F 3 "~" H 8000 3850 50  0001 C CNN
F 4 "16" H 8000 3850 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 8000 3850 50  0001 C CNN "Tolerance"
F 6 "399-10051-1-ND" H 8000 3850 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 8000 3850 50  0001 C CNN "Farnell_PN"
	1    8000 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP?
U 1 1 5F989520
P 4150 3500
AR Path="/5ECDAC29/5F989520" Ref="JP?"  Part="1" 
AR Path="/5EE77D5A/5F989520" Ref="JP1"  Part="1" 
F 0 "JP1" V 4196 3412 50  0000 R CNN
F 1 "Jumper" V 4105 3412 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" V 4059 3413 50  0001 R CNN
F 3 "~" H 4150 3500 50  0001 C CNN
F 4 "732-5315-ND" H 4150 3500 50  0001 C CNN "Digi-Key_PN"
	1    4150 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3500 3450 3500
Connection ~ 4450 3500
Wire Wire Line
	3750 3500 3850 3500
$Comp
L Device:C_Small C?
U 1 1 5FB4DCF3
P 4700 3900
AR Path="/5EB41214/5FB4DCF3" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FB4DCF3" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FB4DCF3" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FB4DCF3" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5FB4DCF3" Ref="C17"  Part="1" 
AR Path="/5EE8A13E/5FB4DCF3" Ref="C?"  Part="1" 
F 0 "C17" H 4792 3946 50  0000 L CNN
F 1 "C 1u" H 4792 3855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4700 3900 50  0001 C CNN
F 3 "~" H 4700 3900 50  0001 C CNN
F 4 "16" H 4700 3900 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 4700 3900 50  0001 C CNN "Tolerance"
F 6 "311-1365-1-ND" H 4700 3900 50  0001 C CNN "Digi-Key_PN"
F 7 "2547037" H 4700 3900 50  0001 C CNN "Farnell_PN"
	1    4700 3900
	1    0    0    -1  
$EndComp
Text Notes 3750 3950 0    50   ~ 0
10Hz low pass
$Comp
L Device:R R?
U 1 1 5FB4DD00
P 3600 3500
AR Path="/5FB4DD00" Ref="R?"  Part="1" 
AR Path="/5EB41214/5FB4DD00" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5FB4DD00" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5FB4DD00" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5FB4DD00" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5FB4DD00" Ref="R19"  Part="1" 
AR Path="/5EE8A13E/5FB4DD00" Ref="R?"  Part="1" 
F 0 "R19" V 3807 3500 50  0000 C CNN
F 1 "R 16K" V 3716 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3530 3500 50  0001 C CNN
F 3 "~" H 3600 3500 50  0001 C CNN
F 4 "1%" H 3600 3500 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 3600 3500 50  0001 C CNN "Digi-Key_PN"
	1    3600 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7250 4250 8000 4250
$EndSCHEMATC
