EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 20 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 6750 3400 2    50   Input ~ 0
!ARM
Text GLabel 6500 2500 0    50   Input ~ 0
!T_arm_low
$Comp
L dk_Slide-Switches:EG1218 S2
U 1 1 5F89B346
P 4550 3400
F 0 "S2" H 4500 3050 50  0000 C CNN
F 1 "EG1218" H 4450 3150 50  0000 C CNN
F 2 "digikey-footprints:Switch_Slide_11.6x4mm_EG1218" H 4750 3600 50  0001 L CNN
F 3 "http://spec_sheets.e-switch.com/specs/P040040.pdf" H 4750 3700 60  0001 L CNN
F 4 "EG1903-ND" H 4750 3800 60  0001 L CNN "Digi-Key_PN"
F 5 "EG1218" H 4750 3900 60  0001 L CNN "MPN"
F 6 "Switches" H 4750 4000 60  0001 L CNN "Category"
F 7 "Slide Switches" H 4750 4100 60  0001 L CNN "Family"
F 8 "http://spec_sheets.e-switch.com/specs/P040040.pdf" H 4750 4200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/e-switch/EG1218/EG1903-ND/101726" H 4750 4300 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH SLIDE SPDT 200MA 30V" H 4750 4400 60  0001 L CNN "Description"
F 11 "E-Switch" H 4750 4500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4750 4600 60  0001 L CNN "Status"
	1    4550 3400
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F89FB49
P 4350 4050
AR Path="/5F89FB49" Ref="#PWR?"  Part="1" 
AR Path="/5EE5AAE3/5F89FB49" Ref="#PWR?"  Part="1" 
AR Path="/5ECA36FF/5F89FB49" Ref="#PWR?"  Part="1" 
AR Path="/5ECDAC29/5F89FB49" Ref="#PWR066"  Part="1" 
F 0 "#PWR066" H 4350 3800 50  0001 C CNN
F 1 "GND" H 4355 3877 50  0000 C CNN
F 2 "" H 4350 4050 50  0001 C CNN
F 3 "" H 4350 4050 50  0001 C CNN
	1    4350 4050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F89FB51
P 4350 3050
AR Path="/5EB41214/5F89FB51" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5F89FB51" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5F89FB51" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5F89FB51" Ref="#PWR?"  Part="1" 
AR Path="/5EE5AAE3/5F89FB51" Ref="#PWR?"  Part="1" 
AR Path="/5ECA36FF/5F89FB51" Ref="#PWR?"  Part="1" 
AR Path="/5ECDAC29/5F89FB51" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 4350 2900 50  0001 C CNN
F 1 "+5V" H 4365 3223 50  0000 C CNN
F 2 "" H 4350 3050 50  0001 C CNN
F 3 "" H 4350 3050 50  0001 C CNN
	1    4350 3050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5750 3050 5750 3100
Wire Wire Line
	5350 3400 5450 3400
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:SN74LVC1G17DCKR U?
U 1 1 5F89FB63
P 5750 3300
AR Path="/5EE5AAE3/5F89FB63" Ref="U?"  Part="1" 
AR Path="/5ECDAC29/5F89FB63" Ref="U26"  Part="1" 
F 0 "U26" H 5900 3400 60  0000 L CNN
F 1 "SN74LVC1G17DCKR" H 5850 2600 60  0000 L CNN
F 2 "digikey-footprints:SC-70-5" H 5950 3500 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g17" H 5950 3600 60  0001 L CNN
F 4 "296-11934-1-ND" H 5950 3700 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G17DCKR" H 5950 3800 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 5950 3900 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 5950 4000 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g17" H 5950 4100 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G17DCKR/296-11934-1-ND/389052" H 5950 4200 60  0001 L CNN "DK_Detail_Page"
F 10 "IC BUF NON-INVERT 5.5V SC70-5" H 5950 4300 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 5950 4400 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5950 4500 60  0001 L CNN "Status"
	1    5750 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3800 5750 4050
$Comp
L Device:C_Small C?
U 1 1 5F89FB6E
P 5350 3600
AR Path="/5EB41214/5F89FB6E" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F89FB6E" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F89FB6E" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F89FB6E" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5F89FB6E" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F89FB6E" Ref="C?"  Part="1" 
AR Path="/5ED08686/5F89FB6E" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5F89FB6E" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5F89FB6E" Ref="C49"  Part="1" 
F 0 "C49" H 5442 3646 50  0000 L CNN
F 1 "C 1u" H 5442 3555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5350 3600 50  0001 C CNN
F 3 "~" H 5350 3600 50  0001 C CNN
F 4 "16" H 5350 3600 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5350 3600 50  0001 C CNN "Tolerance"
F 6 "311-1365-1-ND" H 5350 3600 50  0001 C CNN "Digi-Key_PN"
F 7 "2547037" H 5350 3600 50  0001 C CNN "Farnell_PN"
	1    5350 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3700 5350 4050
$Comp
L Device:R R?
U 1 1 5F89FB77
P 5050 3400
AR Path="/5F89FB77" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F89FB77" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F89FB77" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5F89FB77" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5F89FB77" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5F89FB77" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5F89FB77" Ref="R?"  Part="1" 
AR Path="/5ED08686/5F89FB77" Ref="R?"  Part="1" 
AR Path="/5EE5AAE3/5F89FB77" Ref="R?"  Part="1" 
AR Path="/5ECDAC29/5F89FB77" Ref="R53"  Part="1" 
F 0 "R53" V 5257 3400 50  0000 C CNN
F 1 "R 16K" V 5166 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4980 3400 50  0001 C CNN
F 3 "~" H 5050 3400 50  0001 C CNN
F 4 "1%" H 5050 3400 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 5050 3400 50  0001 C CNN "Digi-Key_PN"
	1    5050 3400
	0    -1   -1   0   
$EndComp
Text Notes 4700 3600 0    50   ~ 0
10Hz low pass
Wire Wire Line
	5200 3400 5350 3400
Wire Wire Line
	5350 3500 5350 3400
Wire Wire Line
	4900 3400 4750 3400
Connection ~ 5350 4050
Wire Wire Line
	5350 4050 5750 4050
Connection ~ 5350 3400
Wire Wire Line
	6050 3400 6250 3400
Wire Wire Line
	4350 3500 4350 4050
Wire Wire Line
	4350 4050 4850 4050
Wire Wire Line
	4350 3300 4350 3050
Wire Wire Line
	4350 3050 5750 3050
Connection ~ 4350 3050
Connection ~ 4350 4050
$Sheet
S 6750 2400 550  200 
U 5FA61171
F0 "sheet5FA6116E" 50
F1 "green_led_indicator.sch" 50
F2 "!EN" I L 6750 2500 50 
$EndSheet
Wire Wire Line
	6500 2500 6750 2500
$Sheet
S 6750 2900 550  200 
U 5FA6142E
F0 "sheet5FA6142B" 50
F1 "green_led_indicator.sch" 50
F2 "!EN" I L 6750 3000 50 
$EndSheet
Wire Wire Line
	6600 3450 6600 3400
Wire Wire Line
	6600 3400 6750 3400
$Comp
L Device:R R?
U 1 1 5FB6BB98
P 6400 3400
AR Path="/5FB6BB98" Ref="R?"  Part="1" 
AR Path="/5EB41214/5FB6BB98" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5FB6BB98" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5FB6BB98" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5FB6BB98" Ref="R?"  Part="1" 
AR Path="/5EED7553/5FB6BB98" Ref="R?"  Part="1" 
AR Path="/5EEDC7D6/5FB6BB98" Ref="R?"  Part="1" 
AR Path="/5ED08686/5FB6BB98" Ref="R?"  Part="1" 
AR Path="/5ECDAC29/5FB6BB98" Ref="R54"  Part="1" 
F 0 "R54" V 6193 3400 50  0000 C CNN
F 1 "R 10" V 6284 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6330 3400 50  0001 C CNN
F 3 "~" H 6400 3400 50  0001 C CNN
F 4 "" H 6400 3400 50  0001 C CNN "Tolerance"
F 5 "RMCF0603FT10R0CT-ND" H 6400 3400 50  0001 C CNN "Digi-Key_PN"
	1    6400 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	6550 3400 6600 3400
Connection ~ 6600 3400
$Comp
L Device:C_Small C?
U 1 1 5FB6BBA5
P 6600 3550
AR Path="/5EB41214/5FB6BBA5" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FB6BBA5" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FB6BBA5" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FB6BBA5" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5FB6BBA5" Ref="C?"  Part="1" 
AR Path="/5ECA36FF/5FB6BBA5" Ref="C?"  Part="1" 
AR Path="/5EED7553/5FB6BBA5" Ref="C?"  Part="1" 
AR Path="/5EEDC7D6/5FB6BBA5" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5FB6BBA5" Ref="C50"  Part="1" 
AR Path="/5EE8A13E/5FB6BBA5" Ref="C?"  Part="1" 
AR Path="/5ED08686/5FB6BBA5" Ref="C?"  Part="1" 
F 0 "C50" H 6692 3596 50  0000 L CNN
F 1 "C 470p" H 6692 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6600 3550 50  0001 C CNN
F 3 "~" H 6600 3550 50  0001 C CNN
F 4 "16" H 6600 3550 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 6600 3550 50  0001 C CNN "Tolerance"
F 6 "399-10051-1-ND" H 6600 3550 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 6600 3550 50  0001 C CNN "Farnell_PN"
	1    6600 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 4050 5750 4050
Wire Wire Line
	6600 3650 6600 4050
Connection ~ 5750 4050
$Comp
L Device:C_Small C?
U 1 1 5FB6F76D
P 4850 3950
AR Path="/5EB41214/5FB6F76D" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FB6F76D" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FB6F76D" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FB6F76D" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5FB6F76D" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FB6F76D" Ref="C?"  Part="1" 
AR Path="/5ED08686/5FB6F76D" Ref="C?"  Part="1" 
AR Path="/5F94411C/5FB6F76D" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5FB6F76D" Ref="C48"  Part="1" 
F 0 "C48" H 4942 3996 50  0000 L CNN
F 1 "C 100n" H 4942 3905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4850 3950 50  0001 C CNN
F 3 "~" H 4850 3950 50  0001 C CNN
F 4 "16" H 4850 3950 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 4850 3950 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 4850 3950 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 4850 3950 50  0001 C CNN "Farnell_PN"
	1    4850 3950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5FB6F928
P 4850 3850
AR Path="/5EB41214/5FB6F928" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5FB6F928" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5FB6F928" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5FB6F928" Ref="#PWR?"  Part="1" 
AR Path="/5EE5AAE3/5FB6F928" Ref="#PWR?"  Part="1" 
AR Path="/5ECA36FF/5FB6F928" Ref="#PWR?"  Part="1" 
AR Path="/5ECDAC29/5FB6F928" Ref="#PWR067"  Part="1" 
F 0 "#PWR067" H 4850 3700 50  0001 C CNN
F 1 "+5V" H 4865 4023 50  0000 C CNN
F 2 "" H 4850 3850 50  0001 C CNN
F 3 "" H 4850 3850 50  0001 C CNN
	1    4850 3850
	-1   0    0    -1  
$EndComp
Connection ~ 4850 4050
Wire Wire Line
	4850 4050 5350 4050
Wire Wire Line
	6600 3400 6600 3000
Wire Wire Line
	6600 3000 6750 3000
$EndSCHEMATC
