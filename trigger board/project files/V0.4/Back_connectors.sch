EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+5V #PWR043
U 1 1 5F1A5696
P 5150 3100
F 0 "#PWR043" H 5150 2950 50  0001 C CNN
F 1 "+5V" H 5165 3273 50  0000 C CNN
F 2 "" H 5150 3100 50  0001 C CNN
F 3 "" H 5150 3100 50  0001 C CNN
	1    5150 3100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5150 3200 5150 3100
$Comp
L dk_Ferrite-Beads-and-Chips:BLM21PG221SN1D FB1
U 1 1 5F1B2769
P 4700 3200
F 0 "FB1" H 4700 3487 60  0000 C CNN
F 1 "BLM21PG221SN1D" H 4700 3381 60  0000 C CNN
F 2 "digikey-footprints:0805" H 4900 3400 60  0001 L CNN
F 3 "https://www.murata.com/en-us/products/en-us/products/productdata/8796738977822/ENFA0005.pdf" H 4900 3500 60  0001 L CNN
F 4 "490-1054-1-ND" H 4900 3600 60  0001 L CNN "Digi-Key_PN"
F 5 "BLM21PG221SN1D" H 4900 3700 60  0001 L CNN "MPN"
F 6 "Filters" H 4900 3800 60  0001 L CNN "Category"
F 7 "Ferrite Beads and Chips" H 4900 3900 60  0001 L CNN "Family"
F 8 "https://www.murata.com/en-us/products/en-us/products/productdata/8796738977822/ENFA0005.pdf" H 4900 4000 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/murata-electronics-north-america/BLM21PG221SN1D/490-1054-1-ND/574418" H 4900 4100 60  0001 L CNN "DK_Detail_Page"
F 10 "FERRITE BEAD 220 OHM 0805 1LN" H 4900 4200 60  0001 L CNN "Description"
F 11 "Murata Electronics North America" H 4900 4300 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4900 4400 60  0001 L CNN "Status"
	1    4700 3200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4900 3200 5150 3200
Connection ~ 5150 3200
Text HLabel 2800 2250 1    50   Input ~ 0
!BSW_interlock
Text HLabel 3200 2250 1    50   Input ~ 0
!DA_interlock
Text HLabel 2600 2250 1    50   Input ~ 0
!V_interlock
Text HLabel 3400 2250 1    50   Input ~ 0
!RH_interlock
Text HLabel 3000 2250 1    50   Input ~ 0
!ALT_SW_interlock
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5ECAE1B5
P 5300 3200
F 0 "#FLG01" H 5300 3275 50  0001 C CNN
F 1 "PWR_FLAG" H 5300 3373 50  0000 C CNN
F 2 "" H 5300 3200 50  0001 C CNN
F 3 "~" H 5300 3200 50  0001 C CNN
	1    5300 3200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5300 3200 5150 3200
Wire Wire Line
	5950 2650 6700 2650
$Comp
L power:+5V #PWR044
U 1 1 5EE25D80
P 5950 2650
F 0 "#PWR044" H 5950 2500 50  0001 C CNN
F 1 "+5V" H 5965 2823 50  0000 C CNN
F 2 "" H 5950 2650 50  0001 C CNN
F 3 "" H 5950 2650 50  0001 C CNN
	1    5950 2650
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC1G32 U?
U 1 1 5EE283EE
P 6700 3050
AR Path="/5ED2BC2A/5EE283EE" Ref="U?"  Part="1" 
AR Path="/5ED08686/5EE283EE" Ref="U?"  Part="1" 
AR Path="/5F19D573/5EE283EE" Ref="U18"  Part="1" 
F 0 "U18" H 6850 3200 50  0000 L CNN
F 1 "SN74LVC1G32DCKR" H 6850 3400 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 6700 3050 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 6700 3050 50  0001 C CNN
F 4 "296-9848-1-ND" H 6700 3050 50  0001 C CNN "Digi-Key_PN"
	1    6700 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR045
U 1 1 5EE290A8
P 6700 3500
F 0 "#PWR045" H 6700 3250 50  0001 C CNN
F 1 "GND" H 6705 3327 50  0000 C CNN
F 2 "" H 6700 3500 50  0001 C CNN
F 3 "" H 6700 3500 50  0001 C CNN
	1    6700 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2650 6700 2950
Wire Wire Line
	6700 3150 6700 3500
Wire Wire Line
	6350 3000 6400 3000
$Comp
L Device:R R?
U 1 1 5EE2DD69
P 6350 3350
AR Path="/5EE2DD69" Ref="R?"  Part="1" 
AR Path="/5EB41214/5EE2DD69" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5EE2DD69" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5EE2DD69" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5EE2DD69" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5EE2DD69" Ref="R?"  Part="1" 
AR Path="/5ECDAC29/5EE2DD69" Ref="R?"  Part="1" 
AR Path="/5F19D573/5EE2DD69" Ref="R31"  Part="1" 
F 0 "R31" V 6557 3350 50  0000 C CNN
F 1 "R 16K" V 6466 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6280 3350 50  0001 C CNN
F 3 "~" H 6350 3350 50  0001 C CNN
F 4 "1%" H 6350 3350 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 6350 3350 50  0001 C CNN "Digi-Key_PN"
	1    6350 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3500 6700 3500
Connection ~ 6700 3500
Wire Wire Line
	6350 3200 6350 3000
$Comp
L dk_Tactile-Switches:1825910-6 S1
U 1 1 5EE32232
P 6150 2900
F 0 "S1" H 6150 3247 60  0000 C CNN
F 1 "1825910-6" H 6150 3141 60  0000 C CNN
F 2 "digikey-footprints:Switch_Tactile_THT_6x6mm" H 6350 3100 60  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1825910&DocType=Customer+Drawing&DocLang=English" H 6350 3200 60  0001 L CNN
F 4 "450-1650-ND" H 6350 3300 60  0001 L CNN "Digi-Key_PN"
F 5 "1825910-6" H 6350 3400 60  0001 L CNN "MPN"
F 6 "Switches" H 6350 3500 60  0001 L CNN "Category"
F 7 "Tactile Switches" H 6350 3600 60  0001 L CNN "Family"
F 8 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1825910&DocType=Customer+Drawing&DocLang=English" H 6350 3700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/te-connectivity-alcoswitch-switches/1825910-6/450-1650-ND/1632536" H 6350 3800 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH TACTILE SPST-NO 0.05A 24V" H 6350 3900 60  0001 L CNN "Description"
F 11 "TE Connectivity ALCOSWITCH Switches" H 6350 4000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6350 4100 60  0001 L CNN "Status"
	1    6150 2900
	1    0    0    -1  
$EndComp
Connection ~ 6350 3000
Wire Wire Line
	6350 2800 6350 3000
Connection ~ 6350 3500
$Comp
L dk_Test-Points:5011 TP?
U 1 1 5EE6C002
P 5400 3100
AR Path="/5EEDC7D6/5EE6C002" Ref="TP?"  Part="1" 
AR Path="/5F19D573/5EE6C002" Ref="TP2"  Part="1" 
F 0 "TP2" H 5350 3147 50  0000 R CNN
F 1 "5011" H 5400 3000 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5600 3300 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 5600 3400 60  0001 L CNN
F 4 "" H 5600 3500 60  0001 L CNN "Digi-Key_PN"
F 5 "5011" H 5600 3600 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 5600 3700 60  0001 L CNN "Category"
F 7 "Test Points" H 5600 3800 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 5600 3900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5011/36-5011-ND/255333" H 5600 4000 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MULTIPURPOSE BLACK" H 5600 4100 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 5600 4200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5600 4300 60  0001 L CNN "Status"
	1    5400 3100
	-1   0    0    1   
$EndComp
Wire Wire Line
	5300 3200 5400 3200
Connection ~ 5300 3200
Text HLabel 3600 2250 1    50   Input ~ 0
!T_lo_interlock
Text HLabel 3800 2250 1    50   Input ~ 0
!T_hi_interlock
$Comp
L Device:CP C30
U 1 1 5F1AC2D6
P 5150 3350
F 0 "C30" H 5268 3396 50  0000 L CNN
F 1 "CP 100u" H 5268 3305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D12.5mm_P5.00mm" H 5188 3200 50  0001 C CNN
F 3 "~" H 5150 3350 50  0001 C CNN
F 4 "16" H 5150 3350 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5150 3350 50  0001 C CNN "Tolerance"
F 6 "493-12135-1-ND" H 5150 3350 50  0001 C CNN "Digi-Key_PN"
	1    5150 3350
	-1   0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5EEDCF84
P 3250 4950
F 0 "H3" H 3350 4996 50  0000 L CNN
F 1 "MountingHole" H 3350 4905 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 3250 4950 50  0001 C CNN
F 3 "~" H 3250 4950 50  0001 C CNN
	1    3250 4950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5EEDE7D8
P 3250 5200
F 0 "H4" H 3350 5246 50  0000 L CNN
F 1 "MountingHole" H 3350 5155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 3250 5200 50  0001 C CNN
F 3 "~" H 3250 5200 50  0001 C CNN
	1    3250 5200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5EEDE95A
P 2500 4950
F 0 "H1" H 2600 4996 50  0000 L CNN
F 1 "MountingHole" H 2600 4905 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 2500 4950 50  0001 C CNN
F 3 "~" H 2500 4950 50  0001 C CNN
	1    2500 4950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5EEDEA28
P 2500 5200
F 0 "H2" H 2600 5246 50  0000 L CNN
F 1 "MountingHole" H 2600 5155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 2500 5200 50  0001 C CNN
F 3 "~" H 2500 5200 50  0001 C CNN
	1    2500 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 2650 5950 2800
Connection ~ 5950 2650
Connection ~ 5950 2800
Wire Wire Line
	5950 2800 5950 3000
$Comp
L Device:C_Small C?
U 1 1 5EF1CF2F
P 5950 3400
AR Path="/5EB41214/5EF1CF2F" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5EF1CF2F" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5EF1CF2F" Ref="C?"  Part="1" 
AR Path="/5F19D573/5EF1CF2F" Ref="C31"  Part="1" 
F 0 "C31" H 6042 3446 50  0000 L CNN
F 1 "C 100n" H 6042 3355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5950 3400 50  0001 C CNN
F 3 "~" H 5950 3400 50  0001 C CNN
F 4 "16" H 5950 3400 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5950 3400 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 5950 3400 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 5950 3400 50  0001 C CNN "Farnell_PN"
	1    5950 3400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5950 3300 5950 3000
Connection ~ 5950 3000
Text GLabel 3500 3200 1    50   Input ~ 0
!ARM
Text GLabel 2900 3200 1    50   Input ~ 0
!T_arm_low
Text GLabel 3100 3200 1    50   Input ~ 0
!T_arm_high
Text GLabel 2700 3200 1    50   Input ~ 0
!V_arm
Wire Wire Line
	6400 3100 5750 3100
$Comp
L power:GND #PWR042
U 1 1 5F5CB3B7
P 2300 4000
F 0 "#PWR042" H 2300 3750 50  0001 C CNN
F 1 "GND" H 2305 3827 50  0000 C CNN
F 2 "" H 2300 4000 50  0001 C CNN
F 3 "" H 2300 4000 50  0001 C CNN
	1    2300 4000
	1    0    0    -1  
$EndComp
Text GLabel 8950 3050 2    50   Input ~ 0
!LE
Wire Wire Line
	8650 3100 8650 3050
Wire Wire Line
	8650 3050 8950 3050
$Comp
L Device:R R?
U 1 1 5F8FE4CF
P 8450 3050
AR Path="/5F8FE4CF" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F8FE4CF" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F8FE4CF" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5F8FE4CF" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5F8FE4CF" Ref="R?"  Part="1" 
AR Path="/5EED7553/5F8FE4CF" Ref="R?"  Part="1" 
AR Path="/5EEDC7D6/5F8FE4CF" Ref="R?"  Part="1" 
AR Path="/5ECDAC29/5F8FE4CF" Ref="R?"  Part="1" 
AR Path="/5F8D1E66/5F8FE4CF" Ref="R?"  Part="1" 
AR Path="/5F19D573/5F8FE4CF" Ref="R33"  Part="1" 
F 0 "R33" V 8243 3050 50  0000 C CNN
F 1 "R 10" V 8334 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8380 3050 50  0001 C CNN
F 3 "~" H 8450 3050 50  0001 C CNN
F 4 "" H 8450 3050 50  0001 C CNN "Tolerance"
F 5 "RMCF0603FT10R0CT-ND" H 8450 3050 50  0001 C CNN "Digi-Key_PN"
	1    8450 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	8600 3050 8650 3050
Connection ~ 8650 3050
Wire Wire Line
	8650 3300 8650 3500
$Comp
L Device:C_Small C?
U 1 1 5F8FE4DC
P 8650 3200
AR Path="/5EB41214/5F8FE4DC" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F8FE4DC" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F8FE4DC" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F8FE4DC" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5F8FE4DC" Ref="C?"  Part="1" 
AR Path="/5ECA36FF/5F8FE4DC" Ref="C?"  Part="1" 
AR Path="/5EED7553/5F8FE4DC" Ref="C?"  Part="1" 
AR Path="/5EEDC7D6/5F8FE4DC" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5F8FE4DC" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F8FE4DC" Ref="C?"  Part="1" 
AR Path="/5F8D1E66/5F8FE4DC" Ref="C?"  Part="1" 
AR Path="/5F19D573/5F8FE4DC" Ref="C33"  Part="1" 
F 0 "C33" H 8742 3246 50  0000 L CNN
F 1 "C 470p" H 8742 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8650 3200 50  0001 C CNN
F 3 "~" H 8650 3200 50  0001 C CNN
F 4 "16" H 8650 3200 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 8650 3200 50  0001 C CNN "Tolerance"
F 6 "399-10051-1-ND" H 8650 3200 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 8650 3200 50  0001 C CNN "Farnell_PN"
	1    8650 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 3050 7600 3050
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:SN74LVC1G17DCKR U?
U 1 1 5F900F75
P 7900 2950
AR Path="/5EE5AAE3/5F900F75" Ref="U?"  Part="1" 
AR Path="/5ECDAC29/5F900F75" Ref="U?"  Part="1" 
AR Path="/5F19D573/5F900F75" Ref="U19"  Part="1" 
F 0 "U19" H 8244 2853 60  0000 L CNN
F 1 "SN74LVC1G17DCKR" H 8244 2747 60  0000 L CNN
F 2 "digikey-footprints:SC-70-5" H 8100 3150 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g17" H 8100 3250 60  0001 L CNN
F 4 "296-11934-1-ND" H 8100 3350 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G17DCKR" H 8100 3450 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 8100 3550 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 8100 3650 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g17" H 8100 3750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G17DCKR/296-11934-1-ND/389052" H 8100 3850 60  0001 L CNN "DK_Detail_Page"
F 10 "IC BUF NON-INVERT 5.5V SC70-5" H 8100 3950 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 8100 4050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8100 4150 60  0001 L CNN "Status"
	1    7900 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 3450 7900 3500
$Comp
L Device:C_Small C?
U 1 1 5F900F80
P 7500 3250
AR Path="/5EB41214/5F900F80" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F900F80" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F900F80" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F900F80" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5F900F80" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F900F80" Ref="C?"  Part="1" 
AR Path="/5ED08686/5F900F80" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5F900F80" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5F900F80" Ref="C?"  Part="1" 
AR Path="/5F19D573/5F900F80" Ref="C32"  Part="1" 
F 0 "C32" H 7592 3296 50  0000 L CNN
F 1 "C 1u" H 7592 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7500 3250 50  0001 C CNN
F 3 "~" H 7500 3250 50  0001 C CNN
F 4 "16" H 7500 3250 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7500 3250 50  0001 C CNN "Tolerance"
F 6 "311-1365-1-ND" H 7500 3250 50  0001 C CNN "Digi-Key_PN"
F 7 "2547037" H 7500 3250 50  0001 C CNN "Farnell_PN"
	1    7500 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 3350 7500 3500
$Comp
L Device:R R?
U 1 1 5F900F89
P 7200 3050
AR Path="/5F900F89" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F900F89" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F900F89" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5F900F89" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5F900F89" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5F900F89" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5F900F89" Ref="R?"  Part="1" 
AR Path="/5ED08686/5F900F89" Ref="R?"  Part="1" 
AR Path="/5EE5AAE3/5F900F89" Ref="R?"  Part="1" 
AR Path="/5ECDAC29/5F900F89" Ref="R?"  Part="1" 
AR Path="/5F19D573/5F900F89" Ref="R32"  Part="1" 
F 0 "R32" V 7407 3050 50  0000 C CNN
F 1 "R 16K" V 7316 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7130 3050 50  0001 C CNN
F 3 "~" H 7200 3050 50  0001 C CNN
F 4 "1%" H 7200 3050 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 7200 3050 50  0001 C CNN "Digi-Key_PN"
	1    7200 3050
	0    -1   -1   0   
$EndComp
Text Notes 7100 2650 0    50   ~ 0
10Hz low pass
Wire Wire Line
	7350 3050 7500 3050
Wire Wire Line
	7500 3150 7500 3050
Connection ~ 7500 3050
Wire Wire Line
	6950 3050 7050 3050
Wire Wire Line
	6700 3500 6950 3500
Connection ~ 7500 3500
Wire Wire Line
	7500 3500 7900 3500
Wire Wire Line
	8200 3050 8300 3050
Wire Wire Line
	7900 3500 8650 3500
Connection ~ 7900 3500
Wire Wire Line
	6700 2650 7900 2650
Wire Wire Line
	7900 2650 7900 2750
Connection ~ 6700 2650
$Sheet
S 3950 3100 550  200 
U 5FC01F23
F0 "Power regulator" 50
F1 "regulation.sch" 50
F2 "Vin" I L 3950 3200 50 
F3 "Vout" I R 4500 3200 50 
$EndSheet
Wire Wire Line
	3900 3200 3950 3200
Wire Wire Line
	5150 3500 5400 3500
Connection ~ 5950 3500
Wire Wire Line
	5750 3100 5750 2700
Wire Wire Line
	5750 2700 3700 2700
Text GLabel 3300 3200 1    50   Input ~ 0
T_SELECT
Wire Wire Line
	5950 3500 6350 3500
$Comp
L dk_Test-Points:5011 TP?
U 1 1 5FCD9E70
P 5400 3400
AR Path="/5EEDC7D6/5FCD9E70" Ref="TP?"  Part="1" 
AR Path="/5F19D573/5FCD9E70" Ref="TP3"  Part="1" 
F 0 "TP3" H 5350 3447 50  0000 R CNN
F 1 "5011" H 5400 3300 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5600 3600 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 5600 3700 60  0001 L CNN
F 4 "" H 5600 3800 60  0001 L CNN "Digi-Key_PN"
F 5 "5011" H 5600 3900 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 5600 4000 60  0001 L CNN "Category"
F 7 "Test Points" H 5600 4100 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 5600 4200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5011/36-5011-ND/255333" H 5600 4300 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MULTIPURPOSE BLACK" H 5600 4400 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 5600 4500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5600 4600 60  0001 L CNN "Status"
	1    5400 3400
	-1   0    0    1   
$EndComp
Connection ~ 5400 3500
Wire Wire Line
	5400 3500 5950 3500
$Comp
L dk_Test-Points:5011 TP?
U 1 1 5FCF1AF4
P 8650 2950
AR Path="/5EEDC7D6/5FCF1AF4" Ref="TP?"  Part="1" 
AR Path="/5EED7553/5FCF1AF4" Ref="TP?"  Part="1" 
AR Path="/5EE136D8/5FCF1AF4" Ref="TP?"  Part="1" 
AR Path="/5F19D573/5FCF1AF4" Ref="TP6"  Part="1" 
F 0 "TP6" H 8600 2997 50  0000 R CNN
F 1 "5011" H 8650 2850 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 8850 3150 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 8850 3250 60  0001 L CNN
F 4 "" H 8850 3350 60  0001 L CNN "Digi-Key_PN"
F 5 "5011" H 8850 3450 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 8850 3550 60  0001 L CNN "Category"
F 7 "Test Points" H 8850 3650 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 8850 3750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5011/36-5011-ND/255333" H 8850 3850 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MULTIPURPOSE BLACK" H 8850 3950 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 8850 4050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8850 4150 60  0001 L CNN "Status"
	1    8650 2950
	-1   0    0    1   
$EndComp
$Comp
L dk_Test-Points:5011 TP?
U 1 1 5FD1BAFC
P 7050 3400
AR Path="/5EEDC7D6/5FD1BAFC" Ref="TP?"  Part="1" 
AR Path="/5F19D573/5FD1BAFC" Ref="TP5"  Part="1" 
F 0 "TP5" H 7000 3447 50  0000 R CNN
F 1 "5011" H 7050 3300 50  0001 C CNN
F 2 "magne_footprints:Via_1.778mm" H 7250 3600 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 7250 3700 60  0001 L CNN
F 4 "" H 7250 3800 60  0001 L CNN "Digi-Key_PN"
F 5 "5011" H 7250 3900 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 7250 4000 60  0001 L CNN "Category"
F 7 "Test Points" H 7250 4100 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 7250 4200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5011/36-5011-ND/255333" H 7250 4300 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MULTIPURPOSE BLACK" H 7250 4400 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 7250 4500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7250 4600 60  0001 L CNN "Status"
	1    7050 3400
	-1   0    0    1   
$EndComp
Connection ~ 7050 3500
Wire Wire Line
	7050 3500 7500 3500
$Comp
L dk_Test-Points:5011 TP?
U 1 1 5FD1CA81
P 6950 3400
AR Path="/5EEDC7D6/5FD1CA81" Ref="TP?"  Part="1" 
AR Path="/5F19D573/5FD1CA81" Ref="TP4"  Part="1" 
F 0 "TP4" H 6900 3447 50  0000 R CNN
F 1 "5011" H 6950 3300 50  0001 C CNN
F 2 "magne_footprints:Via_1.778mm" H 7150 3600 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 7150 3700 60  0001 L CNN
F 4 "" H 7150 3800 60  0001 L CNN "Digi-Key_PN"
F 5 "5011" H 7150 3900 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 7150 4000 60  0001 L CNN "Category"
F 7 "Test Points" H 7150 4100 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 7150 4200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5011/36-5011-ND/255333" H 7150 4300 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MULTIPURPOSE BLACK" H 7150 4400 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 7150 4500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7150 4600 60  0001 L CNN "Status"
	1    6950 3400
	-1   0    0    1   
$EndComp
Connection ~ 6950 3500
Wire Wire Line
	6950 3500 7050 3500
Wire Wire Line
	2700 3200 2700 3700
Wire Wire Line
	3800 2250 3800 3700
Wire Wire Line
	3600 2250 3600 3700
Wire Wire Line
	3400 2250 3400 3700
Wire Wire Line
	3200 2250 3200 3700
Wire Wire Line
	3000 3700 3000 2250
Wire Wire Line
	2800 3700 2800 2250
Wire Wire Line
	3900 3200 3900 3700
Wire Wire Line
	3700 3700 3700 2700
Wire Wire Line
	3500 3200 3500 3700
Wire Wire Line
	3300 3200 3300 3700
Wire Wire Line
	3100 3200 3100 3700
Wire Wire Line
	2900 3200 2900 3700
Wire Wire Line
	2600 3700 2600 2250
Wire Wire Line
	2300 4000 2400 4000
$Comp
L Connector:DB15_Male_MountingHoles J1
U 1 1 5F1C3CA1
P 3300 4000
F 0 "J1" H 3454 4002 50  0000 L CNN
F 1 "L717SDA15PA4CH4F" H 3454 3911 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-15_Male_Horizontal_P2.77x2.84mm_EdgePinOffset7.70mm_Housed_MountingHolesOffset9.12mm" H 3300 4000 50  0001 C CNN
F 3 " ~" H 3300 4000 50  0001 C CNN
F 4 "	L717SDA15PA4CH4F-ND" H 3300 4000 50  0001 C CNN "Digi-Key_PN"
	1    3300 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 3650 4000 3700
$Comp
L power:+5V #PWR08
U 1 1 60134075
P 4000 3650
F 0 "#PWR08" H 4000 3500 50  0001 C CNN
F 1 "+5V" H 4015 3823 50  0000 C CNN
F 2 "" H 4000 3650 50  0001 C CNN
F 3 "" H 4000 3650 50  0001 C CNN
	1    4000 3650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
