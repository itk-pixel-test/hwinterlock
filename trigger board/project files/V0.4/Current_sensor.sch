EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 26 32
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 4050 2150 0    50   ~ 0
+-25uV offset voltage
Text Notes 4150 2300 0    50   ~ 0
0.5% gain error
Text Notes 2950 2650 0    50   ~ 0
0.5% tolerance
Text HLabel 6100 2850 2    50   Input ~ 0
Current_meas
$Comp
L power:GND1 #PWR?
U 1 1 5ECE9303
P 4350 3450
AR Path="/5EB41214/5ECE9303" Ref="#PWR?"  Part="1" 
AR Path="/5ECE87A4/5ECE9303" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 4350 3200 50  0001 C CNN
F 1 "GND1" H 4355 3277 50  0000 C CNN
F 2 "" H 4350 3450 50  0001 C CNN
F 3 "" H 4350 3450 50  0001 C CNN
	1    4350 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR0106
U 1 1 5ECE92FD
P 4350 2550
F 0 "#PWR0106" H 4350 2400 50  0001 C CNN
F 1 "+5VA" H 4365 2723 50  0000 C CNN
F 2 "" H 4350 2550 50  0001 C CNN
F 3 "" H 4350 2550 50  0001 C CNN
	1    4350 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR0108
U 1 1 5ECE92F7
P 5050 3200
F 0 "#PWR0108" H 5050 3050 50  0001 C CNN
F 1 "+5VA" H 5065 3373 50  0000 C CNN
F 2 "" H 5050 3200 50  0001 C CNN
F 3 "" H 5050 3200 50  0001 C CNN
	1    5050 3200
	1    0    0    -1  
$EndComp
Text Notes 5300 2550 0    50   ~ 0
100Hz low pass
Text Notes 4400 2750 0    50   ~ 0
0.4V / A
Text HLabel 2950 2750 0    50   Input ~ 0
I_in
Text HLabel 2950 3350 0    50   Input ~ 0
I_out
$Comp
L Device:R_Shunt R56
U 1 1 5ECE92ED
P 3100 3050
F 0 "R56" H 3013 3096 50  0000 R CNN
F 1 "LVK25R002DER" H 3013 3005 50  0000 R CNN
F 2 "magne_footprints:LVK25" V 3030 3050 50  0001 C CNN
F 3 "https://www.ohmite.com/assets/docs/res_lvk.pdf?r=false" H 3100 3050 50  0001 C CNN
	1    3100 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2950 3950 2950
Wire Wire Line
	3950 3150 3250 3150
Wire Wire Line
	2950 2750 3100 2750
Wire Wire Line
	3100 2750 3100 2850
Wire Wire Line
	2950 3350 3100 3350
Wire Wire Line
	3100 3350 3100 3250
Wire Wire Line
	5050 3200 5050 3250
Wire Wire Line
	5050 3450 5700 3450
Connection ~ 5050 3450
$Comp
L Device:C_Small C?
U 1 1 5ECE92DB
P 5050 3350
AR Path="/5EB41214/5ECE92DB" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ECE92DB" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ECE92DB" Ref="C?"  Part="1" 
AR Path="/5EBFB816/5ECE92DB" Ref="C?"  Part="1" 
AR Path="/5ECE87A4/5ECE92DB" Ref="C48"  Part="1" 
F 0 "C48" H 5142 3396 50  0000 L CNN
F 1 "C 47n" H 5142 3305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5050 3350 50  0001 C CNN
F 3 "~" H 5050 3350 50  0001 C CNN
F 4 "16" H 5050 3350 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5050 3350 50  0001 C CNN "Tolerance"
	1    5050 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2550 4350 2650
Wire Wire Line
	4850 3450 5050 3450
Connection ~ 4850 3450
Wire Wire Line
	4850 3050 4850 3450
Connection ~ 4350 3450
Wire Wire Line
	5700 3450 5700 3150
Wire Wire Line
	4350 3450 4850 3450
Wire Wire Line
	4350 3450 4350 3350
Wire Wire Line
	4850 2850 5300 2850
$Comp
L Amplifier_Current:NCS210 U33
U 1 1 5ECE92C9
P 4350 3050
F 0 "U33" H 4550 3600 50  0000 C CNN
F 1 "NCS210RSQT2G" H 4600 3500 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 4350 3050 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/NCS210R-D.PDF" H 4350 3050 50  0001 C CNN
	1    4350 3050
	1    0    0    -1  
$EndComp
Connection ~ 5700 2850
Wire Wire Line
	5700 2850 6100 2850
Wire Wire Line
	5700 2850 5600 2850
Wire Wire Line
	5700 2950 5700 2850
$Comp
L Device:C_Small C?
U 1 1 5ECE92BE
P 5700 3050
AR Path="/5EB41214/5ECE92BE" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ECE92BE" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ECE92BE" Ref="C?"  Part="1" 
AR Path="/5EBFB816/5ECE92BE" Ref="C?"  Part="1" 
AR Path="/5ECE87A4/5ECE92BE" Ref="C49"  Part="1" 
F 0 "C49" H 5792 3096 50  0000 L CNN
F 1 "C 1u" H 5792 3005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5700 3050 50  0001 C CNN
F 3 "~" H 5700 3050 50  0001 C CNN
F 4 "16" H 5700 3050 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5700 3050 50  0001 C CNN "Tolerance"
	1    5700 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5ECE92B6
P 5450 2850
AR Path="/5ECE92B6" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ECE92B6" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ECE92B6" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ECE92B6" Ref="R?"  Part="1" 
AR Path="/5EBFB816/5ECE92B6" Ref="R?"  Part="1" 
AR Path="/5ECE87A4/5ECE92B6" Ref="R57"  Part="1" 
F 0 "R57" V 5243 2850 50  0000 C CNN
F 1 "R 1.5K" V 5334 2850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5380 2850 50  0001 C CNN
F 3 "~" H 5450 2850 50  0001 C CNN
F 4 "1%" H 5450 2850 50  0001 C CNN "Tolerance"
	1    5450 2850
	0    1    1    0   
$EndComp
$EndSCHEMATC
