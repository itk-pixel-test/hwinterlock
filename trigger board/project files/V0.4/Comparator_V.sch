EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 27 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5EED812B
P 4700 2600
AR Path="/5EED812B" Ref="R?"  Part="1" 
AR Path="/5EB41214/5EED812B" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5EED812B" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5EED812B" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5EED812B" Ref="R?"  Part="1" 
AR Path="/5EED7553/5EED812B" Ref="R71"  Part="1" 
F 0 "R71" V 4493 2600 50  0000 C CNN
F 1 "R 1.82K" V 4584 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4630 2600 50  0001 C CNN
F 3 "~" H 4700 2600 50  0001 C CNN
F 4 "0.1%" H 4700 2600 50  0001 C CNN "Tolerance"
F 5 "P1.82KDACT-ND" H 4700 2600 50  0001 C CNN "Digi-Key_PN"
	1    4700 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5EED8132
P 5100 2600
AR Path="/5EED8132" Ref="R?"  Part="1" 
AR Path="/5EB41214/5EED8132" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5EED8132" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5EED8132" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5EED8132" Ref="R?"  Part="1" 
AR Path="/5EED7553/5EED8132" Ref="R72"  Part="1" 
F 0 "R72" V 4893 2600 50  0000 C CNN
F 1 "R 16K" V 4984 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5030 2600 50  0001 C CNN
F 3 "~" H 5100 2600 50  0001 C CNN
F 4 "1%" H 5100 2600 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 5100 2600 50  0001 C CNN "Digi-Key_PN"
	1    5100 2600
	0    1    1    0   
$EndComp
Text Notes 4700 2300 0    50   ~ 0
0.51V setpoint
Wire Wire Line
	5100 3000 4550 3000
Wire Wire Line
	4500 2600 4550 2600
Connection ~ 5450 2600
Wire Wire Line
	5250 2600 5300 2600
Wire Wire Line
	4900 2600 4950 2600
Wire Wire Line
	4900 2800 4900 2600
Wire Wire Line
	5100 2800 4900 2800
Connection ~ 4900 2600
Wire Wire Line
	4850 2600 4900 2600
$Comp
L power:GND1 #PWR?
U 1 1 5EED8144
P 5300 3450
AR Path="/5ED2BC2A/5EED8144" Ref="#PWR?"  Part="1" 
AR Path="/5EED7553/5EED8144" Ref="#PWR098"  Part="1" 
F 0 "#PWR098" H 5300 3200 50  0001 C CNN
F 1 "GND1" H 5305 3277 50  0000 C CNN
F 2 "" H 5300 3450 50  0001 C CNN
F 3 "" H 5300 3450 50  0001 C CNN
	1    5300 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR?
U 1 1 5EED814A
P 4500 2600
AR Path="/5ED2BC2A/5EED814A" Ref="#PWR?"  Part="1" 
AR Path="/5EED7553/5EED814A" Ref="#PWR096"  Part="1" 
F 0 "#PWR096" H 4500 2350 50  0001 C CNN
F 1 "GND1" H 4505 2427 50  0000 C CNN
F 2 "" H 4500 2600 50  0001 C CNN
F 3 "" H 4500 2600 50  0001 C CNN
	1    4500 2600
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5EED8150
P 5450 2250
AR Path="/5ED2BC2A/5EED8150" Ref="#PWR?"  Part="1" 
AR Path="/5EED7553/5EED8150" Ref="#PWR099"  Part="1" 
F 0 "#PWR099" H 5450 2100 50  0001 C CNN
F 1 "+5VA" H 5465 2423 50  0000 C CNN
F 2 "" H 5450 2250 50  0001 C CNN
F 3 "" H 5450 2250 50  0001 C CNN
	1    5450 2250
	1    0    0    -1  
$EndComp
Text GLabel 6450 3250 0    50   Input ~ 0
!ARM_LVsens
Text HLabel 4350 3000 0    50   Input ~ 0
Voltage
$Comp
L dk_Logic-Gates-and-Inverters:SN74LVC1G14DBVR U?
U 1 1 5EED81A7
P 6850 3250
AR Path="/5EE8A13E/5EED81A7" Ref="U?"  Part="1" 
AR Path="/5ED2BC2A/5EED81A7" Ref="U?"  Part="1" 
AR Path="/5EED7553/5EED81A7" Ref="U35"  Part="1" 
F 0 "U35" H 7094 3303 60  0000 L CNN
F 1 "SN74LVC1G14DBVR" H 6450 2950 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 7050 3450 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 7050 3550 60  0001 L CNN
F 4 "296-11607-1-ND" H 7050 3650 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G14DBVR" H 7050 3750 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 7050 3850 60  0001 L CNN "Category"
F 7 "Logic - Gates and Inverters" H 7050 3950 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 7050 4050 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G14DBVR/296-11607-1-ND/385746" H 7050 4150 60  0001 L CNN "DK_Detail_Page"
F 10 "IC INVERTER SCHMITT 1CH SOT23-5" H 7050 4250 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 7050 4350 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7050 4450 60  0001 L CNN "Status"
	1    6850 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3250 6550 3250
Wire Wire Line
	7050 3250 7050 2800
Wire Wire Line
	7050 2800 7250 2800
Wire Wire Line
	6850 3000 6850 3050
Wire Wire Line
	6400 2300 6350 2300
Wire Wire Line
	6350 2300 6350 2700
Wire Wire Line
	5300 3450 5550 3450
$Comp
L Device:C_Small C?
U 1 1 5EEDA78E
P 6800 5450
AR Path="/5EB41214/5EEDA78E" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5EEDA78E" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5EEDA78E" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5EEDA78E" Ref="C?"  Part="1" 
AR Path="/5EED7553/5EEDA78E" Ref="C65"  Part="1" 
F 0 "C65" H 6892 5496 50  0000 L CNN
F 1 "C 100n" H 6892 5405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6800 5450 50  0001 C CNN
F 3 "~" H 6800 5450 50  0001 C CNN
F 4 "16" H 6800 5450 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 6800 5450 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 6800 5450 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 6800 5450 50  0001 C CNN "Farnell_PN"
	1    6800 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 5300 6800 5350
Wire Wire Line
	6650 5300 6650 5200
Wire Wire Line
	6800 5300 6650 5300
Connection ~ 6800 5300
Wire Wire Line
	6950 4900 7100 4900
Text Notes 5750 4800 0    50   ~ 0
1.95V setpoint
Wire Wire Line
	5550 4550 5650 4550
Wire Wire Line
	5550 4800 5550 4550
Connection ~ 5550 4550
Wire Wire Line
	5450 4550 5550 4550
$Comp
L Device:R R?
U 1 1 5EEDA7E4
P 5800 4550
AR Path="/5EEDA7E4" Ref="R?"  Part="1" 
AR Path="/5EB41214/5EEDA7E4" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5EEDA7E4" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5EEDA7E4" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5EEDA7E4" Ref="R?"  Part="1" 
AR Path="/5EED7553/5EEDA7E4" Ref="R74"  Part="1" 
F 0 "R74" V 6007 4550 50  0000 C CNN
F 1 "R 1.82K" V 5916 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5730 4550 50  0001 C CNN
F 3 "~" H 5800 4550 50  0001 C CNN
F 4 "0.1%" H 5800 4550 50  0001 C CNN "Tolerance"
F 5 "P1.82KDACT-ND" H 5800 4550 50  0001 C CNN "Digi-Key_PN"
	1    5800 4550
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5EEDA7EB
P 5300 4550
AR Path="/5EEDA7EB" Ref="R?"  Part="1" 
AR Path="/5EB41214/5EEDA7EB" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5EEDA7EB" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5EEDA7EB" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5EEDA7EB" Ref="R?"  Part="1" 
AR Path="/5EED7553/5EEDA7EB" Ref="R73"  Part="1" 
F 0 "R73" V 5093 4550 50  0000 C CNN
F 1 "R 1.65K" V 5184 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5230 4550 50  0001 C CNN
F 3 "~" H 5300 4550 50  0001 C CNN
F 4 "0.1%" H 5300 4550 50  0001 C CNN "Tolerance"
F 5 "P1.65KDACT-ND" H 5300 4550 50  0001 C CNN "Digi-Key_PN"
	1    5300 4550
	0    1    1    0   
$EndComp
$Comp
L comparators_magne:MAX9141 U?
U 1 1 5EEDA803
P 6650 4900
AR Path="/5EB41214/5EEDA803" Ref="U?"  Part="1" 
AR Path="/5ED2BC2A/5EEDA803" Ref="U?"  Part="1" 
AR Path="/5ECA2575/5EEDA803" Ref="U?"  Part="1" 
AR Path="/5ECAC95D/5EEDA803" Ref="U?"  Part="1" 
AR Path="/5EED7553/5EEDA803" Ref="U34"  Part="1" 
F 0 "U34" H 6800 5150 50  0000 L CNN
F 1 "MAX9141ESA+-ND" H 6800 5050 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 6650 4900 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX9140-MAX9144.pdf" H 6650 4900 50  0001 C CNN
F 4 "	MAX9141ESA+-ND" H 6650 4900 50  0001 C CNN "Digi-Key_PN"
	1    6650 4900
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5EEDA815
P 6800 5300
AR Path="/5ED2BC2A/5EEDA815" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5EEDA815" Ref="#PWR?"  Part="1" 
AR Path="/5EED7553/5EEDA815" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 6800 5150 50  0001 C CNN
F 1 "+5VA" H 6815 5473 50  0000 C CNN
F 2 "" H 6800 5300 50  0001 C CNN
F 3 "" H 6800 5300 50  0001 C CNN
	1    6800 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR?
U 1 1 5EEDA821
P 5000 4550
AR Path="/5ED2BC2A/5EEDA821" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5EEDA821" Ref="#PWR?"  Part="1" 
AR Path="/5EED7553/5EEDA821" Ref="#PWR097"  Part="1" 
F 0 "#PWR097" H 5000 4300 50  0001 C CNN
F 1 "GND1" H 5005 4377 50  0000 C CNN
F 2 "" H 5000 4550 50  0001 C CNN
F 3 "" H 5000 4550 50  0001 C CNN
	1    5000 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4550 5150 4550
Connection ~ 4550 3000
Wire Wire Line
	4550 3000 4350 3000
$Comp
L comparators_magne:MAX9141 U?
U 1 1 5EF4AB41
P 5400 2900
AR Path="/5EB41214/5EF4AB41" Ref="U?"  Part="1" 
AR Path="/5ED2BC2A/5EF4AB41" Ref="U?"  Part="1" 
AR Path="/5ECA2575/5EF4AB41" Ref="U?"  Part="1" 
AR Path="/5ECAC95D/5EF4AB41" Ref="U?"  Part="1" 
AR Path="/5EED7553/5EF4AB41" Ref="U32"  Part="1" 
F 0 "U32" H 5550 3150 50  0000 L CNN
F 1 "MAX9141ESA+-ND" H 5550 3050 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5400 2900 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX9140-MAX9144.pdf" H 5400 2900 50  0001 C CNN
F 4 "	MAX9141ESA+-ND" H 5400 2900 50  0001 C CNN "Digi-Key_PN"
	1    5400 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3200 5300 3450
Connection ~ 5300 3450
$Comp
L Device:C_Small C?
U 1 1 5EF5380C
P 5550 3350
AR Path="/5EB41214/5EF5380C" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5EF5380C" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5EF5380C" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5EF5380C" Ref="C?"  Part="1" 
AR Path="/5EED7553/5EF5380C" Ref="C63"  Part="1" 
F 0 "C63" H 5642 3396 50  0000 L CNN
F 1 "C 100n" H 5642 3305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5550 3350 50  0001 C CNN
F 3 "~" H 5550 3350 50  0001 C CNN
F 4 "16" H 5550 3350 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5550 3350 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 5550 3350 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 5550 3350 50  0001 C CNN "Farnell_PN"
	1    5550 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3200 5550 3250
Wire Wire Line
	5550 3200 5400 3200
Wire Wire Line
	5650 3200 5550 3200
Connection ~ 5550 3200
$Comp
L power:+5VA #PWR?
U 1 1 5EF53816
P 5650 3200
AR Path="/5ED2BC2A/5EF53816" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5EF53816" Ref="#PWR?"  Part="1" 
AR Path="/5EED7553/5EF53816" Ref="#PWR0100"  Part="1" 
F 0 "#PWR0100" H 5650 3050 50  0001 C CNN
F 1 "+5VA" H 5665 3373 50  0000 C CNN
F 2 "" H 5650 3200 50  0001 C CNN
F 3 "" H 5650 3200 50  0001 C CNN
	1    5650 3200
	1    0    0    -1  
$EndComp
Connection ~ 5300 2600
Wire Wire Line
	5300 2600 5450 2600
Wire Wire Line
	5550 4800 6350 4800
Text GLabel 6000 4550 2    50   Input ~ 0
4.096V_ref
Wire Wire Line
	5950 4550 6000 4550
$Comp
L power:+5VA #PWR?
U 1 1 5EFB3A73
P 6850 3000
AR Path="/5ED2BC2A/5EFB3A73" Ref="#PWR?"  Part="1" 
AR Path="/5EED7553/5EFB3A73" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0105" H 6850 2850 50  0001 C CNN
F 1 "+5VA" H 6865 3173 50  0000 C CNN
F 2 "" H 6850 3000 50  0001 C CNN
F 3 "" H 6850 3000 50  0001 C CNN
	1    6850 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EFC3BFD
P 7700 3350
AR Path="/5EB41214/5EFC3BFD" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5EFC3BFD" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5EFC3BFD" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5EFC3BFD" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5EFC3BFD" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5EFC3BFD" Ref="C?"  Part="1" 
AR Path="/5ED08686/5EFC3BFD" Ref="C?"  Part="1" 
AR Path="/5EED7553/5EFC3BFD" Ref="C66"  Part="1" 
F 0 "C66" H 7792 3396 50  0000 L CNN
F 1 "C 100n" H 7792 3305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7700 3350 50  0001 C CNN
F 3 "~" H 7700 3350 50  0001 C CNN
F 4 "16" H 7700 3350 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7700 3350 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 7700 3350 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 7700 3350 50  0001 C CNN "Farnell_PN"
	1    7700 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 2850 7550 3450
$Comp
L logic_gates_magne:74LVC1G32 U?
U 1 1 5EFC3C0C
P 7550 2750
AR Path="/5ED2BC2A/5EFC3C0C" Ref="U?"  Part="1" 
AR Path="/5ED08686/5EFC3C0C" Ref="U?"  Part="1" 
AR Path="/5EED7553/5EFC3C0C" Ref="U36"  Part="1" 
F 0 "U36" H 7750 3000 50  0000 L CNN
F 1 "SN74LVC1G32DCKR" H 7750 3350 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 7550 2750 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/sn74lvc1g32.pdf?ts=1599127308969&ref_url=https%253A%252F%252Fwww.ti.com%252Fsitesearch%252Fdocs%252Funiversalsearch.tsp%253FsearchTerm%253DSN74LVC1G32DCKR" H 7550 2750 50  0001 C CNN
F 4 "296-9848-1-ND" H 7550 2750 50  0001 C CNN "Digi-Key_PN"
	1    7550 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3450 7550 3450
Connection ~ 7550 3450
$Comp
L power:+5VA #PWR?
U 1 1 5EFCA7AB
P 7550 2650
AR Path="/5ED2BC2A/5EFCA7AB" Ref="#PWR?"  Part="1" 
AR Path="/5EED7553/5EFCA7AB" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 7550 2500 50  0001 C CNN
F 1 "+5VA" H 7565 2823 50  0000 C CNN
F 2 "" H 7550 2650 50  0001 C CNN
F 3 "" H 7550 2650 50  0001 C CNN
	1    7550 2650
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5EFCAA4D
P 7700 3250
AR Path="/5ED2BC2A/5EFCAA4D" Ref="#PWR?"  Part="1" 
AR Path="/5EED7553/5EFCAA4D" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 7700 3100 50  0001 C CNN
F 1 "+5VA" H 7715 3423 50  0000 C CNN
F 2 "" H 7700 3250 50  0001 C CNN
F 3 "" H 7700 3250 50  0001 C CNN
	1    7700 3250
	1    0    0    -1  
$EndComp
$Comp
L dk_Test-Points:5011 TP?
U 1 1 5EE6F19F
P 4550 2900
AR Path="/5EEDC7D6/5EE6F19F" Ref="TP?"  Part="1" 
AR Path="/5EED7553/5EE6F19F" Ref="TP8"  Part="1" 
F 0 "TP8" H 4500 2947 50  0000 R CNN
F 1 "5011" H 4550 2800 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4750 3100 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 4750 3200 60  0001 L CNN
F 4 "" H 4750 3300 60  0001 L CNN "Digi-Key_PN"
F 5 "5011" H 4750 3400 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 4750 3500 60  0001 L CNN "Category"
F 7 "Test Points" H 4750 3600 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 4750 3700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5011/36-5011-ND/255333" H 4750 3800 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MULTIPURPOSE BLACK" H 4750 3900 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 4750 4000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4750 4100 60  0001 L CNN "Status"
	1    4550 2900
	-1   0    0    1   
$EndComp
Wire Wire Line
	4550 3000 4550 5000
Text HLabel 6400 2300 2    50   Input ~ 0
VSens_ARM
Wire Wire Line
	5450 2250 5450 2600
Wire Wire Line
	5700 2900 6350 2900
Wire Wire Line
	6350 2900 6350 2700
Connection ~ 6350 2700
Wire Wire Line
	6350 2700 7250 2700
Wire Wire Line
	5550 3450 6750 3450
Connection ~ 5550 3450
Connection ~ 6750 3450
Wire Wire Line
	7550 3450 7700 3450
Wire Wire Line
	4550 5000 6350 5000
$Comp
L power:+5VA #PWR?
U 1 1 5EEDA80F
P 6550 4550
AR Path="/5ED2BC2A/5EEDA80F" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5EEDA80F" Ref="#PWR?"  Part="1" 
AR Path="/5EED7553/5EEDA80F" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 6550 4400 50  0001 C CNN
F 1 "+5VA" H 6565 4723 50  0000 C CNN
F 2 "" H 6550 4550 50  0001 C CNN
F 3 "" H 6550 4550 50  0001 C CNN
	1    6550 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 4550 6550 4600
Text HLabel 7550 4900 2    50   Input ~ 0
Vsens_interlock
Text HLabel 7850 2750 2    50   Input ~ 0
Vsens_ARM_alarm
Wire Wire Line
	7800 2750 7850 2750
Wire Wire Line
	6200 4000 6200 4350
Wire Wire Line
	6450 3900 6700 3900
$Comp
L Device:C_Small C?
U 1 1 5FB9B5CA
P 6350 4250
AR Path="/5EB41214/5FB9B5CA" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FB9B5CA" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FB9B5CA" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FB9B5CA" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5FB9B5CA" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FB9B5CA" Ref="C?"  Part="1" 
AR Path="/5ED08686/5FB9B5CA" Ref="C?"  Part="1" 
AR Path="/5EED7553/5FB9B5CA" Ref="C64"  Part="1" 
F 0 "C64" H 6442 4296 50  0000 L CNN
F 1 "C 100n" H 6442 4205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6350 4250 50  0001 C CNN
F 3 "~" H 6350 4250 50  0001 C CNN
F 4 "16" H 6350 4250 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 6350 4250 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 6350 4250 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 6350 4250 50  0001 C CNN "Farnell_PN"
	1    6350 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 3600 5900 3850
$Comp
L power:GND1 #PWR?
U 1 1 5EEDA81B
P 6550 5550
AR Path="/5ED2BC2A/5EEDA81B" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5EEDA81B" Ref="#PWR?"  Part="1" 
AR Path="/5EED7553/5EEDA81B" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 6550 5300 50  0001 C CNN
F 1 "GND1" H 6555 5377 50  0000 C CNN
F 2 "" H 6550 5550 50  0001 C CNN
F 3 "" H 6550 5550 50  0001 C CNN
	1    6550 5550
	1    0    0    -1  
$EndComp
Connection ~ 6550 5550
Wire Wire Line
	6550 5550 6800 5550
Wire Wire Line
	6550 5200 6550 5550
Wire Wire Line
	6700 4600 6700 3900
Wire Wire Line
	5900 3600 7100 3600
Wire Wire Line
	7100 3600 7100 4900
Connection ~ 7100 4900
Wire Wire Line
	7100 4900 7550 4900
$Comp
L logic_gates_magne:74LVC1G32 U?
U 1 1 5FB9B5DC
P 6200 3900
AR Path="/5ED2BC2A/5FB9B5DC" Ref="U?"  Part="1" 
AR Path="/5ED08686/5FB9B5DC" Ref="U?"  Part="1" 
AR Path="/5EED7553/5FB9B5DC" Ref="U33"  Part="1" 
F 0 "U33" H 6494 3946 50  0000 L CNN
F 1 "SN74LVC1G32DCKR" H 6494 3855 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 6200 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 6200 3900 50  0001 C CNN
F 4 "296-9848-1-ND" H 6200 3900 50  0001 C CNN "Digi-Key_PN"
	1    6200 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4550 5000 4350
Wire Wire Line
	5000 4350 6200 4350
Connection ~ 5000 4550
Connection ~ 6200 4350
Wire Wire Line
	6200 4350 6350 4350
Wire Wire Line
	6200 3800 6350 3800
Wire Wire Line
	6350 3800 6350 4150
$Comp
L power:+5VA #PWR?
U 1 1 5FBA6D63
P 6350 3800
AR Path="/5ED2BC2A/5FBA6D63" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5FBA6D63" Ref="#PWR?"  Part="1" 
AR Path="/5EED7553/5FBA6D63" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 6350 3650 50  0001 C CNN
F 1 "+5VA" H 6365 3973 50  0000 C CNN
F 2 "" H 6350 3800 50  0001 C CNN
F 3 "" H 6350 3800 50  0001 C CNN
	1    6350 3800
	1    0    0    -1  
$EndComp
Connection ~ 6350 3800
Text GLabel 5800 3950 0    50   Input ~ 0
!LE_LVsens
Wire Wire Line
	5800 3950 5900 3950
$EndSCHEMATC
