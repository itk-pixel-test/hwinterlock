EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 5EE61F51
P 5250 4400
AR Path="/5EE61F51" Ref="#PWR?"  Part="1" 
AR Path="/5EE5AAE3/5EE61F51" Ref="#PWR017"  Part="1" 
AR Path="/5ECA36FF/5EE61F51" Ref="#PWR?"  Part="1" 
F 0 "#PWR017" H 5250 4150 50  0001 C CNN
F 1 "GND" H 5255 4227 50  0000 C CNN
F 2 "" H 5250 4400 50  0001 C CNN
F 3 "" H 5250 4400 50  0001 C CNN
	1    5250 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EE61F7F
P 7300 4300
AR Path="/5EB41214/5EE61F7F" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5EE61F7F" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5EE61F7F" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5EE61F7F" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5EE61F7F" Ref="C13"  Part="1" 
AR Path="/5ECA36FF/5EE61F7F" Ref="C?"  Part="1" 
F 0 "C13" H 7392 4346 50  0000 L CNN
F 1 "C 100n" H 7392 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7300 4300 50  0001 C CNN
F 3 "~" H 7300 4300 50  0001 C CNN
F 4 "16" H 7300 4300 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7300 4300 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 7300 4300 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 7300 4300 50  0001 C CNN "Farnell_PN"
	1    7300 4300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EE61F85
P 7300 4150
AR Path="/5EB41214/5EE61F85" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5EE61F85" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5EE61F85" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5EE61F85" Ref="#PWR?"  Part="1" 
AR Path="/5EE5AAE3/5EE61F85" Ref="#PWR020"  Part="1" 
AR Path="/5ECA36FF/5EE61F85" Ref="#PWR?"  Part="1" 
F 0 "#PWR020" H 7300 4000 50  0001 C CNN
F 1 "+5V" H 7315 4323 50  0000 C CNN
F 2 "" H 7300 4150 50  0001 C CNN
F 3 "" H 7300 4150 50  0001 C CNN
	1    7300 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 4150 7300 4200
$Comp
L power:+5V #PWR?
U 1 1 5EE61F96
P 7150 3650
AR Path="/5EB41214/5EE61F96" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5EE61F96" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5EE61F96" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5EE61F96" Ref="#PWR?"  Part="1" 
AR Path="/5EE5AAE3/5EE61F96" Ref="#PWR019"  Part="1" 
AR Path="/5ECA36FF/5EE61F96" Ref="#PWR?"  Part="1" 
F 0 "#PWR019" H 7150 3500 50  0001 C CNN
F 1 "+5V" H 7165 3823 50  0000 C CNN
F 2 "" H 7150 3650 50  0001 C CNN
F 3 "" H 7150 3650 50  0001 C CNN
	1    7150 3650
	-1   0    0    -1  
$EndComp
Text HLabel 8200 3850 2    50   Input ~ 0
Output
Wire Wire Line
	7050 3100 6850 3100
Wire Wire Line
	5950 3750 6850 3750
Connection ~ 5650 4400
$Comp
L power:+5V #PWR?
U 1 1 5ED7E0D4
P 5650 3400
AR Path="/5EB41214/5ED7E0D4" Ref="#PWR?"  Part="1" 
AR Path="/5ED2BC2A/5ED7E0D4" Ref="#PWR?"  Part="1" 
AR Path="/5EE0AC5D/5ED7E0D4" Ref="#PWR?"  Part="1" 
AR Path="/5EE0E313/5ED7E0D4" Ref="#PWR?"  Part="1" 
AR Path="/5EE5AAE3/5ED7E0D4" Ref="#PWR018"  Part="1" 
AR Path="/5ECA36FF/5ED7E0D4" Ref="#PWR?"  Part="1" 
F 0 "#PWR018" H 5650 3250 50  0001 C CNN
F 1 "+5V" H 5665 3573 50  0000 C CNN
F 2 "" H 5650 3400 50  0001 C CNN
F 3 "" H 5650 3400 50  0001 C CNN
	1    5650 3400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5650 3400 5650 3450
Text HLabel 4650 3750 0    50   Input ~ 0
Input
Wire Wire Line
	6850 3100 6850 3750
$Comp
L 74xGxx:74LVC1G332 U9
U 1 1 5F17799C
P 7150 3850
AR Path="/5EE5AAE3/5F17799C" Ref="U9"  Part="1" 
AR Path="/5ECA36FF/5F17799C" Ref="U?"  Part="1" 
F 0 "U9" H 7300 4200 50  0000 C CNN
F 1 "SN74LVC1G332DCKRG4" H 7500 4100 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6_Handsoldering" H 7150 3850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74lvc1g332.pdf?HQS=TI-null-null-digikeymode-df-pf-null-wwe&ts=1590142069492" H 7150 3850 50  0001 C CNN
F 4 "296-51594-1-ND" H 7150 3850 50  0001 C CNN "Digi-Key_PN"
	1    7150 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 4400 7150 4400
Connection ~ 6850 3750
Wire Wire Line
	7150 3650 7150 3725
Wire Wire Line
	7450 3850 7550 3850
Wire Wire Line
	7150 4400 7150 3975
Connection ~ 7150 4400
Wire Wire Line
	7150 4400 7300 4400
Wire Wire Line
	6800 3850 6850 3850
Text GLabel 6800 3950 0    50   Input ~ 0
!ARM
Wire Wire Line
	6800 3950 6850 3950
$Sheet
S 7050 3000 550  200 
U 5EDB1EB2
F0 "sheet5EDB1EAF" 50
F1 "R_G_LED_indicator.sch" 50
F2 "Cntrl" I L 7050 3100 50 
$EndSheet
Text GLabel 6800 3850 0    50   Input ~ 0
!T_arm_low
Wire Wire Line
	5250 3750 5350 3750
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:SN74LVC1G17DCKR U8
U 1 1 5F76706E
P 5650 3650
F 0 "U8" H 5994 3553 60  0000 L CNN
F 1 "SN74LVC1G17DCKR" H 5994 3447 60  0000 L CNN
F 2 "digikey-footprints:SC-70-5" H 5850 3850 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g17" H 5850 3950 60  0001 L CNN
F 4 "296-11934-1-ND" H 5850 4050 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G17DCKR" H 5850 4150 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 5850 4250 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 5850 4350 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g17" H 5850 4450 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G17DCKR/296-11934-1-ND/389052" H 5850 4550 60  0001 L CNN "DK_Detail_Page"
F 10 "IC BUF NON-INVERT 5.5V SC70-5" H 5850 4650 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 5850 4750 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5850 4850 60  0001 L CNN "Status"
	1    5650 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 4150 5650 4400
Wire Wire Line
	7900 3900 7900 3850
Wire Wire Line
	7900 3850 8200 3850
$Comp
L Device:R R?
U 1 1 5F8982CA
P 7700 3850
AR Path="/5F8982CA" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F8982CA" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F8982CA" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5F8982CA" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5F8982CA" Ref="R?"  Part="1" 
AR Path="/5EED7553/5F8982CA" Ref="R?"  Part="1" 
AR Path="/5EEDC7D6/5F8982CA" Ref="R?"  Part="1" 
AR Path="/5EE5AAE3/5F8982CA" Ref="R16"  Part="1" 
F 0 "R16" V 7493 3850 50  0000 C CNN
F 1 "R 10" V 7584 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7630 3850 50  0001 C CNN
F 3 "~" H 7700 3850 50  0001 C CNN
F 4 "" H 7700 3850 50  0001 C CNN "Tolerance"
F 5 "RMCF0603FT10R0CT-ND" H 7700 3850 50  0001 C CNN "Digi-Key_PN"
	1    7700 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	7850 3850 7900 3850
Connection ~ 7900 3850
Wire Wire Line
	7300 4400 7900 4400
Wire Wire Line
	7900 4400 7900 4100
Connection ~ 7300 4400
$Comp
L Device:C_Small C?
U 1 1 5F925B9C
P 7900 4000
AR Path="/5EB41214/5F925B9C" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F925B9C" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F925B9C" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F925B9C" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5F925B9C" Ref="C14"  Part="1" 
AR Path="/5ECA36FF/5F925B9C" Ref="C?"  Part="1" 
AR Path="/5EED7553/5F925B9C" Ref="C?"  Part="1" 
AR Path="/5EEDC7D6/5F925B9C" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5F925B9C" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F925B9C" Ref="C?"  Part="1" 
F 0 "C14" H 7992 4046 50  0000 L CNN
F 1 "C 470p" H 7992 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7900 4000 50  0001 C CNN
F 3 "~" H 7900 4000 50  0001 C CNN
F 4 "16" H 7900 4000 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7900 4000 50  0001 C CNN "Tolerance"
F 6 "399-10051-1-ND" H 7900 4000 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 7900 4000 50  0001 C CNN "Farnell_PN"
	1    7900 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FB8F811
P 5250 3950
AR Path="/5EB41214/5FB8F811" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FB8F811" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FB8F811" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FB8F811" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5FB8F811" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FB8F811" Ref="C?"  Part="1" 
AR Path="/5ED08686/5FB8F811" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5FB8F811" Ref="C12"  Part="1" 
F 0 "C12" H 5342 3996 50  0000 L CNN
F 1 "C 1u" H 5342 3905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5250 3950 50  0001 C CNN
F 3 "~" H 5250 3950 50  0001 C CNN
F 4 "16" H 5250 3950 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5250 3950 50  0001 C CNN "Tolerance"
F 6 "311-1365-1-ND" H 5250 3950 50  0001 C CNN "Digi-Key_PN"
F 7 "2547037" H 5250 3950 50  0001 C CNN "Farnell_PN"
	1    5250 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4050 5250 4400
$Comp
L Device:R R?
U 1 1 5FB8F81A
P 4950 3750
AR Path="/5FB8F81A" Ref="R?"  Part="1" 
AR Path="/5EB41214/5FB8F81A" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5FB8F81A" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5FB8F81A" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5FB8F81A" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5FB8F81A" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5FB8F81A" Ref="R?"  Part="1" 
AR Path="/5ED08686/5FB8F81A" Ref="R?"  Part="1" 
AR Path="/5EE5AAE3/5FB8F81A" Ref="R15"  Part="1" 
F 0 "R15" V 5157 3750 50  0000 C CNN
F 1 "R 16K" V 5066 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4880 3750 50  0001 C CNN
F 3 "~" H 4950 3750 50  0001 C CNN
F 4 "1%" H 4950 3750 50  0001 C CNN "Tolerance"
F 5 "311-16.0KCRCT-ND" H 4950 3750 50  0001 C CNN "Digi-Key_PN"
	1    4950 3750
	0    -1   -1   0   
$EndComp
Text Notes 4600 3950 0    50   ~ 0
10Hz low pass
Wire Wire Line
	5100 3750 5250 3750
Wire Wire Line
	5250 3850 5250 3750
Wire Wire Line
	4800 3750 4650 3750
Connection ~ 5250 4400
Wire Wire Line
	5250 4400 5650 4400
Connection ~ 5250 3750
$EndSCHEMATC
