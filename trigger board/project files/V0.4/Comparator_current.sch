EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 27 30
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5ECAD0C8
P 6050 2550
AR Path="/5ECAD0C8" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ECAD0C8" Ref="R66"  Part="1" 
AR Path="/5ED2BC2A/5ECAD0C8" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ECAD0C8" Ref="R?"  Part="1" 
AR Path="/5EBFB816/5ECAD0C8" Ref="R?"  Part="1" 
F 0 "R66" V 6257 2550 50  0000 C CNN
F 1 "R 10K " V 6166 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5980 2550 50  0001 C CNN
F 3 "~" H 6050 2550 50  0001 C CNN
F 4 "0.1%" V 6050 2550 50  0001 C CNN "Tolerance"
	1    6050 2550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 2600 6650 2550
$Comp
L Device:C_Small C62
U 1 1 5ECAD0C5
P 9550 3450
AR Path="/5EB41214/5ECAD0C5" Ref="C62"  Part="1" 
AR Path="/5ED2BC2A/5ECAD0C5" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ECAD0C5" Ref="C?"  Part="1" 
AR Path="/5EBFB816/5ECAD0C5" Ref="C?"  Part="1" 
F 0 "C62" H 9642 3496 50  0000 L CNN
F 1 "C 47n" H 9642 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9550 3450 50  0001 C CNN
F 3 "~" H 9550 3450 50  0001 C CNN
F 4 "16" H 9550 3450 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 9550 3450 50  0001 C CNN "Tolerance"
	1    9550 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 3300 9550 3350
$Comp
L Device:C_Small C58
U 1 1 5EE8A62C
P 6900 3450
AR Path="/5EB41214/5EE8A62C" Ref="C58"  Part="1" 
AR Path="/5ED2BC2A/5EE8A62C" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5EE8A62C" Ref="C?"  Part="1" 
AR Path="/5EBFB816/5EE8A62C" Ref="C?"  Part="1" 
F 0 "C58" H 6992 3496 50  0000 L CNN
F 1 "C 47n" H 6992 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6900 3450 50  0001 C CNN
F 3 "~" H 6900 3450 50  0001 C CNN
F 4 "16" H 6900 3450 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 6900 3450 50  0001 C CNN "Tolerance"
	1    6900 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 3550 6900 3550
Wire Wire Line
	6900 3200 6900 3350
Wire Wire Line
	6650 3200 6650 3550
Wire Wire Line
	6200 2550 6250 2550
Wire Wire Line
	6450 2800 6250 2800
Wire Wire Line
	6250 2800 6250 2550
Connection ~ 6250 2550
Connection ~ 6650 2550
Wire Wire Line
	6650 2550 6650 2250
$Comp
L comparators_magne:MAX9141 U42
U 1 1 5ECAD0C6
P 6750 2900
AR Path="/5EB41214/5ECAD0C6" Ref="U42"  Part="1" 
AR Path="/5ED2BC2A/5ECAD0C6" Ref="U?"  Part="1" 
AR Path="/5EE0AC5D/5ECAD0C6" Ref="U?"  Part="1" 
AR Path="/5EBFB816/5ECAD0C6" Ref="U?"  Part="1" 
F 0 "U42" H 6850 3150 50  0000 L CNN
F 1 "MAX9141ESA+-ND" H 6850 3050 50  0000 L CNN
F 2 "digikey-footprints:SOIC-8_W3.9mm" H 6750 2900 50  0001 C CNN
F 3 "" H 6750 2900 50  0001 C CNN
	1    6750 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3200 6750 3200
Wire Wire Line
	9650 2950 9950 2950
Wire Wire Line
	6800 1450 6800 2600
$Comp
L Device:R R?
U 1 1 5ED74512
P 6450 4150
AR Path="/5ED74512" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED74512" Ref="R69"  Part="1" 
AR Path="/5ED2BC2A/5ED74512" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ED74512" Ref="R?"  Part="1" 
AR Path="/5EBFB816/5ED74512" Ref="R?"  Part="1" 
F 0 "R69" V 6243 4150 50  0000 C CNN
F 1 "R" V 6334 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6380 4150 50  0001 C CNN
F 3 "~" H 6450 4150 50  0001 C CNN
F 4 "1%" H 6450 4150 50  0001 C CNN "Tolerance"
	1    6450 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5ED74518
P 6050 4150
AR Path="/5ED74518" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ED74518" Ref="R67"  Part="1" 
AR Path="/5ED2BC2A/5ED74518" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ED74518" Ref="R?"  Part="1" 
AR Path="/5EBFB816/5ED74518" Ref="R?"  Part="1" 
F 0 "R67" V 6257 4150 50  0000 C CNN
F 1 "R 10K 0.1%" V 6166 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5980 4150 50  0001 C CNN
F 3 "~" H 6050 4150 50  0001 C CNN
F 4 "0.1%" H 6050 4150 50  0001 C CNN "Tolerance"
	1    6050 4150
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C61
U 1 1 5ED74520
P 9100 5150
AR Path="/5EB41214/5ED74520" Ref="C61"  Part="1" 
AR Path="/5ED2BC2A/5ED74520" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ED74520" Ref="C?"  Part="1" 
AR Path="/5EBFB816/5ED74520" Ref="C?"  Part="1" 
F 0 "C61" H 9192 5196 50  0000 L CNN
F 1 "C 47n" H 9192 5105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9100 5150 50  0001 C CNN
F 3 "~" H 9100 5150 50  0001 C CNN
F 4 "16" H 9100 5150 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 9100 5150 50  0001 C CNN "Tolerance"
	1    9100 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 5000 9100 5050
Wire Wire Line
	6650 4800 6650 5250
Wire Wire Line
	6200 4150 6250 4150
Wire Wire Line
	6450 4400 6250 4400
Wire Wire Line
	6250 4400 6250 4150
Connection ~ 6250 4150
Wire Wire Line
	6250 4150 6300 4150
Wire Wire Line
	6600 4150 6650 4150
Connection ~ 6650 4150
Wire Wire Line
	6650 4150 6650 4050
$Comp
L comparators_magne:MAX9141 U43
U 1 1 5ED7456D
P 6750 4500
AR Path="/5EB41214/5ED7456D" Ref="U43"  Part="1" 
AR Path="/5ED2BC2A/5ED7456D" Ref="U?"  Part="1" 
AR Path="/5EE0AC5D/5ED7456D" Ref="U?"  Part="1" 
AR Path="/5EBFB816/5ED7456D" Ref="U?"  Part="1" 
F 0 "U43" H 7094 4546 50  0000 L CNN
F 1 "MAX9141ESA+-ND" H 7094 4455 50  0000 L CNN
F 2 "digikey-footprints:SOIC-8_W3.9mm" H 6750 4500 50  0001 C CNN
F 3 "" H 6750 4500 50  0001 C CNN
	1    6750 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 4800 6750 4800
Wire Wire Line
	8650 4950 8650 4600
Wire Wire Line
	8950 4650 8950 5250
Wire Wire Line
	8950 5250 9100 5250
Wire Wire Line
	6800 4200 6800 4150
Wire Wire Line
	6800 4150 6650 4150
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:SN74LVC1G07DBVR U?
U 1 1 5EEE408A
P 8450 2900
AR Path="/5ED2BC2A/5EEE408A" Ref="U?"  Part="1" 
AR Path="/5EB41214/5EEE408A" Ref="U44"  Part="1" 
F 0 "U44" H 8550 2550 60  0000 L CNN
F 1 "SN74LVC1G07DBVR" H 8550 2450 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 8650 3100 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g07" H 8650 3200 60  0001 L CNN
F 4 "296-8485-1-ND" H 8650 3300 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G07DBVR" H 8650 3400 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 8650 3500 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 8650 3600 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g07" H 8650 3700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G07DBVR/296-8485-1-ND/377454" H 8650 3800 60  0001 L CNN "DK_Detail_Page"
F 10 "IC BUF NON-INVERT 5.5V SOT23-5" H 8650 3900 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 8650 4000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8650 4100 60  0001 L CNN "Status"
	1    8450 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3200 8450 3550
Wire Wire Line
	8450 2500 8450 2600
$Comp
L Device:C_Small C60
U 1 1 5EEE4094
P 7650 3200
AR Path="/5EB41214/5EEE4094" Ref="C60"  Part="1" 
AR Path="/5ED2BC2A/5EEE4094" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5EEE4094" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5EEE4094" Ref="C?"  Part="1" 
F 0 "C60" H 7742 3246 50  0000 L CNN
F 1 "C 1u" H 7742 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7650 3200 50  0001 C CNN
F 3 "~" H 7650 3200 50  0001 C CNN
F 4 "16" H 7650 3200 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 7650 3200 50  0001 C CNN "Tolerance"
	1    7650 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EEE409A
P 7500 2900
AR Path="/5EEE409A" Ref="R?"  Part="1" 
AR Path="/5EB41214/5EEE409A" Ref="R70"  Part="1" 
AR Path="/5ED2BC2A/5EEE409A" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5EEE409A" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5EEE409A" Ref="R?"  Part="1" 
F 0 "R70" V 7293 2900 50  0000 C CNN
F 1 "R 150K" V 7384 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7430 2900 50  0001 C CNN
F 3 "~" H 7500 2900 50  0001 C CNN
F 4 "1%" H 7500 2900 50  0001 C CNN "Tolerance"
	1    7500 2900
	0    1    1    0   
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5EEE40A0
P 7500 2500
AR Path="/5ED2BC2A/5EEE40A0" Ref="D?"  Part="1" 
AR Path="/5EB41214/5EEE40A0" Ref="D16"  Part="1" 
F 0 "D16" H 7500 2717 50  0000 C CNN
F 1 "CUS520,H3F" H 7500 2626 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 7500 2500 50  0001 C CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=7041&prodName=CUS520" H 7500 2500 50  0001 C CNN
	1    7500 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 2500 7650 2900
Connection ~ 7650 2900
Wire Wire Line
	7650 2900 8150 2900
Wire Wire Line
	7650 2900 7650 3100
Wire Wire Line
	7050 2900 7350 2900
Text Notes 7650 2150 0    50   ~ 0
200ms delay
Wire Wire Line
	8750 2900 8900 2900
Wire Wire Line
	6900 3550 7650 3550
Connection ~ 6900 3550
Connection ~ 8450 3550
Wire Notes Line
	7150 2200 7150 3350
Wire Notes Line
	7150 3350 8750 3350
Wire Notes Line
	8750 3350 8750 2200
Wire Notes Line
	8750 2200 7150 2200
Wire Wire Line
	7650 3300 7650 3550
Connection ~ 7650 3550
Wire Wire Line
	7650 3550 8450 3550
Text HLabel 9950 2950 2    50   Input ~ 0
Isens_interlock
Text HLabel 9600 4550 2    50   Input ~ 0
Isens_ARM_alarm
$Comp
L dk_Logic-Gates-and-Inverters:SN74LVC1G14DBVR U?
U 1 1 5EF8DE8C
P 8450 4950
AR Path="/5EE8A13E/5EF8DE8C" Ref="U?"  Part="1" 
AR Path="/5EB41214/5EF8DE8C" Ref="U45"  Part="1" 
F 0 "U45" H 8300 4600 60  0000 L CNN
F 1 "SN74LVC1G14DBVR" H 8050 4500 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 8650 5150 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 8650 5250 60  0001 L CNN
F 4 "296-11607-1-ND" H 8650 5350 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G14DBVR" H 8650 5450 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 8650 5550 60  0001 L CNN "Category"
F 7 "Logic - Gates and Inverters" H 8650 5650 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 8650 5750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G14DBVR/296-11607-1-ND/385746" H 8650 5850 60  0001 L CNN "DK_Detail_Page"
F 10 "IC INVERTER SCHMITT 1CH SOT23-5" H 8650 5950 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 8650 6050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8650 6150 60  0001 L CNN "Status"
	1    8450 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 4650 8450 4750
Wire Wire Line
	7050 4500 8150 4500
Wire Wire Line
	8350 5150 8350 5250
Wire Wire Line
	5050 3000 6450 3000
Wire Wire Line
	8900 1150 8900 2150
$Sheet
S 9000 2050 550  200 
U 5F076AF2
F0 "RED/GREEN LED indicator" 50
F1 "R_G_LED_indicator_LVsens.sch" 50
F2 "Cntrl" I L 9000 2150 50 
$EndSheet
Wire Wire Line
	9000 2150 8900 2150
Wire Wire Line
	5500 1400 5500 1150
Wire Wire Line
	5500 1150 8900 1150
Wire Wire Line
	6250 2550 6300 2550
Wire Wire Line
	6600 2550 6650 2550
$Comp
L Device:R R?
U 1 1 5ECAD0C7
P 6450 2550
AR Path="/5ECAD0C7" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ECAD0C7" Ref="R68"  Part="1" 
AR Path="/5ED2BC2A/5ECAD0C7" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5ECAD0C7" Ref="R?"  Part="1" 
AR Path="/5EBFB816/5ECAD0C7" Ref="R?"  Part="1" 
F 0 "R68" V 6243 2550 50  0000 C CNN
F 1 "R" V 6334 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6380 2550 50  0001 C CNN
F 3 "~" H 6450 2550 50  0001 C CNN
F 4 "1%" H 6450 2550 50  0001 C CNN "Tolerance"
	1    6450 2550
	0    1    1    0   
$EndComp
Text GLabel 9100 3200 0    50   Input ~ 0
!ARM_LVsens
Text GLabel 5500 1500 0    50   Input ~ 0
!LE_LVsens
Wire Wire Line
	9100 3000 9100 3200
Text GLabel 8100 4950 0    50   Input ~ 0
!ARM_LVsens
Wire Wire Line
	8100 4950 8150 4950
Wire Wire Line
	8350 5250 8950 5250
Connection ~ 8350 5250
Connection ~ 8950 5250
Wire Wire Line
	8900 2150 8900 2900
Connection ~ 8900 2150
Connection ~ 8900 2900
Wire Wire Line
	8900 2900 9100 2900
Text Notes 4600 2900 0    50   ~ 0
What to set as limit? Chip abs max is 4A.\nExpected consumption is 1.1A.
Text GLabel 8150 4400 2    50   Input ~ 0
!I_arm
Wire Wire Line
	8150 4400 8150 4500
Connection ~ 8150 4500
Wire Wire Line
	8150 4500 8650 4500
Wire Wire Line
	6650 4200 6650 4150
Connection ~ 7350 2900
Wire Wire Line
	7350 2500 7350 2900
$Comp
L power:+5VA #PWR0129
U 1 1 5EC9E1F7
P 6650 2250
F 0 "#PWR0129" H 6650 2100 50  0001 C CNN
F 1 "+5VA" H 6665 2423 50  0000 C CNN
F 2 "" H 6650 2250 50  0001 C CNN
F 3 "" H 6650 2250 50  0001 C CNN
	1    6650 2250
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR0135
U 1 1 5EC9E437
P 8450 2500
F 0 "#PWR0135" H 8450 2350 50  0001 C CNN
F 1 "+5VA" H 8465 2673 50  0000 C CNN
F 2 "" H 8450 2500 50  0001 C CNN
F 3 "" H 8450 2500 50  0001 C CNN
	1    8450 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR0133
U 1 1 5EC9E68C
P 6900 3200
F 0 "#PWR0133" H 6900 3050 50  0001 C CNN
F 1 "+5VA" H 6915 3373 50  0000 C CNN
F 2 "" H 6900 3200 50  0001 C CNN
F 3 "" H 6900 3200 50  0001 C CNN
	1    6900 3200
	1    0    0    -1  
$EndComp
Connection ~ 6900 3200
$Comp
L power:+5VA #PWR0131
U 1 1 5EC9FBEC
P 6650 4050
F 0 "#PWR0131" H 6650 3900 50  0001 C CNN
F 1 "+5VA" H 6665 4223 50  0000 C CNN
F 2 "" H 6650 4050 50  0001 C CNN
F 3 "" H 6650 4050 50  0001 C CNN
	1    6650 4050
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR0134
U 1 1 5ECA26A1
P 6900 4800
F 0 "#PWR0134" H 6900 4650 50  0001 C CNN
F 1 "+5VA" H 6915 4973 50  0000 C CNN
F 2 "" H 6900 4800 50  0001 C CNN
F 3 "" H 6900 4800 50  0001 C CNN
	1    6900 4800
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR0136
U 1 1 5ECA2A09
P 8450 4650
F 0 "#PWR0136" H 8450 4500 50  0001 C CNN
F 1 "+5VA" H 8465 4823 50  0000 C CNN
F 2 "" H 8450 4650 50  0001 C CNN
F 3 "" H 8450 4650 50  0001 C CNN
	1    8450 4650
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR0138
U 1 1 5ECA37E3
P 9100 5000
F 0 "#PWR0138" H 9100 4850 50  0001 C CNN
F 1 "+5VA" H 9115 5173 50  0000 C CNN
F 2 "" H 9100 5000 50  0001 C CNN
F 3 "" H 9100 5000 50  0001 C CNN
	1    9100 5000
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR0139
U 1 1 5ECA8A8C
P 9550 3300
F 0 "#PWR0139" H 9550 3150 50  0001 C CNN
F 1 "+5VA" H 9565 3473 50  0000 C CNN
F 2 "" H 9550 3300 50  0001 C CNN
F 3 "" H 9550 3300 50  0001 C CNN
	1    9550 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR0132
U 1 1 5ECA9D29
P 6650 5250
F 0 "#PWR0132" H 6650 5000 50  0001 C CNN
F 1 "GND1" H 6655 5077 50  0000 C CNN
F 2 "" H 6650 5250 50  0001 C CNN
F 3 "" H 6650 5250 50  0001 C CNN
	1    6650 5250
	1    0    0    -1  
$EndComp
Connection ~ 6650 5250
$Comp
L power:GND1 #PWR0128
U 1 1 5ECAA030
P 5900 4150
F 0 "#PWR0128" H 5900 3900 50  0001 C CNN
F 1 "GND1" H 5905 3977 50  0000 C CNN
F 2 "" H 5900 4150 50  0001 C CNN
F 3 "" H 5900 4150 50  0001 C CNN
	1    5900 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR0130
U 1 1 5ECAA11B
P 6650 3550
F 0 "#PWR0130" H 6650 3300 50  0001 C CNN
F 1 "GND1" H 6655 3377 50  0000 C CNN
F 2 "" H 6650 3550 50  0001 C CNN
F 3 "" H 6650 3550 50  0001 C CNN
	1    6650 3550
	1    0    0    -1  
$EndComp
Connection ~ 6650 3550
$Comp
L power:GND1 #PWR0127
U 1 1 5ECAA248
P 5900 2550
F 0 "#PWR0127" H 5900 2300 50  0001 C CNN
F 1 "GND1" H 5905 2377 50  0000 C CNN
F 2 "" H 5900 2550 50  0001 C CNN
F 3 "" H 5900 2550 50  0001 C CNN
	1    5900 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3550 9550 3550
$Comp
L logic_gates_magne:74LVC2G32 U41
U 1 1 5ECD3981
P 6050 1450
F 0 "U41" H 6025 1717 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 6025 1626 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 6200 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 6250 1450 50  0001 C CNN
	1    6050 1450
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U41
U 2 1 5ECD45CA
P 9400 2950
F 0 "U41" H 9375 3217 50  0000 C CNN
F 1 "SN74LVC2G32DCUR" H 9375 3126 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 9550 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 9600 2950 50  0001 C CNN
	2    9400 2950
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC2G32 U41
U 3 1 5ECD4E7B
P 10000 3300
F 0 "U41" H 10053 3346 50  0000 L CNN
F 1 "SN74LVC2G32DCUR" H 10053 3255 50  0000 L CNN
F 2 "Package_SO:VSSOP-8_2.3x2mm_P0.5mm" H 10150 3350 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 10200 3300 50  0001 C CNN
	3    10000 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 3300 9800 3300
Wire Wire Line
	9800 3300 9800 3050
Wire Wire Line
	9800 3050 10000 3050
Wire Wire Line
	5500 1500 5750 1500
Wire Wire Line
	5500 1400 5750 1400
Connection ~ 9550 3300
Wire Wire Line
	9550 3550 10000 3550
Connection ~ 9550 3550
Wire Wire Line
	6300 1450 6800 1450
Wire Wire Line
	8950 4200 8950 4450
$Comp
L power:+5VA #PWR?
U 1 1 5ED10297
P 8950 4200
AR Path="/5ED2BC2A/5ED10297" Ref="#PWR?"  Part="1" 
AR Path="/5EB41214/5ED10297" Ref="#PWR0137"  Part="1" 
F 0 "#PWR0137" H 8950 4050 50  0001 C CNN
F 1 "+5VA" H 8965 4373 50  0000 C CNN
F 2 "" H 8950 4200 50  0001 C CNN
F 3 "" H 8950 4200 50  0001 C CNN
	1    8950 4200
	1    0    0    -1  
$EndComp
$Comp
L logic_gates_magne:74LVC1G32 U?
U 1 1 5ED1029D
P 8950 4550
AR Path="/5ED2BC2A/5ED1029D" Ref="U?"  Part="1" 
AR Path="/5EB41214/5ED1029D" Ref="U46"  Part="1" 
F 0 "U46" H 9244 4596 50  0000 L CNN
F 1 "SN74LVC1G32DCKR" H 9244 4505 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 8950 4550 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 8950 4550 50  0001 C CNN
	1    8950 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 4550 9600 4550
Connection ~ 6900 4800
Wire Wire Line
	6900 4800 6900 5050
Wire Wire Line
	6900 5250 8350 5250
Wire Wire Line
	6650 5250 6900 5250
Connection ~ 6900 5250
$Comp
L Device:C_Small C59
U 1 1 5ED74533
P 6900 5150
AR Path="/5EB41214/5ED74533" Ref="C59"  Part="1" 
AR Path="/5ED2BC2A/5ED74533" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5ED74533" Ref="C?"  Part="1" 
AR Path="/5EBFB816/5ED74533" Ref="C?"  Part="1" 
F 0 "C59" H 6992 5196 50  0000 L CNN
F 1 "C 47n" H 6992 5105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6900 5150 50  0001 C CNN
F 3 "~" H 6900 5150 50  0001 C CNN
F 4 "16" H 6900 5150 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 6900 5150 50  0001 C CNN "Tolerance"
	1    6900 5150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
