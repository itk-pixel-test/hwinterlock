EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 5850 3400 800  200 
U 5EE0AC5D
F0 "Comparator temperature low" 50
F1 "Comparator_temperature_low.sch" 50
F2 "Input" I L 5850 3500 50 
F3 "Output" I R 6650 3500 50 
$EndSheet
$Sheet
S 5850 3850 800  200 
U 5EE0E313
F0 "Comparator temperature high" 50
F1 "Comparator_temperature_high.sch" 50
F2 "Input" I L 5850 3950 50 
F3 "Output" I R 6650 3950 50 
$EndSheet
Wire Wire Line
	5850 3950 5650 3950
Wire Wire Line
	5650 3950 5650 3500
Wire Wire Line
	5650 3500 5850 3500
Connection ~ 5650 3500
$Sheet
S 5850 4650 650  200 
U 5EE5AAE3
F0 "Box switch" 50
F1 "Switch_0.sch" 50
F2 "Output" I R 6500 4750 50 
F3 "Input" I L 5850 4750 50 
$EndSheet
$Sheet
S 5850 5100 800  200 
U 5EE77D5A
F0 "Comparator dry air flow" 50
F1 "Comparator_flow.sch" 50
F2 "Input" I L 5850 5200 50 
F3 "Output" I R 6650 5200 50 
$EndSheet
$Sheet
S 5850 2900 1050 250 
U 5EE8A13E
F0 "Comparator temperature alarm" 50
F1 "Comparator_temperature_alarm.sch" 50
F2 "Input" I L 5850 3000 50 
F3 "T_ARM_alarm_hi" I R 6900 2950 50 
F4 "T_ARM_alarm_lo" I R 6900 3050 50 
$EndSheet
Wire Wire Line
	5650 3000 5850 3000
Wire Wire Line
	5650 3500 5650 3000
Wire Wire Line
	6900 2950 8300 2950
Wire Wire Line
	8300 2950 8300 1400
Wire Wire Line
	6900 3050 8350 3050
$Sheet
S 9050 4500 750  900 
U 5F19D573
F0 "Back connectors" 50
F1 "Back_connectors.sch" 50
F2 "!BSW_interlock" I L 9050 5050 50 
F3 "!DA_interlock" I L 9050 5200 50 
F4 "!T_lo_interlock" I L 9050 4850 50 
F5 "!T_hi_interlock" I L 9050 4950 50 
F6 "!V_interlock" I L 9050 4650 50 
F7 "!RH_interlock" I L 9050 4750 50 
F8 "!ALT_SW_interlock" I L 9050 5300 50 
$EndSheet
Wire Wire Line
	8300 1400 8400 1400
$Sheet
S 8400 1150 550  500 
U 5EBFF7D9
F0 "Unarmed alarm" 50
F1 "Unarmed_alarm.sch" 50
F2 "V_alarm" I L 8400 1250 50 
F3 "T_hi_alarm" I L 8400 1400 50 
F4 "T_lo_alarm" I L 8400 1550 50 
$EndSheet
Wire Wire Line
	4500 3500 5650 3500
$Sheet
S 3700 3300 800  300 
U 5EE136D8
F0 "NTC signal conditioner" 50
F1 "NTC_signal_conditioner.sch" 50
F2 "NTC_in" I L 3700 3500 50 
F3 "NTC_out" I R 4500 3500 50 
$EndSheet
$Sheet
S 5850 5500 650  200 
U 5ECA36FF
F0 "Alternative switch" 50
F1 "Alt_switch.sch" 50
F2 "Output" I R 6500 5600 50 
F3 "Input" I L 5850 5600 50 
$EndSheet
$Sheet
S 8950 2450 1300 650 
U 5ECDAC29
F0 "Arm-signal creation" 50
F1 "Arm_signal_creation.sch" 50
$EndSheet
$Sheet
S 5850 2400 800  300 
U 5ED08686
F0 "Comparator relative humidity" 50
F1 "Comparator_RH.sch" 50
F2 "HUM_SDA" I L 5850 2450 50 
F3 "HUM_SCL" I L 5850 2550 50 
F4 "Output" I R 6650 2500 50 
F5 "NTC" I L 5850 2650 50 
$EndSheet
Wire Wire Line
	6650 2500 8000 2500
Wire Wire Line
	6650 3500 7950 3500
Wire Wire Line
	6650 3950 7900 3950
Wire Wire Line
	8100 1400 8100 4650
Wire Wire Line
	8100 4650 9050 4650
Wire Wire Line
	8000 2500 8000 4750
Wire Wire Line
	8000 4750 9050 4750
Wire Wire Line
	7950 3500 7950 4850
Wire Wire Line
	7950 4850 9050 4850
Wire Wire Line
	7900 3950 7900 4950
Wire Wire Line
	7900 4950 9050 4950
Wire Wire Line
	7850 5050 9050 5050
Wire Wire Line
	7100 1250 8400 1250
Wire Wire Line
	7100 1400 8100 1400
$Sheet
S 2950 1250 850  200 
U 5ECF279F
F0 "Input protection" 50
F1 "Input_protection.sch" 50
F2 "Input" I L 2950 1350 50 
F3 "Output" I R 3800 1350 50 
$EndSheet
Wire Wire Line
	8400 1550 8350 1550
Wire Wire Line
	8350 1550 8350 3050
Wire Wire Line
	5850 2550 2150 2550
Wire Wire Line
	5850 2450 2150 2450
Wire Wire Line
	2150 3500 3700 3500
$Sheet
S 1050 1150 1100 4600
U 5EDDE380
F0 "Front connectors" 50
F1 "Connectors_front.sch" 50
F2 "NTC_out" I R 2150 3500 50 
F3 "HUM_SDA" I R 2150 2450 50 
F4 "HUM_SCL" I R 2150 2550 50 
F5 "Box_sw" I R 2150 4750 50 
F6 "Flow_sense" I R 2150 5200 50 
F7 "Alt_sw" I R 2150 5600 50 
F8 "V_sense_hi" I R 2150 1350 50 
$EndSheet
$Sheet
S 4050 1200 1100 550 
U 5EED7553
F0 "Comparator voltage" 50
F1 "Comparator_V.sch" 50
F2 "Voltage" I L 4050 1350 50 
F3 "VSens_ARM" I R 5150 1700 50 
F4 "Vsens_ARM_alarm" I R 5150 1250 50 
F5 "Vsens_interlock" I R 5150 1400 50 
$EndSheet
Wire Wire Line
	3800 1350 4050 1350
Wire Wire Line
	2150 1350 2950 1350
Wire Wire Line
	5150 1250 5400 1250
Wire Wire Line
	5150 1400 5400 1400
Wire Wire Line
	5150 1700 5400 1700
Wire Wire Line
	5650 3000 5650 2650
Wire Wire Line
	5650 2650 5850 2650
Connection ~ 5650 3000
$Sheet
S 8950 3350 1200 550 
U 5F94411C
F0 "Temperature limit selector" 50
F1 "t_select.sch" 50
$EndSheet
$Sheet
S 5850 4250 550  200 
U 5FAAE40B
F0 "NTC Digitizer" 50
F1 "ntc_digitizer.sch" 50
F2 "In" I L 5850 4350 50 
$EndSheet
Wire Wire Line
	7850 5050 7850 4750
Wire Wire Line
	6500 4750 7850 4750
Wire Wire Line
	5650 3950 5650 4350
Wire Wire Line
	5650 4350 5850 4350
Connection ~ 5650 3950
Wire Wire Line
	6650 5200 9050 5200
Wire Wire Line
	6500 5600 8150 5600
Wire Wire Line
	8150 5600 8150 5300
Wire Wire Line
	8150 5300 9050 5300
Wire Wire Line
	5850 4750 2150 4750
Wire Wire Line
	5850 5200 2150 5200
Wire Wire Line
	5850 5600 2150 5600
$Sheet
S 5400 1200 1700 550 
U 5EEDC7D6
F0 "V sense interconnects" 50
F1 "V_sens_interconnects.sch" 50
F2 "VSens_interlock" I L 5400 1400 50 
F3 "VSens_ARM_alarm" I L 5400 1250 50 
F4 "VSens_ARM" I L 5400 1700 50 
F5 "VSens_ARM_alarm_out" I R 7100 1250 50 
F6 "VSens_interlock_out" I R 7100 1400 50 
$EndSheet
$EndSCHEMATC
