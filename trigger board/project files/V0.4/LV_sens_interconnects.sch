EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 17 32
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Converter_DCDC:MEE1S0505SC PS1
U 1 1 5ED14C41
P 5600 2600
F 0 "PS1" H 5600 2967 50  0000 C CNN
F 1 "MEE1S0505SC" H 5600 2876 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_Murata_MEE1SxxxxSC_THT" H 4550 2350 50  0001 L CNN
F 3 "https://power.murata.com/pub/data/power/ncl/kdc_mee1.pdf" H 6650 2300 50  0001 L CNN
	1    5600 2600
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR060
U 1 1 5ED165FA
P 4300 2500
F 0 "#PWR060" H 4300 2350 50  0001 C CNN
F 1 "+5V" H 4315 2673 50  0000 C CNN
F 2 "" H 4300 2500 50  0001 C CNN
F 3 "" H 4300 2500 50  0001 C CNN
	1    4300 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR061
U 1 1 5ED171C9
P 4300 2700
F 0 "#PWR061" H 4300 2450 50  0001 C CNN
F 1 "GND" H 4305 2527 50  0000 C CNN
F 2 "" H 4300 2700 50  0001 C CNN
F 3 "" H 4300 2700 50  0001 C CNN
	1    4300 2700
	1    0    0    -1  
$EndComp
$Comp
L dk_Fixed-Inductors:82103C L2
U 1 1 5ED18A3C
P 6300 2500
F 0 "L2" H 6300 2747 60  0000 C CNN
F 1 "82103C" H 6300 2641 60  0000 C CNN
F 2 "digikey-footprints:1206" H 6500 2700 60  0001 L CNN
F 3 "https://www.murata-ps.com/data/magnetics/kmp_8200c.pdf" H 6500 2800 60  0001 L CNN
F 4 "811-2473-1-ND" H 6500 2900 60  0001 L CNN "Digi-Key_PN"
F 5 "82103C" H 6500 3000 60  0001 L CNN "MPN"
F 6 "Inductors, Coils, Chokes" H 6500 3100 60  0001 L CNN "Category"
F 7 "Fixed Inductors" H 6500 3200 60  0001 L CNN "Family"
F 8 "https://www.murata-ps.com/data/magnetics/kmp_8200c.pdf" H 6500 3300 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/murata-power-solutions-inc/82103C/811-2473-1-ND/3178544" H 6500 3400 60  0001 L CNN "DK_Detail_Page"
F 10 "FIXED IND 10UH 500MA 400 MOHM" H 6500 3500 60  0001 L CNN "Description"
F 11 "Murata Power Solutions Inc." H 6500 3600 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6500 3700 60  0001 L CNN "Status"
	1    6300 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C33
U 1 1 5ED1C071
P 6600 2600
F 0 "C33" H 6550 2400 50  0000 L CNN
F 1 "C 4.7u" H 6550 2300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6600 2600 50  0001 C CNN
F 3 "~" H 6600 2600 50  0001 C CNN
F 4 "16" H 6600 2600 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 6600 2600 50  0001 C CNN "Tolerance"
	1    6600 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2500 6100 2500
Wire Wire Line
	6500 2500 6600 2500
Connection ~ 6600 2500
Connection ~ 6600 2700
$Comp
L dk_Fixed-Inductors:82103C L1
U 1 1 5ED1E1F1
P 4900 2500
F 0 "L1" H 4900 2747 60  0000 C CNN
F 1 "82103C" H 4900 2641 60  0000 C CNN
F 2 "digikey-footprints:1206" H 5100 2700 60  0001 L CNN
F 3 "https://www.murata-ps.com/data/magnetics/kmp_8200c.pdf" H 5100 2800 60  0001 L CNN
F 4 "811-2473-1-ND" H 5100 2900 60  0001 L CNN "Digi-Key_PN"
F 5 "82103C" H 5100 3000 60  0001 L CNN "MPN"
F 6 "Inductors, Coils, Chokes" H 5100 3100 60  0001 L CNN "Category"
F 7 "Fixed Inductors" H 5100 3200 60  0001 L CNN "Family"
F 8 "https://www.murata-ps.com/data/magnetics/kmp_8200c.pdf" H 5100 3300 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/murata-power-solutions-inc/82103C/811-2473-1-ND/3178544" H 5100 3400 60  0001 L CNN "DK_Detail_Page"
F 10 "FIXED IND 10UH 500MA 400 MOHM" H 5100 3500 60  0001 L CNN "Description"
F 11 "Murata Power Solutions Inc." H 5100 3600 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5100 3700 60  0001 L CNN "Status"
	1    4900 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C28
U 1 1 5ED1FDEB
P 4600 2600
F 0 "C28" H 4550 2400 50  0000 L CNN
F 1 "C 680n" H 4550 2300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4600 2600 50  0001 C CNN
F 3 "~" H 4600 2600 50  0001 C CNN
F 4 "16" H 4600 2600 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 4600 2600 50  0001 C CNN "Tolerance"
	1    4600 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2500 4600 2500
Wire Wire Line
	5100 2500 5200 2500
Wire Wire Line
	4300 2700 4600 2700
Connection ~ 4600 2500
Wire Wire Line
	4600 2500 4700 2500
Connection ~ 4600 2700
Wire Wire Line
	4600 2700 5200 2700
Wire Wire Line
	5200 5200 5050 5200
Wire Wire Line
	5200 5100 5050 5100
Text HLabel 5050 5200 0    50   Input ~ 0
VSens_ARM_alarm
$Comp
L power:GND #PWR?
U 1 1 5F029AA8
P 5000 6000
AR Path="/5ED2BC2A/5F029AA8" Ref="#PWR?"  Part="1" 
AR Path="/5ED13689/5F029AA8" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 5000 5750 50  0001 C CNN
F 1 "GND" H 5005 5827 50  0000 C CNN
F 2 "" H 5000 6000 50  0001 C CNN
F 3 "" H 5000 6000 50  0001 C CNN
	1    5000 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5200 4800 5050 4800
$Comp
L power:+5V #PWR?
U 1 1 5F029AB1
P 5050 4800
AR Path="/5ED2BC2A/5F029AB1" Ref="#PWR?"  Part="1" 
AR Path="/5ED13689/5F029AB1" Ref="#PWR066"  Part="1" 
F 0 "#PWR066" H 5050 4650 50  0001 C CNN
F 1 "+5V" H 5065 4973 50  0000 C CNN
F 2 "" H 5050 4800 50  0001 C CNN
F 3 "" H 5050 4800 50  0001 C CNN
	1    5050 4800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6200 4800 6000 4800
Text HLabel 5050 5100 0    50   Input ~ 0
VSens_interlock
$Comp
L Device:C_Small C?
U 1 1 5F029ACB
P 6150 5900
AR Path="/5EB41214/5F029ACB" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F029ACB" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5F029ACB" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5F029ACB" Ref="C?"  Part="1" 
AR Path="/5ED13689/5F029ACB" Ref="C31"  Part="1" 
F 0 "C31" H 6242 5946 50  0000 L CNN
F 1 "C 100n" H 6242 5855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6150 5900 50  0001 C CNN
F 3 "~" H 6150 5900 50  0001 C CNN
F 4 "16" H 6150 5900 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 6150 5900 50  0001 C CNN "Tolerance"
	1    6150 5900
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F029AD1
P 5100 5900
AR Path="/5EB41214/5F029AD1" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F029AD1" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5F029AD1" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5F029AD1" Ref="C?"  Part="1" 
AR Path="/5ED13689/5F029AD1" Ref="C30"  Part="1" 
F 0 "C30" H 5192 5946 50  0000 L CNN
F 1 "C 100n" H 5192 5855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5100 5900 50  0001 C CNN
F 3 "~" H 5100 5900 50  0001 C CNN
F 4 "16" H 5100 5900 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5100 5900 50  0001 C CNN "Tolerance"
	1    5100 5900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6250 6000 6150 6000
Wire Wire Line
	6150 6000 6000 6000
Wire Wire Line
	6000 6000 6000 5600
Connection ~ 6150 6000
Wire Wire Line
	5200 5600 5200 6000
Wire Wire Line
	5200 6000 5100 6000
Connection ~ 5100 6000
Wire Wire Line
	5100 6000 5000 6000
$Comp
L power:+5V #PWR?
U 1 1 5F029AE0
P 5100 5800
AR Path="/5ED2BC2A/5F029AE0" Ref="#PWR?"  Part="1" 
AR Path="/5ED13689/5F029AE0" Ref="#PWR067"  Part="1" 
F 0 "#PWR067" H 5100 5650 50  0001 C CNN
F 1 "+5V" H 5115 5973 50  0000 C CNN
F 2 "" H 5100 5800 50  0001 C CNN
F 3 "" H 5100 5800 50  0001 C CNN
	1    5100 5800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6250 5800 6150 5800
$Comp
L Isolator:Si8640BA-B-IU U19
U 1 1 5F02A7A8
P 5600 5200
F 0 "U19" H 5600 5867 50  0000 C CNN
F 1 "Si8640BA-B-IU" H 5600 5776 50  0000 C CNN
F 2 "Package_SO:QSOP-16_3.9x4.9mm_P0.635mm" H 5600 4650 50  0001 C CIN
F 3 "https://www.silabs.com/documents/public/data-sheets/si864x-datasheet.pdf" H 5600 5600 50  0001 C CNN
	1    5600 5200
	-1   0    0    -1  
$EndComp
Text HLabel 5050 5300 0    50   Input ~ 0
ISens_interlock
Text HLabel 5050 5400 0    50   Input ~ 0
ISens_ARM_alarm
Wire Wire Line
	5200 5300 5050 5300
Wire Wire Line
	5200 5400 5050 5400
Text HLabel 6250 5100 2    50   Input ~ 0
VSens_interlock_in
Text HLabel 6250 5300 2    50   Input ~ 0
ISens_interlock_in
Text HLabel 6250 5400 2    50   Input ~ 0
ISens_ARM_alarm_in
Text HLabel 6250 5200 2    50   Input ~ 0
VSens_ARM_alarm_in
Wire Wire Line
	6250 5100 6000 5100
Wire Wire Line
	6250 5200 6000 5200
Wire Wire Line
	6250 5300 6000 5300
Wire Wire Line
	6250 5400 6000 5400
$Comp
L digital_isolators_magne:SI8620BB-B-IS U18
U 1 1 5F040DC5
P 5600 3650
F 0 "U18" H 5600 4117 50  0000 C CNN
F 1 "SI8620BB-B-IS" H 5600 4026 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5600 3650 50  0001 C CNN
F 3 "" H 5600 3650 50  0001 C CNN
	1    5600 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR063
U 1 1 5F049000
P 4950 3450
F 0 "#PWR063" H 4950 3300 50  0001 C CNN
F 1 "+5V" H 4965 3623 50  0000 C CNN
F 2 "" H 4950 3450 50  0001 C CNN
F 3 "" H 4950 3450 50  0001 C CNN
	1    4950 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3450 5100 3450
Wire Wire Line
	6100 3450 6250 3450
$Comp
L Device:C_Small C?
U 1 1 5F04C1B0
P 6250 4150
AR Path="/5EB41214/5F04C1B0" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F04C1B0" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5F04C1B0" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5F04C1B0" Ref="C?"  Part="1" 
AR Path="/5ED13689/5F04C1B0" Ref="C32"  Part="1" 
F 0 "C32" H 6342 4196 50  0000 L CNN
F 1 "C 100n" H 6342 4105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6250 4150 50  0001 C CNN
F 3 "~" H 6250 4150 50  0001 C CNN
F 4 "16" H 6250 4150 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 6250 4150 50  0001 C CNN "Tolerance"
	1    6250 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6350 4250 6250 4250
Wire Wire Line
	6250 4250 6100 4250
Connection ~ 6250 4250
Wire Wire Line
	6350 4050 6250 4050
$Comp
L power:GND #PWR?
U 1 1 5F04EE7A
P 4900 4300
AR Path="/5ED2BC2A/5F04EE7A" Ref="#PWR?"  Part="1" 
AR Path="/5ED13689/5F04EE7A" Ref="#PWR062"  Part="1" 
F 0 "#PWR062" H 4900 4050 50  0001 C CNN
F 1 "GND" H 4905 4127 50  0000 C CNN
F 2 "" H 4900 4300 50  0001 C CNN
F 3 "" H 4900 4300 50  0001 C CNN
	1    4900 4300
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F04EE80
P 5000 4200
AR Path="/5EB41214/5F04EE80" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F04EE80" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5F04EE80" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5F04EE80" Ref="C?"  Part="1" 
AR Path="/5ED13689/5F04EE80" Ref="C29"  Part="1" 
F 0 "C29" H 5092 4246 50  0000 L CNN
F 1 "C 100n" H 5092 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5000 4200 50  0001 C CNN
F 3 "~" H 5000 4200 50  0001 C CNN
F 4 "16" H 5000 4200 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5000 4200 50  0001 C CNN "Tolerance"
	1    5000 4200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 4300 5000 4300
Connection ~ 5000 4300
Wire Wire Line
	5000 4300 4900 4300
$Comp
L power:+5V #PWR?
U 1 1 5F04EE89
P 5000 4100
AR Path="/5ED2BC2A/5F04EE89" Ref="#PWR?"  Part="1" 
AR Path="/5ED13689/5F04EE89" Ref="#PWR064"  Part="1" 
F 0 "#PWR064" H 5000 3950 50  0001 C CNN
F 1 "+5V" H 5015 4273 50  0000 C CNN
F 2 "" H 5000 4100 50  0001 C CNN
F 3 "" H 5000 4100 50  0001 C CNN
	1    5000 4100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 4300 5100 3850
Wire Wire Line
	6100 4250 6100 3850
Text GLabel 5000 3550 0    50   Input ~ 0
!ARM
Text GLabel 5000 3750 0    50   Input ~ 0
!LE
Text GLabel 6250 3550 2    50   Input ~ 0
!ARM_LVsens
Text GLabel 6250 3750 2    50   Input ~ 0
!LE_LVsens
Wire Wire Line
	5000 3550 5100 3550
Wire Wire Line
	5000 3750 5100 3750
Wire Wire Line
	6100 3550 6250 3550
Wire Wire Line
	6100 3750 6250 3750
NoConn ~ 5200 4900
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5EE84295
P 5200 2500
F 0 "#FLG02" H 5200 2575 50  0001 C CNN
F 1 "PWR_FLAG" H 5200 2673 50  0000 C CNN
F 2 "" H 5200 2500 50  0001 C CNN
F 3 "~" H 5200 2500 50  0001 C CNN
	1    5200 2500
	1    0    0    -1  
$EndComp
Connection ~ 5200 2500
$Comp
L power:GND1 #PWR072
U 1 1 5EC99250
P 6300 2950
F 0 "#PWR072" H 6300 2700 50  0001 C CNN
F 1 "GND1" H 6305 2777 50  0000 C CNN
F 2 "" H 6300 2950 50  0001 C CNN
F 3 "" H 6300 2950 50  0001 C CNN
	1    6300 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 2950 6300 2700
Wire Wire Line
	6000 2700 6300 2700
Connection ~ 6300 2700
Wire Wire Line
	6300 2700 6600 2700
Wire Wire Line
	6600 2500 7000 2500
Wire Wire Line
	6600 2700 7000 2700
$Comp
L power:+5VA #PWR075
U 1 1 5EC956A4
P 7000 2500
F 0 "#PWR075" H 7000 2350 50  0001 C CNN
F 1 "+5VA" H 7015 2673 50  0000 C CNN
F 2 "" H 7000 2500 50  0001 C CNN
F 3 "" H 7000 2500 50  0001 C CNN
	1    7000 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR069
U 1 1 5EC95A4B
P 6250 3450
F 0 "#PWR069" H 6250 3300 50  0001 C CNN
F 1 "+5VA" H 6265 3623 50  0000 C CNN
F 2 "" H 6250 3450 50  0001 C CNN
F 3 "" H 6250 3450 50  0001 C CNN
	1    6250 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR073
U 1 1 5EC95C2B
P 6350 4050
F 0 "#PWR073" H 6350 3900 50  0001 C CNN
F 1 "+5VA" H 6365 4223 50  0000 C CNN
F 2 "" H 6350 4050 50  0001 C CNN
F 3 "" H 6350 4050 50  0001 C CNN
	1    6350 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR074
U 1 1 5EC95D5F
P 6350 4250
F 0 "#PWR074" H 6350 4000 50  0001 C CNN
F 1 "GND1" H 6355 4077 50  0000 C CNN
F 2 "" H 6350 4250 50  0001 C CNN
F 3 "" H 6350 4250 50  0001 C CNN
	1    6350 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR071
U 1 1 5EC95F32
P 6250 6000
F 0 "#PWR071" H 6250 5750 50  0001 C CNN
F 1 "GND1" H 6255 5827 50  0000 C CNN
F 2 "" H 6250 6000 50  0001 C CNN
F 3 "" H 6250 6000 50  0001 C CNN
	1    6250 6000
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR068
U 1 1 5EC96075
P 6200 4800
F 0 "#PWR068" H 6200 4650 50  0001 C CNN
F 1 "+5VA" H 6215 4973 50  0000 C CNN
F 2 "" H 6200 4800 50  0001 C CNN
F 3 "" H 6200 4800 50  0001 C CNN
	1    6200 4800
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR070
U 1 1 5EC96221
P 6250 5800
F 0 "#PWR070" H 6250 5650 50  0001 C CNN
F 1 "+5VA" H 6265 5973 50  0000 C CNN
F 2 "" H 6250 5800 50  0001 C CNN
F 3 "" H 6250 5800 50  0001 C CNN
	1    6250 5800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
