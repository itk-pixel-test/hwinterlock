EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 25 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5ECF2EAC
P 5150 3250
AR Path="/5ECF2EAC" Ref="R?"  Part="1" 
AR Path="/5EB41214/5ECF2EAC" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5ECF2EAC" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5ECF2EAC" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5ECF2EAC" Ref="R?"  Part="1" 
AR Path="/5ECF279F/5ECF2EAC" Ref="R66"  Part="1" 
F 0 "R66" V 4943 3250 50  0000 C CNN
F 1 "R 316" V 5034 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5080 3250 50  0001 C CNN
F 3 "~" H 5150 3250 50  0001 C CNN
F 4 "1%" H 5150 3250 50  0001 C CNN "Tolerance"
F 5 "P316CCT-ND" H 5150 3250 50  0001 C CNN "Digi-Key_PN"
	1    5150 3250
	0    1    1    0   
$EndComp
$Comp
L Device:D_Schottky D14
U 1 1 5ECF2EB2
P 5400 3500
F 0 "D14" V 5350 3300 50  0000 L CNN
F 1 "CUS520,H3F" V 5450 2950 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 5400 3500 50  0001 C CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=7041&prodName=CUS520" H 5400 3500 50  0001 C CNN
F 4 "CUS520H3FCT-ND" H 5400 3500 50  0001 C CNN "Digi-Key_PN"
	1    5400 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 2800 5400 2750
$Comp
L Device:D_Schottky D13
U 1 1 5ECF2EB9
P 5400 2950
F 0 "D13" V 5354 3030 50  0000 L CNN
F 1 "CUS520,H3F" V 5445 3030 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323_HandSoldering" H 5400 2950 50  0001 C CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=7041&prodName=CUS520" H 5400 2950 50  0001 C CNN
F 4 "CUS520H3FCT-ND" H 5400 2950 50  0001 C CNN "Digi-Key_PN"
	1    5400 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 3250 5000 3250
Wire Wire Line
	4350 3250 4500 3250
$Comp
L Device:Polyfuse F1
U 1 1 5ECF2EC3
P 4650 3250
F 0 "F1" V 4550 3250 50  0000 C CNN
F 1 "0ZCM0001FF2G" V 4800 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4700 3050 50  0001 L CNN
F 3 "https://belfuse.com/resources/datasheets/circuitprotection/ds-cp-0zcm-series.pdf" H 4650 3250 50  0001 C CNN
F 4 "507-0ZCM0001FF2GCT-ND" H 4650 3250 50  0001 C CNN "Digi-Key_PN"
	1    4650 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 3250 5400 3100
Wire Wire Line
	5400 3350 5400 3250
Connection ~ 5400 3250
Wire Wire Line
	5300 3250 5400 3250
Wire Wire Line
	5400 3650 5400 3700
Connection ~ 5400 3700
Wire Wire Line
	5400 3700 5650 3700
$Comp
L Device:C_Small C?
U 1 1 5ECF2ED2
P 5650 3450
AR Path="/5EB41214/5ECF2ED2" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5ECF2ED2" Ref="C?"  Part="1" 
AR Path="/5ECA2575/5ECF2ED2" Ref="C?"  Part="1" 
AR Path="/5ECAC95D/5ECF2ED2" Ref="C?"  Part="1" 
AR Path="/5ECF279F/5ECF2ED2" Ref="C62"  Part="1" 
F 0 "C62" H 5742 3496 50  0000 L CNN
F 1 "C 2.2u" H 5742 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5650 3450 50  0001 C CNN
F 3 "~" H 5650 3450 50  0001 C CNN
F 4 "16" H 5650 3450 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 5650 3450 50  0001 C CNN "Tolerance"
F 6 "490-4787-1-ND" H 5650 3450 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 5650 3450 50  0001 C CNN "Farnell_PN"
	1    5650 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3250 7050 3250
Wire Wire Line
	5400 3250 5650 3250
Connection ~ 5650 3250
Wire Wire Line
	5650 3350 5650 3250
Wire Wire Line
	5650 3550 5650 3700
Text HLabel 4350 3250 0    50   Input ~ 0
Input
$Comp
L power:GND1 #PWR?
U 1 1 5ECF2EDE
P 5400 3700
AR Path="/5ED2BC2A/5ECF2EDE" Ref="#PWR?"  Part="1" 
AR Path="/5ECF279F/5ECF2EDE" Ref="#PWR086"  Part="1" 
F 0 "#PWR086" H 5400 3450 50  0001 C CNN
F 1 "GND1" H 5405 3527 50  0000 C CNN
F 2 "" H 5400 3700 50  0001 C CNN
F 3 "" H 5400 3700 50  0001 C CNN
	1    5400 3700
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR?
U 1 1 5ECF2EE4
P 5400 2750
AR Path="/5ED2BC2A/5ECF2EE4" Ref="#PWR?"  Part="1" 
AR Path="/5ECF279F/5ECF2EE4" Ref="#PWR085"  Part="1" 
F 0 "#PWR085" H 5400 2600 50  0001 C CNN
F 1 "+5VA" H 5415 2923 50  0000 C CNN
F 2 "" H 5400 2750 50  0001 C CNN
F 3 "" H 5400 2750 50  0001 C CNN
	1    5400 2750
	1    0    0    -1  
$EndComp
Text Notes 3950 2400 0    50   ~ 0
Inrush current in case of a severe failure is limited by a polyfuse.\nInput voltage is clamped between GND and 5V to prevent damaging components down the line.\nVoltage signal is  filteered with 330Hz RC low-pass filter.
Text HLabel 7850 3350 2    50   Input ~ 0
Output
$Comp
L Diode:MM3Zxx D15
U 1 1 5F600CC7
P 6100 2950
F 0 "D15" V 6054 3030 50  0000 L CNN
F 1 "MM3Z5V6ST1GOSTR-ND" V 6145 3030 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323" H 6100 2775 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/mm3z2v4.pdf" H 6100 2950 50  0001 C CNN
F 4 "MM3Z5V6ST1GOSCT-ND" H 6100 2950 50  0001 C CNN "Digi-Key_PN"
	1    6100 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 2800 6100 2800
Connection ~ 5400 2800
Wire Wire Line
	6100 3100 6100 3700
Wire Wire Line
	6100 3700 5650 3700
Connection ~ 5650 3700
$Comp
L Amplifier_Difference:AD8276 U?
U 1 1 5FDD4975
P 7350 3350
AR Path="/5EED7553/5FDD4975" Ref="U?"  Part="1" 
AR Path="/5ECF279F/5FDD4975" Ref="U47"  Part="1" 
F 0 "U47" H 7694 3396 50  0000 L CNN
F 1 "AD8276" H 7694 3305 50  0000 L CNN
F 2 "Package_SO:TSSOP-8_3x3mm_P0.65mm" H 7350 3350 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/AD8276_8277.pdf" H 7350 3350 50  0001 C CNN
F 4 "AD8276ARMZ-R7CT-ND" H 7350 3350 50  0001 C CNN "Digi-Key_PN"
	1    7350 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR?
U 1 1 5FDD4FF7
P 7350 3750
AR Path="/5ED2BC2A/5FDD4FF7" Ref="#PWR?"  Part="1" 
AR Path="/5ECF279F/5FDD4FF7" Ref="#PWR0142"  Part="1" 
F 0 "#PWR0142" H 7350 3500 50  0001 C CNN
F 1 "GND1" H 7355 3577 50  0000 C CNN
F 2 "" H 7350 3750 50  0001 C CNN
F 3 "" H 7350 3750 50  0001 C CNN
	1    7350 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 3750 7350 3650
Wire Wire Line
	7250 3650 7350 3650
Connection ~ 7350 3650
$Comp
L power:+5VA #PWR?
U 1 1 5FDD5626
P 7250 2950
AR Path="/5ED2BC2A/5FDD5626" Ref="#PWR?"  Part="1" 
AR Path="/5ECF279F/5FDD5626" Ref="#PWR0143"  Part="1" 
F 0 "#PWR0143" H 7250 2800 50  0001 C CNN
F 1 "+5VA" H 7265 3123 50  0000 C CNN
F 2 "" H 7250 2950 50  0001 C CNN
F 3 "" H 7250 2950 50  0001 C CNN
	1    7250 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 2950 7250 3050
Wire Wire Line
	7650 3350 7650 3050
Wire Wire Line
	7650 3050 7350 3050
Wire Wire Line
	7050 3450 7050 3650
Wire Wire Line
	7050 3650 7250 3650
Connection ~ 7250 3650
Wire Wire Line
	7650 3350 7850 3350
Connection ~ 7650 3350
$EndSCHEMATC
