EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 29 30
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 9050 3800 2    50   Input ~ 0
!LE
$Comp
L dk_Logic-Gates-and-Inverters:SN74LVC1G08DBVR U44
U 1 1 5F8D451C
P 5550 3850
F 0 "U44" H 5850 4100 60  0000 L CNN
F 1 "SN74LVC1G08DBVR" H 5900 4250 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 5750 4050 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g08" H 5750 4150 60  0001 L CNN
F 4 "296-11601-1-ND" H 5750 4250 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G08DBVR" H 5750 4350 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 5750 4450 60  0001 L CNN "Category"
F 7 "Logic - Gates and Inverters" H 5750 4550 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g08" H 5750 4650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G08DBVR/296-11601-1-ND/385740" H 5750 4750 60  0001 L CNN "DK_Detail_Page"
F 10 "IC GATE AND 1CH 2-INP SOT23-5" H 5750 4850 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 5750 4950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5750 5050 60  0001 L CNN "Status"
	1    5550 3850
	1    0    0    -1  
$EndComp
$Sheet
S 6750 3800 550  300 
U 5F8D7D6A
F0 "SR Latch" 50
F1 "sr_latch.sch" 50
F2 "S" I L 6750 3850 50 
F3 "R" I L 6750 4050 50 
F4 "Q" I R 7300 3850 50 
F5 "~Q" I R 7300 4050 50 
$EndSheet
Text GLabel 5100 3950 0    50   Input ~ 0
!ARM
Text GLabel 5100 3750 0    50   Input ~ 0
RESET
Wire Wire Line
	5100 3750 5250 3750
$Comp
L dk_Logic-Gates-and-Inverters:SN74LVC1G14DBVR U?
U 1 1 5F8DE030
P 6200 4250
AR Path="/5EE8A13E/5F8DE030" Ref="U?"  Part="1" 
AR Path="/5F8D1E66/5F8DE030" Ref="U43"  Part="1" 
F 0 "U43" H 6444 4303 60  0000 L CNN
F 1 "SN74LVC1G14DBVR" H 5950 3800 60  0000 L CNN
F 2 "digikey-footprints:SOT-753" H 6400 4450 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 6400 4550 60  0001 L CNN
F 4 "296-11607-1-ND" H 6400 4650 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G14DBVR" H 6400 4750 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 6400 4850 60  0001 L CNN "Category"
F 7 "Logic - Gates and Inverters" H 6400 4950 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g14" H 6400 5050 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G14DBVR/296-11607-1-ND/385746" H 6400 5150 60  0001 L CNN "DK_Detail_Page"
F 10 "IC INVERTER SCHMITT 1CH SOT23-5" H 6400 5250 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 6400 5350 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6400 5450 60  0001 L CNN "Status"
	1    6200 4250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0122
U 1 1 5F8E199A
P 5550 3350
F 0 "#PWR0122" H 5550 3200 50  0001 C CNN
F 1 "+5V" H 5565 3523 50  0000 C CNN
F 2 "" H 5550 3350 50  0001 C CNN
F 3 "" H 5550 3350 50  0001 C CNN
	1    5550 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 5F8E1C5B
P 5550 4600
F 0 "#PWR0123" H 5550 4350 50  0001 C CNN
F 1 "GND" H 5400 4550 50  0000 C CNN
F 2 "" H 5550 4600 50  0001 C CNN
F 3 "" H 5550 4600 50  0001 C CNN
	1    5550 4600
	1    0    0    -1  
$EndComp
Text GLabel 7750 3750 0    50   Input ~ 0
RESET
Wire Wire Line
	7300 3850 7750 3850
$Comp
L logic_gates_magne:74LVC1G32 U?
U 1 1 5F8E3127
P 8050 3800
AR Path="/5ED2BC2A/5F8E3127" Ref="U?"  Part="1" 
AR Path="/5ED08686/5F8E3127" Ref="U?"  Part="1" 
AR Path="/5F8D1E66/5F8E3127" Ref="U45"  Part="1" 
F 0 "U45" H 8250 4150 50  0000 L CNN
F 1 "SN74LVC1G32DCKR" H 8300 4050 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 8050 3800 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 8050 3800 50  0001 C CNN
F 4 "296-9848-1-ND" H 8050 3800 50  0001 C CNN "Digi-Key_PN"
	1    8050 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 3950 5250 3950
Wire Wire Line
	5850 3850 6750 3850
Wire Wire Line
	6400 4250 6400 4050
Wire Wire Line
	6400 4050 6750 4050
Wire Wire Line
	8300 3800 8400 3800
Wire Wire Line
	5550 3550 5550 3350
Wire Wire Line
	5550 3350 6200 3350
Wire Wire Line
	8050 3350 8050 3700
Connection ~ 5550 3350
Wire Wire Line
	6200 4050 6200 3350
Connection ~ 6200 3350
Wire Wire Line
	6200 3350 8050 3350
Wire Wire Line
	5550 4150 5550 4600
Wire Wire Line
	5550 4600 6100 4600
Wire Wire Line
	8050 4600 8050 3900
Connection ~ 5550 4600
Wire Wire Line
	6100 4450 6100 4600
Connection ~ 6100 4600
Wire Wire Line
	6100 4600 8050 4600
Wire Wire Line
	5250 3950 5250 4250
Wire Wire Line
	5250 4250 5900 4250
Connection ~ 5250 3950
$Comp
L Device:C_Small C?
U 1 1 5F8E9F9B
P 8200 4500
AR Path="/5EB41214/5F8E9F9B" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F8E9F9B" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F8E9F9B" Ref="C?"  Part="1" 
AR Path="/5F8D1E66/5F8E9F9B" Ref="C67"  Part="1" 
F 0 "C67" H 8292 4546 50  0000 L CNN
F 1 "C 47n" H 8292 4455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8200 4500 50  0001 C CNN
F 3 "~" H 8200 4500 50  0001 C CNN
F 4 "16" H 8200 4500 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 8200 4500 50  0001 C CNN "Tolerance"
F 6 "1276-2524-1-ND" H 8200 4500 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 8200 4500 50  0001 C CNN "Farnell_PN"
	1    8200 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F8EACC9
P 8600 4500
AR Path="/5EB41214/5F8EACC9" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F8EACC9" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F8EACC9" Ref="C?"  Part="1" 
AR Path="/5F8D1E66/5F8EACC9" Ref="C68"  Part="1" 
F 0 "C68" H 8692 4546 50  0000 L CNN
F 1 "C 47n" H 8692 4455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8600 4500 50  0001 C CNN
F 3 "~" H 8600 4500 50  0001 C CNN
F 4 "16" H 8600 4500 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 8600 4500 50  0001 C CNN "Tolerance"
F 6 "1276-2524-1-ND" H 8600 4500 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 8600 4500 50  0001 C CNN "Farnell_PN"
	1    8600 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 4600 8200 4600
Connection ~ 8050 4600
Connection ~ 8200 4600
Wire Wire Line
	8200 4600 8600 4600
$Comp
L power:+5V #PWR0124
U 1 1 5F8EB034
P 8200 4400
F 0 "#PWR0124" H 8200 4250 50  0001 C CNN
F 1 "+5V" H 8215 4573 50  0000 C CNN
F 2 "" H 8200 4400 50  0001 C CNN
F 3 "" H 8200 4400 50  0001 C CNN
	1    8200 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 4400 8600 4400
Connection ~ 8200 4400
NoConn ~ 7300 4050
Wire Wire Line
	8750 3850 8750 3800
Wire Wire Line
	8750 3800 9050 3800
$Comp
L Device:R R?
U 1 1 5F8A711E
P 8550 3800
AR Path="/5F8A711E" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F8A711E" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F8A711E" Ref="R?"  Part="1" 
AR Path="/5ECA2575/5F8A711E" Ref="R?"  Part="1" 
AR Path="/5ECAC95D/5F8A711E" Ref="R?"  Part="1" 
AR Path="/5EED7553/5F8A711E" Ref="R?"  Part="1" 
AR Path="/5EEDC7D6/5F8A711E" Ref="R?"  Part="1" 
AR Path="/5ECDAC29/5F8A711E" Ref="R?"  Part="1" 
AR Path="/5F8D1E66/5F8A711E" Ref="R91"  Part="1" 
F 0 "R91" V 8343 3800 50  0000 C CNN
F 1 "R 10" V 8434 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8480 3800 50  0001 C CNN
F 3 "~" H 8550 3800 50  0001 C CNN
F 4 "" H 8550 3800 50  0001 C CNN "Tolerance"
F 5 "311-10.0HRCT-ND" H 8550 3800 50  0001 C CNN "Digi-Key_PN"
	1    8550 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	8700 3800 8750 3800
Connection ~ 8750 3800
Wire Wire Line
	8750 4050 8750 4600
Wire Wire Line
	8750 4600 8600 4600
Connection ~ 8600 4600
$Comp
L Device:C_Small C?
U 1 1 5F92CB0A
P 8750 3950
AR Path="/5EB41214/5F92CB0A" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5F92CB0A" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5F92CB0A" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5F92CB0A" Ref="C?"  Part="1" 
AR Path="/5EE5AAE3/5F92CB0A" Ref="C?"  Part="1" 
AR Path="/5ECA36FF/5F92CB0A" Ref="C?"  Part="1" 
AR Path="/5EED7553/5F92CB0A" Ref="C?"  Part="1" 
AR Path="/5EEDC7D6/5F92CB0A" Ref="C?"  Part="1" 
AR Path="/5ECDAC29/5F92CB0A" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5F92CB0A" Ref="C?"  Part="1" 
AR Path="/5F8D1E66/5F92CB0A" Ref="C83"  Part="1" 
F 0 "C83" H 8842 3996 50  0000 L CNN
F 1 "C 470p" H 8842 3905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8750 3950 50  0001 C CNN
F 3 "~" H 8750 3950 50  0001 C CNN
F 4 "16" H 8750 3950 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 8750 3950 50  0001 C CNN "Tolerance"
F 6 "399-10051-1-ND" H 8750 3950 50  0001 C CNN "Digi-Key_PN"
F 7 "" H 8750 3950 50  0001 C CNN "Farnell_PN"
	1    8750 3950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
