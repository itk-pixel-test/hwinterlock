EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 17 34
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+5V #PWR057
U 1 1 5EE1EABE
P 4300 4750
F 0 "#PWR057" H 4300 4600 50  0001 C CNN
F 1 "+5V" H 4315 4923 50  0000 C CNN
F 2 "" H 4300 4750 50  0001 C CNN
F 3 "" H 4300 4750 50  0001 C CNN
	1    4300 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 4750 4300 4850
$Comp
L power:GND #PWR058
U 1 1 5EE1FC1D
P 4300 5650
F 0 "#PWR058" H 4300 5400 50  0001 C CNN
F 1 "GND" H 4305 5477 50  0000 C CNN
F 2 "" H 4300 5650 50  0001 C CNN
F 3 "" H 4300 5650 50  0001 C CNN
	1    4300 5650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EE24B61
P 4050 2950
AR Path="/5EE24B61" Ref="#PWR?"  Part="1" 
AR Path="/5EE136D8/5EE24B61" Ref="#PWR055"  Part="1" 
F 0 "#PWR055" H 4050 2800 50  0001 C CNN
F 1 "+5V" H 4050 3100 50  0000 C CNN
F 2 "" H 4050 2950 50  0001 C CNN
F 3 "" H 4050 2950 50  0001 C CNN
	1    4050 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 2950 4050 3000
$Comp
L power:GND #PWR?
U 1 1 5EE24B69
P 4050 4250
AR Path="/5EE24B69" Ref="#PWR?"  Part="1" 
AR Path="/5EE136D8/5EE24B69" Ref="#PWR056"  Part="1" 
F 0 "#PWR056" H 4050 4000 50  0001 C CNN
F 1 "GND" H 4055 4077 50  0000 C CNN
F 2 "" H 4050 4250 50  0001 C CNN
F 3 "" H 4050 4250 50  0001 C CNN
	1    4050 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 3700 4050 4250
Wire Wire Line
	4050 3300 4050 3350
Connection ~ 4050 3350
Wire Wire Line
	4050 3350 4050 3400
Wire Wire Line
	3850 3350 4050 3350
Wire Wire Line
	4700 5550 4700 5650
$Comp
L Device:R R?
U 1 1 5EE24B55
P 4050 3550
AR Path="/5EE24B55" Ref="R?"  Part="1" 
AR Path="/5EE136D8/5EE24B55" Ref="R45"  Part="1" 
F 0 "R45" H 4120 3596 50  0000 L CNN
F 1 "R 132K" H 4120 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3980 3550 50  0001 C CNN
F 3 "https://www.digikey.com/product-detail/en/te-connectivity-passive-product/CPF0805B2M0E1/A103803CT-ND/2728842" H 4050 3550 50  0001 C CNN
F 4 "0.1%" H 4050 3550 50  0001 C CNN "Tolerance"
F 5 "764-1479-1-ND" H 4050 3550 50  0001 C CNN "Digi-Key_PN"
	1    4050 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5EE34586
P 4300 5300
AR Path="/5EB41214/5EE34586" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5EE34586" Ref="C?"  Part="1" 
AR Path="/5EE136D8/5EE34586" Ref="C42"  Part="1" 
F 0 "C42" H 4392 5346 50  0000 L CNN
F 1 "C 100n" H 4392 5255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4300 5300 50  0001 C CNN
F 3 "~" H 4300 5300 50  0001 C CNN
F 4 "16" H 4300 5300 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 4300 5300 50  0001 C CNN "Tolerance"
F 6 "399-8000-1-ND" H 4300 5300 50  0001 C CNN "Digi-Key_PN"
F 7 "718683" H 4300 5300 50  0001 C CNN "Farnell_PN"
	1    4300 5300
	1    0    0    -1  
$EndComp
Connection ~ 4050 4250
Text HLabel 3850 3350 0    50   Input ~ 0
NTC_in
Wire Wire Line
	4300 5200 4300 4850
Wire Wire Line
	4300 5400 4300 5650
Wire Wire Line
	4300 5650 4700 5650
Wire Wire Line
	5600 3950 5600 3450
Wire Wire Line
	4050 4250 4500 4250
Wire Wire Line
	5300 3600 5300 3950
Wire Wire Line
	5750 3450 5600 3450
Connection ~ 5600 3450
Wire Wire Line
	5600 3450 5550 3450
Wire Wire Line
	4850 3550 4850 3700
Wire Wire Line
	4850 3350 4500 3350
Wire Wire Line
	5100 3700 4850 3700
Connection ~ 4850 3700
Wire Wire Line
	4850 3700 4850 3950
Wire Wire Line
	5250 3950 5300 3950
Connection ~ 5300 3950
Wire Wire Line
	4950 3950 4850 3950
Connection ~ 4850 3950
Wire Wire Line
	4850 3950 4850 4250
Wire Wire Line
	4300 4850 4700 4850
Connection ~ 4300 4850
Connection ~ 4300 5650
$Comp
L Device:R R?
U 1 1 5F8E4180
P 5450 3950
AR Path="/5F8E4180" Ref="R?"  Part="1" 
AR Path="/5EB41214/5F8E4180" Ref="R?"  Part="1" 
AR Path="/5ED2BC2A/5F8E4180" Ref="R?"  Part="1" 
AR Path="/5EE0AC5D/5F8E4180" Ref="R?"  Part="1" 
AR Path="/5EE0E313/5F8E4180" Ref="R?"  Part="1" 
AR Path="/5EE77D5A/5F8E4180" Ref="R?"  Part="1" 
AR Path="/5EE8A13E/5F8E4180" Ref="R?"  Part="1" 
AR Path="/5EE136D8/5F8E4180" Ref="R47"  Part="1" 
F 0 "R47" V 5550 3950 50  0000 C CNN
F 1 "R 60.4K" V 5350 3950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5380 3950 50  0001 C CNN
F 3 "~" H 5450 3950 50  0001 C CNN
F 4 "0.1%" H 5450 3950 50  0001 C CNN "Tolerance"
F 5 "P20797CT-ND" H 5450 3950 50  0001 C CNN "Digi-Key_PN"
	1    5450 3950
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F8F57F0
P 5100 3950
AR Path="/5F8F57F0" Ref="R?"  Part="1" 
AR Path="/5EE136D8/5F8F57F0" Ref="R46"  Part="1" 
F 0 "R46" V 5000 3900 50  0000 L CNN
F 1 "R 84.5K" V 5200 3800 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5030 3950 50  0001 C CNN
F 3 "" H 5100 3950 50  0001 C CNN
F 4 "0.1%" H 5100 3950 50  0001 C CNN "Tolerance"
F 5 "P84.5KDACT-ND" H 5100 3950 50  0001 C CNN "Digi-Key_PN"
	1    5100 3950
	0    1    1    0   
$EndComp
Text Notes 4900 2950 0    50   ~ 0
1.7147x gain
$Comp
L Device:R R?
U 1 1 5F96D4EE
P 4050 3150
AR Path="/5F96D4EE" Ref="R?"  Part="1" 
AR Path="/5EE136D8/5F96D4EE" Ref="R44"  Part="1" 
F 0 "R44" V 3950 3100 50  0000 L CNN
F 1 "R 84.5K" V 4150 3000 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3980 3150 50  0001 C CNN
F 3 "" H 4050 3150 50  0001 C CNN
F 4 "0.1%" H 4050 3150 50  0001 C CNN "Tolerance"
F 5 "P84.5KDACT-ND" H 4050 3150 50  0001 C CNN "Digi-Key_PN"
	1    4050 3150
	-1   0    0    1   
$EndComp
$Comp
L amplifiers_magne:LTC6800 U22
U 1 1 5F9A1E4C
P 4950 3450
F 0 "U22" H 5200 3815 50  0000 C CNN
F 1 "LTC6800" H 5200 3724 50  0000 C CNN
F 2 "Package_SO:TSSOP-8_3x3mm_P0.65mm" H 5050 3900 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/6800fb.pdf" H 4950 3400 50  0001 C CNN
F 4 "LTC6800HMS8#TRPBFCT-ND" H 4950 3450 50  0001 C CNN "Digi-Key_PN"
	1    4950 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 5050 4700 4850
$Comp
L amplifiers_magne:LTC6800 U22
U 2 1 5F9A252F
P 4700 5300
F 0 "U22" H 4744 5346 50  0000 L CNN
F 1 "LTC6800" H 4744 5255 50  0000 L CNN
F 2 "Package_SO:TSSOP-8_3x3mm_P0.65mm" H 4800 5750 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/6800fb.pdf" H 4700 5250 50  0001 C CNN
F 4 "LTC6800HMS8#TRPBFCT-ND" H 4700 5300 50  0001 C CNN "Digi-Key_PN"
	2    4700 5300
	1    0    0    -1  
$EndComp
Text HLabel 6000 3450 2    50   Input ~ 0
NTC_out
Wire Wire Line
	5750 3450 6000 3450
$Comp
L dk_Test-Points:5011 TP?
U 1 1 5EE7772D
P 5750 3350
AR Path="/5EEDC7D6/5EE7772D" Ref="TP?"  Part="1" 
AR Path="/5EED7553/5EE7772D" Ref="TP?"  Part="1" 
AR Path="/5EE136D8/5EE7772D" Ref="TP7"  Part="1" 
F 0 "TP7" H 5700 3397 50  0000 R CNN
F 1 "5011" H 5750 3250 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5950 3550 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 5950 3650 60  0001 L CNN
F 4 "" H 5950 3750 60  0001 L CNN "Digi-Key_PN"
F 5 "5011" H 5950 3850 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 5950 3950 60  0001 L CNN "Category"
F 7 "Test Points" H 5950 4050 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1320" H 5950 4150 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5011/36-5011-ND/255333" H 5950 4250 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MULTIPURPOSE BLACK" H 5950 4350 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 5950 4450 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5950 4550 60  0001 L CNN "Status"
	1    5750 3350
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FB4958B
P 4500 3550
AR Path="/5EB41214/5FB4958B" Ref="C?"  Part="1" 
AR Path="/5ED2BC2A/5FB4958B" Ref="C?"  Part="1" 
AR Path="/5EE0AC5D/5FB4958B" Ref="C?"  Part="1" 
AR Path="/5EE0E313/5FB4958B" Ref="C?"  Part="1" 
AR Path="/5EE77D5A/5FB4958B" Ref="C?"  Part="1" 
AR Path="/5EE8A13E/5FB4958B" Ref="C?"  Part="1" 
AR Path="/5EE136D8/5FB4958B" Ref="C43"  Part="1" 
F 0 "C43" H 4592 3596 50  0000 L CNN
F 1 "C 1u" H 4592 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4500 3550 50  0001 C CNN
F 3 "~" H 4500 3550 50  0001 C CNN
F 4 "16" H 4500 3550 50  0001 C CNN "Voltage - rated"
F 5 "20%" H 4500 3550 50  0001 C CNN "Tolerance"
F 6 "311-1365-1-ND" H 4500 3550 50  0001 C CNN "Digi-Key_PN"
F 7 "2547037" H 4500 3550 50  0001 C CNN "Farnell_PN"
	1    4500 3550
	1    0    0    -1  
$EndComp
Connection ~ 5750 3450
Wire Wire Line
	4500 3450 4500 3350
Connection ~ 4500 3350
Wire Wire Line
	4500 3350 4050 3350
Wire Wire Line
	4500 3650 4500 4250
Connection ~ 4500 4250
Wire Wire Line
	4500 4250 4850 4250
Text Notes 4100 2700 0    50   ~ 0
Low pass filter.\n-3dB at -55C: 3.1Hz\n-3dB at -25C: 4.9Hz\n-3B at 25C : 19Hz\n-3dB at 50C: 41Hz
$EndSCHEMATC
