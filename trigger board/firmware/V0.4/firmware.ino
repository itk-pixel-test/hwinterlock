#include <SHTSensor_Tiny.h>
#include <TinyWireM.h>
#include <USI_TWI_Master.h>

SHTSensor_Tiny sht;
double hum;
int output;

void setup() {
  // put your setup code here, to run once:
  TinyWireM.begin();
  sht.init();
  sht.setAccuracy(SHTSensor_Tiny::SHT_ACCURACY_HIGH);
  pinMode(1, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (sht.readSample()) {
    hum = sht.getHumidity();
    output = (hum / 100.0) * 255;
    analogWrite(1, output);
  }
  else {
    analogWrite(1, 255);
  }
}
