# HW interlock

A repository collecting all information on the HW interlock used to protect ITk modules during testing. Please consult [the wiki](https://gitlab.cern.ch/itk-pixel-test/hwinterlock/-/wikis/home) for documentation.

## Architecture
The HW interlock consists of three main parts, ordered by distance from the ITk module under test (DUT):
1. Trigger board
2. Logic board
3. Power relay boards

The trigger board uses analog electronics to compare parameters such as module temperature and enclosure humidity against fixed limits. If any parameter exceeds its fixed limit, the output line corresponding to this parameter is pulled low. There is one trigger board for each DUT. Up to 8 trigger boards can be connected to one Logic board. The Logic board looks at the output lines from each Trigger board and decides which action to take in order to best protect the DUT and the test setup. The Logic board is connected to the peltier power relay board and the module power relay board, which cuts power to the peltier devices or the modules.

![Top level overview of the HW interlock](https://gitlab.cern.ch/itk-pixel-test/hwinterlock/-/wikis/uploads/6e784d0082d3d50ec4a588ef6afab5ba/untitled.png)

## Uploading firmware to the Trigger Board
First, you will need an AVR programmer like the [Tiny AVR Programmer](https://www.sparkfun.com/products/11801). You will program the ATTiny85 microcontroller with this device before mounting it on the Trigger Board.

Unzip and copy the "arduino-sht-tiny" library from `libraries\arduino\libraries` and add it to your local Arduino `libraries`
folder. Install "TinyWireM by Adafruit" using the Arduino IDE library manager found at `Sketch->Include Library->Manage Libraries...`.

Go to `File->Preferences`and enter `http://drazzy.com/package_drazzy.com_index.json` in `Additional Boards Manager URLs`. Install the "ATTinyCore by Spence Konde" using the Board Manager found at `Tools->Board->Boards Manager`.

Select the `ATtiny25/45/85 (No bootloader)` board and make sure that the `Clock Source` is set to `8 MHz (internal)`. Then, select one of the ISP programmer with "(ATTinyCore)" after it's name and upload the firmware. When it is done you can insert the ATTiny85 into its receptable on the Trigger Board.

## Required libraries 
For working on the Kicad project files, you must download the [Digi-Key Kicad libraries](https://www.digikey.com/en/resources/design-tools/kicad).
In Kicad, go to `Preferences -> Manage Symbol Libraries` and add the Digi-Key symbol library, and the `magne-custom` symbol library in `libraries\kicad`.
Then add the footprint libraries in the same way: `Preferences -> Manage Footprint Libraries`

## Current status
### Trigger board
- :white_check_mark: **Production files (v0.4)**
- :white_check_mark: **Project files (v0.4)**
- :white_check_mark: **Firmware (v0.4)**

### Logic board
- :white_check_mark: **Production files (v0.3)**
- :white_check_mark: **Project files (v0.3)**
- :x: **Firmware (v0.3)**

### Peltier power relay board
- :x: **Production files**
- :x: **Project files**

### Module power relay board
- :x: **Production files**
- :x: **Project files**
